import React from 'react';
import {ActivityIndicator, ScrollView, TextInput , TouchableOpacity} from 'react-native';
import {
    Container,
    Header,
    Title,
    Right,
    Left,
    Content,
    Button,
    Text,
    Card,
    CardItem,
    Body,
    View,
    Input,
    Item,
    Icon
} from 'native-base';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import Moment from 'react-native-moment-jalaali'
// import Icon from 'react-native-vector-icons/FontAwesome'

// My Import
import {form} from "./../assets/styles/index";
import {card} from "../assets/css/SyncWithServer";
import {common} from './../assets/css/common';

import {insertPorseshnameh} from './../components/database/porseshnameh'
import {setPorseshnameh} from "./../components/redux/actions/index";

import SearchPorseshnamehFlatList from './../components/flatLists/SearchPorseshnamehFlatList'
import ProjectFlatList from "../components/flatLists/ProjectFlatList";
import {getPorseshnamehFromDB} from './../components/database/porseshnameh'

import {insertProjectToDatabese} from './../components/database/project';

import axios from 'react-native-axios'


class PorseshnamehManagement extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            flatListItem: [],
        }

    }

    render() {

        return (
            <Container>
                <Header style={form.headerStyle}>
                    <Right>
                        <Title style={common.headerText}>{this.props.project.subject}</Title>
                        <TouchableOpacity style={{flexDirection: 'row'}}>
                            <Icon name='arrow-forward' onPress={() => Actions.replace('porseshnamehManagement')}
                                  style={{fontWeight: 500, color: 'white', marginLeft: 10}}/>
                        </TouchableOpacity>
                    </Right>
                </Header>
                <Content padder>
                    <SearchPorseshnamehFlatList idProject={this.props.project.idInHost}/>
                </Content>
            </Container>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        porseshnameh: state.porseshnameh,
        project: state.project,
        user: state.user
    }
}
const mapDispathToProps = dispatch => {
    return {
        setPorseshnameh: porseshnameh => {
            dispatch(setPorseshnameh(porseshnameh))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(PorseshnamehManagement);
