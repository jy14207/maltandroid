import EStyleSheet from 'react-native-extended-stylesheet';

export const drawer = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f6f5e3'

    },
    imageHeader: {
        height: 140,
        width: 140,
        borderRadius: 80,
        borderColor: '#f6f5e3',
        borderWidth: 1,
        marginTop: 5,
        marginRight: 25,
    },
    item: {
        justifyContent: 'flex-end',
        padding: 10,
        borderWidth: 0
    },
    itemTitle: {
        fontFamily: '$IS'
    },
    itemIcon: {
        marginLeft: 10
    }

})


export const form = EStyleSheet.create({

    styleForm: {
        paddingTop: 20,
        paddingBottom: 20,
    },
    headerStyle: {
        backgroundColor: '#2c3e50',
        backgroundColor: '#337ab7',
    },
    headerText: {
        color: 'white',
    },
    item: {
        borderRadius: 10,
        marginBottom: 10,
        paddingRight: 5,
        paddingLeft: 10,
        backgroundColor: '#ffffff',
        borderWidth: 2,
        borderColor: '#898c8c',

    },

    inputs: {
        fontFamily: 'IRANSansMobile',
        fontSize: 14,
        textAlign: 'right',
        alignSelf: 'stretch',


    },
    backButton: {
        // backgroundColor: '#2c3e50',
        backgroundColor: '#00c0ef',
        borderWidth: 1,
        borderRadius: 5,

    },
    submitButton: {
        // backgroundColor: '#2c3e50',
        backgroundColor: '#00a65a',
        borderWidth: 1,
        width: '100%',
        borderRadius: 25,
        borderColor: '#2ba66c',
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'center',
        paddingTop: 7,
        paddingBottom: 7,

    },
    submitText: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'IRANSansMobile_Medium',
        alignContent: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        paddingRight: 5,
    },
    submitCheckIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        textAlignVertical: 'center',
    },

    error: {
        fontFamily: 'IRANSansMobile',
        fontSize: 12,
        color: '#ed2f2f',
        marginBottom: 10
    }
})


export const index = EStyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backButtonImage:
        backgroundColor: '#2c3e50'
    },
    content: {
        marginTop: 100,
        paddingLeft: 10,
        paddingRight: 10

    },
    title: {
        color: '#fff513',
        fontFamily: 'IRANSansMobile_Bold',
        fontSize: 25,
    },
    text: {
        color: '#ffd534',
        fontFamily: 'IRANSansMobile',
        fontSize: 20
    },
    spectralQuestionStyle: {
        width: '30%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        borderColor: 'blue',
        borderWidth: 3,
    }


});
export default styles = {index};