import React from 'react';
import {Container, Header, Title, Right, Left, Content, Card, CardItem, Body, Input, Item, Button} from 'native-base';

import {
    Text,
    StyleSheet,
    ScrollView,
    View,
    Dimensions,
    TextInput,
    ProgressBarAndroid,
    Linking,
    ImageBackground,
    TouchableOpacity
} from 'react-native';
import Modal from 'react-native-modalbox';
import * as Progress from 'react-native-progress';

import Snackbar from 'react-native-snackbar';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome5'
import PersianCalendarPicker from 'react-native-persian-calendar-picker';

import {form} from "./../../assets/styles/index";
import {card} from "../../assets/css/SyncWithServer";
import {common} from './../../assets/css/common';
import {modal} from './../../assets/css/modal'

import {questionStyle} from '../../assets/css/questionStyle'
import {questionLoadFunc} from './QuestionLoad';
import {storeAnswerToDB} from './../database/answer';
import {getAnswerPromiseOfQuestionRow} from './../database/answer'
import QuestionHeader from "../ui/QuestionHeader";
import StoreButton from "../ui/StoreButton";

class DateFull extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            row: [],
            showSubmitCheck:false,
            answer: [],
            socialPolicyIs: false,
            stopPolicyIs: false,
            countPolicyIs: false,
            rowNumber: 0,
            percentage: 0,
            questionCount: 0,
            text: '',
            error: '#898c8c',
            update: false,
            selectedStartDate: null
        }
    }

    onDateChange = (date) => {
        this.setState({selectedStartDate: date});
    }

    componentDidMount() {
        console.log(this.props.answer);
        this.setState({
            row: this.props.row,
            answer: this.props.answer,
            socialPolicyIs: this.props.socialPolicyIs,
            stopPolicyIs: this.props.stopPolicyIs,
            countPolicyIs: this.props.countPolicyIs,
            percentage: this.props.percentage,
            questionCount: this.props.questionCount,
            rowNumber: this.props.rowNumber,

        });
        if (this.props.answer != null) {
            this.setState({
                update: true,
                text: this.props.answer['text'],
            })
        }
    }

    snackPolicy(socialPolicyMessage) {
        Snackbar.show({
            title: socialPolicyMessage,
            duration: Snackbar.LENGTH_INDEFINITE,
            action: {
                title: 'بستن',
                color: 'green',

            },
        })
    }

    dateMask = (text) => {
        let temp = text;
        let lenText = text.length;
        console.log("lenText :", lenText);
        if (lenText == 4) temp = temp + '/';
        if (lenText == 7) temp = temp + '/';
        if (lenText > 10)
            temp = temp.substr(0, 10);
        if (temp[5] > Number(1)) {
            temp = temp.replace(temp[5], '1');
            temp = temp.replace(temp[6], '2');
        }
        if (temp[5] == Number(1) && temp[6] > Number(2)) {
            temp = temp.replace(temp[5], '1');
            temp = temp.replace(temp[6], '2');
        }
        if (temp[8] > Number(3)) {
            temp = temp.replace(temp[8], '3');
            temp = temp.replace(temp[9], '0');
        }
        if (temp[8] == Number(3) && temp[9] > Number(1)) {
            temp = temp.replace(temp[9], '1');

        }


        if (temp[8] > 3) temp[8] = '3';

        this.setState({
            text: temp,
            error: '#898c8c',
        })
    }
    isValidDate = (str) => {
        // STRING FORMAT yyyy-mm-dd
        if (str == "" || str == null) {
            return false;
        }

        // m[1] is year 'YYYY' * m[2] is month 'MM' * m[3] is day 'DD'
        var m = str.match(/(\d{4})-(\d{2})-(\d{2})/);

        // STR IS NOT FIT m IS NOT OBJECT
        if (m === null || typeof m !== 'object') {
            return false;
        }

        // CHECK m TYPE
        if (typeof m !== 'object' && m !== null && m.size !== 3) {
            return false;
        }

        var ret = true; //RETURN VALUE
        var thisYear = new Date().getFullYear(); //YEAR NOW
        var minYear = 1999; //MIN YEAR

        // YEAR CHECK
        if ((m[1].length < 4) || m[1] < minYear || m[1] > thisYear) {
            ret = false;
        }
        // MONTH CHECK
        if ((m[2].length < 2) || m[2] < 1 || m[2] > 12) {
            ret = false;
        }
        // DAY CHECK
        if ((m[3].length < 2) || m[3] < 1 || m[3] > 31) {
            ret = false;
        }

        return ret;
    }
    jumpToQuestion = () => {
        var idProject = this.props.project.idInHost;
        var idPorseshnameh = this.props.porseshnameh.id;
        var rowNumber = this.state.rowNumber;
        if (this.state.rowNumber != 0 && this.state.rowNumber != null && this.state.rowNumber != '') {
            console.log('rowNumberInJump:', this.state.rowNumber)
            questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, this.state.rowNumber)
        }
        else {
            alert("شماره سوال را وارد کنید.")
        }
    }

    storeAnswer = async() => {
        // console.log("isValidDate:", this.isValidDate(this.state.text));
        if (this.state.text == '' && this.state.row.allowNull == 'no') {
            this.setState({error: 'red'});
        }
        else {
            this.setState({error: '#898c8c'});
            var idQuestion = this.state.row.idInHost;
            let idPorseshnameh = this.props.porseshnameh.id;
            console.log("idPorseshnameh : 12548798:", idPorseshnameh);
            let text = this.state.text;
            // const answer = await getAnswerPromiseOfQuestionRow(this.state.row.idInHost, idPorseshnameh);
            // console.log("answerrrrrrrrrrrrrr : " , answer)
            if (this.state.update === false)
               await storeAnswerToDB(idPorseshnameh, idQuestion, 'openQuestion', text, 'insert');
            else
               await storeAnswerToDB(idPorseshnameh, idQuestion, 'openQuestion', text, 'update', this.state.answer['id']);

            let rowNumber = (Number(this.state.rowNumber) + 1);
            this.setState({showSubmitCheck:true});

            questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, rowNumber);
        }
    };
    loadPreviousQuestion = () => {
        rowNumber = (Number(this.state.rowNumber) - 1);
        questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, rowNumber);

    };

    render() {
        var progressColor = 'orange';
        const {selectedStartDate} = this.state;
        const startDate = selectedStartDate ? selectedStartDate.toString() : '';
        if (this.state.percentage / 100 > 50) {
            progressColor = "green"
        }

        var policy = false, socialPolicyMessage = false, stopPolicyIsMessage = false, countPolicyIsMessage = false;
        if (this.state.socialPolicyIs == true) {
            socialPolicyMessage = 'جواب این سوال در تعیین طبقه اجتماعی موثر است.';
            policy = true;
        }
        if (this.state.stopPolicyIs == true) {
            stopPolicyIsMessage = 'سیاست توقف مصاحبه برای این سوال لحاظ شده است.';
            policy = true;
        }
        if (this.state.countPolicyIs == true) {
            countPolicyIsMessage = 'سیاست تعداد خاص از پرسشنامه برای این سوال لحاظ شده است.';
            policy = true;
        }


        var policyansToOtherTextOptionsBool = false, policyansToOtherTextOptions = '';
        if (this.props.policyansToOtherTextOptions != '') {
            policyansToOtherTextOptions = this.props.policyansToOtherTextOptions;
            policyansToOtherTextOptionsBool = true;
        }


        // console.log("socialPolicyIs=", this.state.socialPolicyIs);

        const componenetTitle = this.props.project.subject;
        const oneCardbuttonText = 'ثبت';
        return (
            <Container>
                <ImageBackground
                    source={require('./../../assets/images/AppImage/question.jpg')}
                    style={common.fullImageBackground}
                    imageStyle={{borderRadius: 2}}>
                    <QuestionHeader
                        myOnPress={this.loadPreviousQuestion}
                        project={this.props.project}
                        modal={this.refs.modal4}
                    />
                    <Content padder style={{marginTop: 20}}>
                        {policy ? <View hide={policy} style={common.questionPolicyAlert}>
                            {socialPolicyMessage ?
                                <Text style={common.questionPolicyTextAlert}> - {socialPolicyMessage}</Text> : null}
                            {stopPolicyIsMessage ?
                                <Text style={common.questionPolicyTextAlert}> - {stopPolicyIsMessage}</Text> : null}
                            {countPolicyIsMessage ?
                                <Text style={common.questionPolicyTextAlert}> - {countPolicyIsMessage}</Text> : null}
                        </View> : null}
                        <ScrollView style={{height: "100%", flex: 1}}>
                            <Card style={card.mainCard}>
                                <CardItem bordered style={card.cardItemHeader}>
                                    <Text
                                        style={[card.cardTitle, common.titleText]}>{this.state.row.rowNumber + "- " + this.state.row.text}
                                        <Text
                                            style={[card.cardTitle, common.moreExplanationText]}>{this.state.row.moreExplanation}</Text>
                                        {policyansToOtherTextOptionsBool ?
                                            <Text
                                                style={[card.cardTitle, common.ansToOtherQTextStyle]}>/جواب هایی که به
                                                سوال های قبل داده اید : {policyansToOtherTextOptions}</Text>
                                            : null}</Text>
                                </CardItem>
                                <CardItem>
                                    <View style={{flex: 1, flexDirection: 'column'}}>
                                        <TextInput
                                            onSubmitEditing={() => this.storeAnswer()}
                                            style={[questionStyle.openQInput, {
                                                borderColor: this.state.error,
                                                marginLeft: '50%',
                                                width: '50%',
                                                height: 40,
                                                padding: 2,
                                            }]}
                                            multiline={false}
                                            numberOfLines={1}
                                            placeholder='----/--/--'
                                            onChangeText={(text) => this.dateMask(text)}
                                            value={this.state.text}
                                        />
                                    </View>
                                </CardItem>
                                <CardItem footer bordered>
                                    <StoreButton onPress={this.storeAnswer} showSubmitCheck={this.state.showSubmitCheck}/>
                                </CardItem>
                            </Card>
                        </ScrollView>
                    </Content>
                    <Modal style={[modal.modal, modal.modal4]} position={"bottom"} ref={"modal4"}>
                        <Card style={modal.modalMainCard}>
                            <CardItem bordered style={card.cardTitle}>
                                <Text style={[common.headerText, {color: 'white'}]}>اطلاعات بیشتر . . .</Text>
                            </CardItem>
                            <CardItem style={{flexDirection: 'column'}}>
                                <Text>{this.state.percentage}% - <Text
                                    style={common.bodyText}>( {this.state.row.rowNumber}
                                    از {this.state.questionCount})</Text></Text>
                                <Progress.Bar progress={this.state.percentage / 100} width={300}
                                              color={(this.state.percentage / 100) < 0.5 ? "red" : "green"}
                                              height={30}></Progress.Bar>
                            </CardItem>
                            <CardItem style={card.modalCardItem2}>
                                <Button info style={[common.bodyText, modal.modalButton]} onPress={this.jumpToQuestion}><Text
                                    onPress={this.jumpToQuestion} style={common.bodyText}> پرش </Text></Button>
                                <Input
                                    style={[common.textInput, modal.modalInput]}
                                    keyboardType='numeric'
                                    onSubmitEditing={()=>this.jumpToQuestion()}
                                    placeholder='شماره سوال؟'
                                    onChangeText={val => this.setState({rowNumber: val})}/>
                            </CardItem>
                            <View style={card.modalView}>
                                <Text style={[common.bodyText]}>بیشتر بدانید :
                                    <Text style={common.webLink} onPress={() => {
                                        Linking.openURL(this.props.user.webSiteLink)
                                    }}>{this.props.user.companyName} </Text>
                                </Text>
                            </View>
                        </Card>
                    </Modal>
                </ImageBackground>
            </Container>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        porseshnameh: state.porseshnameh,
        project: state.project,
        user: state.user
    }
}
const mapDispathToProps = dispatch => {
    return {
        setPorseshnameh: porseshnameh => {
            dispatch(setPorseshnameh(porseshnameh))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(DateFull);
