import React from 'react';
import {ActivityIndicator , NetInfo} from 'react-native';
import {
    Container,
    Header,
    Title,
    Right,
    Left,
    Content,
    Button,
    Text,
    Icon,
    Card,
    CardItem,
    Body,
    View,
    Input
} from 'native-base';
import {Overlay} from 'react-native-elements';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import axios from 'react-native-axios'

import {insertProjectToDatabese} from './../components/database/project';
import {form} from "./../assets/styles/index";
import {card} from "../assets/css/SyncWithServer";
import {common} from '../assets/css/common';
class SyncWithServer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showOverly: false,
            showGetProjectsFromServer:false,
            textOfProjectFromServer:'',
            idUser:'1',
            visitDate:''
        }
    }

    render() {
        const componenetTitle = 'همسان سازی داده ها با سرور ';

        const oneCardTitle = 'دریافت داده ها از سرور . . .';
        // const oneCardBody = 'برای دریافت داده ها و پروژه ها کلیک کنید،این کار باعث برروز رسانی داده هایی مانند صورت سوال ها و ... می شود.';
        const oneCardBody = 'آی دی بیمار را وارد کنید:';
        const oneCardbuttonText = 'دریافت';

        const twoCardTitle = 'ارسال داده ها به سرور . . .';
        const twoCardBody = 'برای ارسال پرسشنامه هایی که تا کنون جواب داده اید به سرور از این بخش استفاده کنید';
        const twoCardbuttonText = 'ارسال';

        return (
            <Container>
                <Header style={form.headerStyle}>
                    <Right>
                        <Title style={common.headerText}>{componenetTitle}</Title>
                        {/*<Icon name='arrow-forward' onPress={() => Actions.reset('root')}
                              style={{fontWeight: 500, color: 'white', marginLeft: 85}}/>*/}
                    </Right>
                </Header>
                <Content padder>
                    <Card>
                        <CardItem bordered style={card.cardTitle}>
                            <Text style={common.titleText}>{oneCardTitle}</Text>
                        </CardItem>
                        <CardItem bordered style={card.body}>
                            <Text style={common.bodyText}>{oneCardBody} </Text>
                            <Input style={[common.textInput, {borderColor:'#365896'}]}
                                   value={this.state.idUser}
                                   returnKeyType={"next"}
                                   onChangeText={idUser => this.setState({idUser: idUser})}/>
                        </CardItem>
                        <CardItem style={{flexDirection:'row-reverse'}}>
                            <Text style={[common.bodyText,{color:'red'}]}> {this.state.visitDate} </Text>
                        </CardItem>
                        <CardItem footer bordered>
                            <Button onPress={this.getProjectFromServer}>
                                <Text style={common.titleText}>{oneCardbuttonText}</Text>
                                <Icon name='download' fontSize='16'/>
                            </Button>
                        </CardItem>
                    </Card>
{/*                    <Card>
                        <CardItem bordered style={card.cardTitle}>
                            <Text style={common.titleText}>{twoCardTitle}</Text>
                        </CardItem>
                        <CardItem bordered style={card.body}>
                            <Text style={common.bodyText}>{twoCardBody}</Text>
                        </CardItem>
                        <CardItem footer bordered>
                            <Button>
                                <Text style={common.titleText}>{twoCardbuttonText}</Text>
                                <Icon name='refresh' fontSize='16'/>
                            </Button>
                        </CardItem>
                    </Card>*/}
                    <Overlay
                        isVisible={this.state.showOverly}
                        windowBackgroundColor="rgba(100,100,100,0.8)">
                        <View>
                            <ActivityIndicator size="small" color="#0000ff"
                                               style={{opacity: this.state.showOverly ? 1.0 : 0.0}}
                                               animating={true}/>
                            <Text style={common.bodyText}>در حال دریافت پروژه ها . . .</Text>
                            <Text style={{opacity: this.state.showGetProjectsFromServer ? 1.0 : 0.0}}>{this.state.textOfProjectFromServer}</Text>
                            <Button onPress={this.setStateToFalse}>
                                <Text>بستن</Text>

                            </Button>
                        </View>
                    </Overlay>
                </Content>
            </Container>
        )
    }

    setStateToFalse=()=>{
        this.setState({showOverly: false});
        this.setState({showGetProjectsFromServer: false});
        this.setState({ textOfProjectFromServer:''});
    }


    getProjectFromServer = () => {
        // this.setState({showOverly: true});
        let remember_Token = this.props.user.remember_Token;
        NetInfo.getConnectionInfo().then((connectionInfo) => {
            if (connectionInfo.type != 'none') {
                this.fetchProjectsFromApi(remember_Token);
            } else {
                alert("دستگاه خودرا به اینترنت وصل کنید")
            }
        });


    }

    async fetchProjectsFromApi() {
        const formData = new FormData();
        formData.append('visit', 1);
        url = 'http://aradebartar.ir/hospital/api/visits/'+this.state.idUser;
        try {
            let response = await axios({
                url: url,
                method: 'Post',
                data: formData,
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data'
                }
            });
            let responseJson = await response.data;
            console.log("responseJson :" ,responseJson.visit.id)
            if (responseJson) {
                console.log("sdfsdfsdfsdf");
                this.setState({visitDate:'تاریخ مراجعه شما برابر است با :' + responseJson.visit.visit_date});
            }
            else {
                this.setState({visitDate:'برای این کاربر تاریخ مراجعه یافت نشد'});

            }
        } catch (error) {
            console.error(error);
        }
    }
}


const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}
const mapDispathToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(SyncWithServer);
