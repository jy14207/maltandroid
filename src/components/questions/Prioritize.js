import React from 'react';
import {Container, Header, Title, Right, Left, Content, Card, CardItem, Body, Input, Item, Button} from 'native-base';

import {
    Text,
    StyleSheet,
    ScrollView,
    View,
    Dimensions,
    TextInput,
    ProgressBarAndroid,
    Linking,
    ImageBackground,
    TouchableOpacity,
    Animated,
    Easing,
    Image,
    Platform,
    FlatList
} from 'react-native';
import SortableList from 'react-native-sortable-list';

const window = Dimensions.get('window');
import Modal from 'react-native-modalbox';
import * as Progress from 'react-native-progress';

import Snackbar from 'react-native-snackbar';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome5'

import {form} from "./../../assets/styles/index";
import {card} from "../../assets/css/SyncWithServer";
import {common} from './../../assets/css/common';
import {modal} from './../../assets/css/modal'

import {questionStyle} from '../../assets/css/questionStyle'
import {questionLoadFunc} from './QuestionLoad';
import {storeAnswerToDB} from './../database/answer';
import {getAnswerPromiseOfQuestionRow} from './../database/answer'
import QuestionHeader from "../ui/QuestionHeader";
import StoreButton from "../ui/StoreButton";

class Prioritize extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            row: [],
            showSubmitCheck:false,
            answer: [],
            socialPolicyIs: false,
            stopPolicyIs: false,
            countPolicyIs: false,
            rowNumber: 0,
            percentage: 0,
            questionCount: 0,
            text: '',
            error: '#898c8c',
            update: false,
            releaseData: null,
            optionData:[] ,
            dataForFlatList: [],
            noChange: false,

        }
    }

    componentWillMount() {
        var tempOptionData = [];
        for (let i = 0; i < this.props.options.length; i++) {
            tempOptionData.push({
                label: this.props.options[i].textOption,
                idInHost: this.props.options[i].idInHost,
                moreExplanation: this.props.options[i].moreExplanation,
            });

        }


        if (this.props.answer != null) {
            let ansArray = this.props.answer.idOption.split(',');
            tempOptionData = tempOptionData.sort((a, b) => ansArray.indexOf(a.idInHost.toString()) - ansArray.indexOf(b.idInHost.toString()))
            this.setState({
                dataForFlatList: tempOptionData,
                noChange:  true,
                rowNumber: this.props.rowNumber,
            });
        }
        else {
            this.setState({
                optionData: tempOptionData,
                rowNumber: this.props.rowNumber,
            });
        }
    }

    componentDidMount() {
        console.log(this.props.answer);
        this.setState({
            row: this.props.row,
            answer: this.props.answer,
            socialPolicyIs: this.props.socialPolicyIs,
            stopPolicyIs: this.props.stopPolicyIs,
            countPolicyIs: this.props.countPolicyIs,
            percentage: this.props.percentage,
            questionCount: this.props.questionCount,
            rowNumber: this.props.rowNumber,


        });
        if (this.props.answer != null) {
            this.setState({
                update: true,
                text: this.props.answer['text'],
            })
        }
    }

    jumpToQuestion = () => {
        var idProject = this.props.project.idInHost;
        var idPorseshnameh = this.props.porseshnameh.id;
        var rowNumber = this.state.rowNumber;
        if (this.state.rowNumber != 0 && this.state.rowNumber != null && this.state.rowNumber != '') {
            console.log('rowNumberInJump:', this.state.rowNumber)
            questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, this.state.rowNumber)
        }
        else {
            alert("شماره سوال را وارد کنید.")
        }
    };

    storeAnswer = async () => {
        if (this.state.dataForFlatList == '' && this.state.row.allowNull == 'no') {
            this.setState({error: 'red'});
            alert('جواب به این سوال الزامی می باشد')
        }
        else {
            let concatArrays=[];
            if (this.state.noChange == true)
                 concatArrays = this.state.dataForFlatList.concat(this.state.dataForFlatList);
            else
                 concatArrays = this.state.dataForFlatList.concat(this.state.optionData);

            console.log("concatArrays: ", concatArrays);
            let ans = (concatArrays.map(({idInHost}) => idInHost));
            console.log("ans:", ans)
            this.setState({maximumRange: false});
            this.setState({error: '#898c8c'});
            var idQuestion = this.state.row.idInHost;
            let idPorseshnameh = this.props.porseshnameh.id;
            if (this.state.update === false)
               await storeAnswerToDB(idPorseshnameh, idQuestion, ans.toString(), null, 'insert');
            else
               await storeAnswerToDB(idPorseshnameh, idQuestion, ans.toString(), null, 'update', this.state.answer['id']);

            this.setState({showSubmitCheck:true});
            let rowNumber = (Number(this.state.rowNumber) + 1);
            questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, rowNumber);
        }
    };
    loadPreviousQuestion = () => {
        rowNumber = (Number(this.state.rowNumber) - 1);
        questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, rowNumber);

    };

    _popFromOriginalOption = (index, item) => {
        let popedData = this.state.optionData;
        popedData.splice(index, 1);
        this.setState({optionData: popedData})
        console.log(item);
        let joined = this.state.dataForFlatList.concat(item);
        this.setState({dataForFlatList: joined,noChange:false});
    }
    _pushToOriginalOption = (index, item) => {
        let pushData = this.state.dataForFlatList;
        pushData.splice(index, 1);
        this.setState({dataForFlatList: pushData});
        let joined = this.state.optionData.concat(item);
        this.setState({optionData: joined,noChange:false});
    }

    render() {
        var errorMessage = '';
        var progressColor = 'orange';
        if (this.state.percentage / 100 > 50) {
            progressColor = "green"
        }

        var policy = false, socialPolicyMessage = false, stopPolicyIsMessage = false, countPolicyIsMessage = false;
        if (this.state.socialPolicyIs == true) {
            socialPolicyMessage = 'جواب این سوال در تعیین طبقه اجتماعی موثر است.';
            policy = true;
        }
        if (this.state.stopPolicyIs == true) {
            stopPolicyIsMessage = 'سیاست توقف مصاحبه برای این سوال لحاظ شده است.';
            policy = true;
        }
        if (this.state.countPolicyIs == true) {
            countPolicyIsMessage = 'سیاست تعداد خاص از پرسشنامه برای این سوال لحاظ شده است.';
            policy = true;
        }

        var policyansToOtherTextOptionsBool = false, policyansToOtherTextOptions = '';
        if (this.props.policyansToOtherTextOptions != '') {
            policyansToOtherTextOptions = this.props.policyansToOtherTextOptions;
            policyansToOtherTextOptionsBool = true;
        }


        const componenetTitle =this.props.project.subject;
        const oneCardbuttonText = 'ثبت';
        return (
            <Container>
                <ImageBackground
                    source={require('./../../assets/images/AppImage/question.jpg')}
                    style={common.fullImageBackground}
                    imageStyle={{borderRadius: 2}}>
                    <QuestionHeader
                        myOnPress={this.loadPreviousQuestion}
                        project={this.props.project}
                        modal={this.refs.modal4}
                    />
                    <Content padder style={{marginTop: 20}}>


                        {policy ? <View hide={policy} style={common.questionPolicyAlert}>
                            {socialPolicyMessage ?
                                <Text style={common.questionPolicyTextAlert}> - {socialPolicyMessage}</Text> : null}
                            {stopPolicyIsMessage ?
                                <Text style={common.questionPolicyTextAlert}> - {stopPolicyIsMessage}</Text> : null}
                            {countPolicyIsMessage ?
                                <Text style={common.questionPolicyTextAlert}> - {countPolicyIsMessage}</Text> : null}
                        </View> : null}
                        <View style={{height: "100%", flex: 1}}>
                            <Card style={card.mainCard}>
                                <CardItem bordered style={card.cardItemHeader}>
                                    <Text
                                        style={[card.cardTitle, common.titleText]}>{this.state.row.rowNumber + "- " + this.state.row.text}
                                        <Text
                                            style={[card.cardTitle, common.moreExplanationText]}>{this.state.row.moreExplanation}</Text>
                                        {policyansToOtherTextOptionsBool ?
                                            <Text
                                                style={[card.cardTitle, common.ansToOtherQTextStyle]}>/جواب هایی که به سوال های قبل داده اید : {policyansToOtherTextOptions}</Text>
                                            : null}</Text>
                                </CardItem>
                                <CardItem style={[card.body, {
                                    padding: 5,
                                    borderWidth: 1,
                                    borderColor: '#c3c7c7',
                                }]}>
                                    <FlatList
                                        style={{paddingBottom: 20}}
                                        data={this.state.dataForFlatList}
                                        keyExtractor={(item, index) => index.toString()}
                                        renderItem={({item, index}) =>
                                            <View style={[
                                                styles.row,
                                            ]}>
                                                <View style={{
                                                    flexDirection: 'row-reverse',
                                                    justifyContent: 'center',
                                                    alignItems: 'center'
                                                }}>
                                                    <View style={{flexDirection: 'column', width: '100%'}}
                                                          onPress={() => this._pushToOriginalOption(item, index)}>
                                                        <Text onPress={() => this._pushToOriginalOption(index, item)}
                                                              style={styles.text}>{item.label}</Text>
                                                        <Text style={[questionStyle.optionMoreExplentionStyle]}
                                                              onPress={() => this._pushToOriginalOption(index, item)}>{item.moreExplanation}</Text>
                                                    </View>
                                                </View>
                                                {/*<Image source={{uri: data.label}} style={styles.image}/>*/}

                                            </View>
                                        }/>

                                </CardItem>
                                <CardItem style={[card.body, {paddingBottom: 0}]}>
                                    {/*<PrioritizeFlatList data={this.state.optionData} popFromData={this._popFromData}/>*/}
                                    {/* <ScrollView>
                                        <SortableList
                                            style={styles.list}
                                            contentContainerStyle={styles.contentContainer}
                                            data={this.state.optionData}
                                            renderRow={this._renderRow}
                                            onReleaseRow={(key, order, finalData) => {
                                                this.setState({releaseData: finalData});
                                                console.log("finalData :", finalData)
                                            }}/>
                                    </ScrollView>*/}
                                    <FlatList
                                        style={{paddingBottom: 20}}
                                        data={this.state.optionData}
                                        keyExtractor={(item, index) => index.toString()}
                                        renderItem={({item, index}) =>
                                            <View style={[
                                                styles.row,
                                            ]}>
                                                <View style={{
                                                    flexDirection: 'row-reverse',
                                                    justifyContent: 'center',
                                                    alignItems: 'center'
                                                }}>
                                                    <View style={{flexDirection: 'column', width: '100%'}}
                                                          onPress={() => this._popFromOriginalOption(item, index)}>
                                                        <Text onPress={() => this._popFromOriginalOption(index, item)}
                                                              style={styles.text}>{item.label}</Text>
                                                        <Text style={[questionStyle.optionMoreExplentionStyle]}
                                                              onPress={() => this._popFromOriginalOption(index, item)}>{item.moreExplanation}</Text>
                                                    </View>
                                                </View>
                                                {/*<Image source={{uri: data.label}} style={styles.image}/>*/}

                                            </View>
                                        }/>
                                </CardItem>
                                <CardItem footer bordered>
                                    <StoreButton onPress={this.storeAnswer} showSubmitCheck={this.state.showSubmitCheck}/>
                                </CardItem>

                            </Card>
                        </View>
                    </Content>
                    <Modal style={[modal.modal, modal.modal4]} position={"bottom"} ref={"modal4"}>
                        <Card style={modal.modalMainCard}>
                            <CardItem bordered style={card.cardTitle}>
                                <Text style={[common.headerText, {color: 'white'}]}>اطلاعات بیشتر . . .</Text>
                            </CardItem>
                            <CardItem style={{flexDirection: 'column'}}>
                                <Text>{this.state.percentage}% - <Text
                                    style={common.bodyText}>( {this.state.row.rowNumber}
                                    از {this.state.questionCount})</Text></Text>
                                <Progress.Bar progress={this.state.percentage / 100} width={300}
                                              color={(this.state.percentage / 100) < 0.5 ? "red" : "green"}
                                              height={30}></Progress.Bar>
                            </CardItem>
                            <CardItem style={card.modalCardItem2}>
                                <Button info style={[common.bodyText, modal.modalButton]} onPress={this.jumpToQuestion}><Text
                                    onPress={this.jumpToQuestion} style={common.bodyText}> پرش </Text></Button>
                                <Input
                                    style={[common.textInput, modal.modalInput]}
                                    keyboardType='numeric'
                                    onSubmitEditing={()=>this.jumpToQuestion()}
                                    placeholder='شماره سوال؟'
                                    onChangeText={val => this.setState({rowNumber: val})}/>
                            </CardItem>
                            <View style={card.modalView}>
                                <Text style={[common.bodyText]}>بیشتر بدانید :
                                    <Text style={common.webLink} onPress={() => {
                                        Linking.openURL(this.props.user.webSiteLink)
                                    }}>{this.props.user.companyName} </Text>
                                </Text>
                            </View>
                        </Card>
                    </Modal>
                </ImageBackground>
            </Container>

        )
    }
}


const styles = StyleSheet.create({

    title: {
        fontSize: 13,
        // paddingVertical: 20,
        color: '#999999',
    },
    list: {
        flex: 1,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        padding: 5,
        alignItems: 'center',
        backgroundColor: '#ededed',
        minHeight: 40,
        flex: 1,
        marginBottom: 5,
        borderRadius: 2,
        marginRight: 10,
        marginLeft: 10,
    },
    text: {
        fontSize: 13,
        color: '#222222',
        fontFamily: 'IRANSansMobile'
    },
});


const mapStateToProps = (state) => {
    return {
        porseshnameh: state.porseshnameh,
        project: state.project,
        user: state.user
    }
}
const mapDispathToProps = dispatch => {
    return {
        setPorseshnameh: porseshnameh => {
            dispatch(setPorseshnameh(porseshnameh))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(Prioritize);
