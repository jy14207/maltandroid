import React from 'react'
import {NetInfo} from 'react-native';

 export async function checkNetAsync() {
     return await NetInfo.getConnectionInfo();
}
