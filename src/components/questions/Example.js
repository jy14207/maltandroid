import React from 'react'
import {Text} from 'react-native';

import DraggableList from 'react-native-draggable-list';


export default class Example extends React.Component {
    constructor() {
        super();

        this.state = {
            activeBlock: null,
            itemsPerRow: 1,
            itemHeight: 150,
            data: [], // Your data goes here
        };
    }

    // MARK: - Handle events

    onDragRelease(newData) {
        this.setState({ activeBlock: null, data: newData });
    }

    onDragMove(newData) {
        this.setState({ data: newData });
    }

    onDragGrant(index) {
        this.setState({ activeBlock: index });
    }

    // MARK: - Render elements

    renderItem(item) {
        return (
            <View key={item.name}> // this key has to be the same as the keyField in the DraggableList
                ...
            </View>
        );
    }

    renderLastItem() {
        return (
            <Text>123</Text>
        );
    }

    render() {
        const {
            data, activeBlock, itemHeight, itemsPerRow,
        } = this.state;
        // const lastItem = data.length === ALL_DATA.length ? undefined : this.renderLastItem();

        return (

        <DraggableList
            onDragRelease={newData => this.onDragRelease(newData)}
            onDragMove={newData => this.onDragMove(newData)}
            onDragGrant={index => this.onDragGrant(index)}
            lastItem={() => this.renderLastItem()}
            itemHeight={itemHeight}
            itemsPerRow={itemsPerRow}
            data={data}
            keyField="name" // this key has to be the same has the item view in  renderItem
            renderItem={item => this.renderItem(item)}
        />
    );
    }
}