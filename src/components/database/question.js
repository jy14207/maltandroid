import {
    selectOneQuestion, getQuestionCount, getQuestionHaveImageFromDB, getQuestionByQTypeFromDB,
    getQByIdInHostFromDB
} from './Model'

export const getQuestion =(idProject,rowNumber,cb)=>{
    selectOneQuestion(idProject,rowNumber,(result)=>{
        cb(result);
    });
}

export const getQPromise =(idProject,rowNumber)=>{
    console.log("idProject :",idProject)
    console.log("rowNumber :" ,rowNumber)
    return new Promise((resolve,reject)=>{
        selectOneQuestion(idProject,rowNumber,(result)=>{
            resolve( result.rows.item(0));
        });
    });
}

export const getQuestionCountPromise=(idProject)=>{
    return new Promise((resolve,reject)=>{
        getQuestionCount(idProject,(result)=>{
            resolve(result.rows.item(0));
        })
    })
}
export const getQuestionHaveImagePromise=(idProject)=>{
    return new Promise((resolve,reject)=>{
        getQuestionHaveImageFromDB(idProject,(result)=>{
            // console.log("getQuestionHaveImagePromise :" ,result.rows);
            resolve(result.rows);
        })
    })
}

export const getQuestionByQTypePromise=(idProject,idQuestionType)=>{
    return new Promise((resolve,reject)=>{
        getQuestionByQTypeFromDB(idProject,idQuestionType,(result)=>{
            resolve(result.rows);
        })
    })
}
export const getQuestionByIDPromise = (idQuestionInHost)=>{
    return new Promise((resolve,reject)=>{
        getQByIdInHostFromDB(idQuestionInHost,(result)=>{
            console.log("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa: ",result);
            resolve(result.rows);
        })
    })
}

