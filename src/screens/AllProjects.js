import React from 'react';
import {FlatList, ScrollView, ActivityIndicator, NetInfo, TouchableOpacity, ImageBackground} from 'react-native';
import {
    Container,
    Header,
    Right,
    Left,
    Content,
    View,
    Button,
    Text,
    Icon,
    Card,
    CardItem,
    Body,
    Title
} from 'native-base';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Overlay} from 'react-native-elements';
import axios from 'react-native-axios'


import {form} from "./../assets/styles/index";
import {common} from '../assets/css/common';

import {projectList, insertToUserProject, deleteProject} from "./../components/database/project";
import {insertProjectToDatabese} from './../components/database/project';

import ProjectFlatList from "../components/flatLists/ProjectFlatList";

class AllProject extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            flatListItem: [],
            showOverly: false,
            showGetProjectsFromServer: false,
            textOfProjectFromServer: '',
        }
        this._loadProject();

    }

    _loadProject = () => {
        console.log('idUser:', this.props.user.idInHost);
        projectList(this.props.user.idInHost, (projectListResult) => {
            var temp = [];
            // console.log(projectListResult);
            for (let i = 0; i < projectListResult.rows.length; ++i) {
                temp.push(projectListResult.rows.item(i));
            }
            this.setState({
                flatListItem: temp,
            });
            // console.log(this.state.flatListItem);
        });
    }

    render() {
        return (
            <Container>
                <ImageBackground
                    source={require('./../assets/images/AppImage/allProject.jpg')}
                    style={{
                        flex: 1,
                        width: '100%',
                    }}
                    imageStyle={{borderRadius: 2}}>
                <Header style={form.headerStyle}>

                    <Left>
                        <TouchableOpacity style={{flexDirection: 'row'}}>
                            <Icon name='refresh' onPress={this.getProjectFromServer}
                                  style={{fontWeight: 500, color: 'white'}}/>
                        </TouchableOpacity>
                    </Left>
                    <Right>
                        <Title style={[common.headerText, {paddingRight: 5}]}>پروژه های فعال</Title>
                        <TouchableOpacity style={{flexDirection: 'row'}}>
                            <Icon name='menu' onPress={() => Actions.drawerOpen()}
                                  style={{fontWeight: 500, color: 'white'}}/>
                        </TouchableOpacity>
                    </Right>
                </Header>
                <Content style={{padding: 10, backgroundColor: '##f7f1e3'}}>
                        <ScrollView style={{backgroundColor: '##f7f1e3'}}>
                            <ProjectFlatList flatListItem={this.state.flatListItem}/>
                        </ScrollView>

                </Content>
                <Overlay
                    isVisible={this.state.showOverly}
                    windowBackgroundColor="rgba(100,100,100,0.8)">
                    <View>
                        <ActivityIndicator size="small" color="#0000ff"
                                           style={{opacity: this.state.showOverly ? 1.0 : 0.0}}
                                           animating={true}/>
                        <Text style={common.bodyText}>در حال دریافت پروژه ها . . .</Text>
                        <Text
                            style={{opacity: this.state.showGetProjectsFromServer ? 1.0 : 0.0}}>{this.state.textOfProjectFromServer}</Text>
                        <Button onPress={this.setStateToFalse}>

                            <Text>بستن</Text>

                        </Button>
                    </View>
                </Overlay>
                </ImageBackground>

            </Container>
        )
    }

    setStateToFalse = () => {
        this.setState({showOverly: false});
        this.setState({showGetProjectsFromServer: false});
        this.setState({textOfProjectFromServer: ''});
        this._loadProject();
        // Actions.reset('root');

    }

    getProjectFromServer = () => {
        this.setState({showOverly: true});
        let remember_Token = this.props.user.remember_Token;
        NetInfo.getConnectionInfo().then((connectionInfo) => {
            if (connectionInfo.type != 'none') {
                // this._deleteUserProjectFromDB(this.props.user.idInHost);
                this.fetchProjectsFromApi(remember_Token).then(() => this._loadProject());
            } else {
                alert("دستگاه خودرا به اینترنت وصل کنید")
            }
        });
    }

    _deleteUserProjectFromDB(idUser) {
        deleteProject(idUser);
    }

    async fetchProjectsFromApi(remember_Token) {
        // console.log('FETCH FROM API')
        const formData = new FormData();
        formData.append('remember_Token', remember_Token);
        url =this.props.globalVariables.homeURL + '/index.php?r=login/androidGetUserProjects';
        try {
            let response = await axios({
                url: url,
                method: 'POST',
                data: formData,
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data'
                }
            });
            let responseJson = await response.data;
            let userPreojects = responseJson;
            if (userPreojects.length > 0) {
                let idUser = this.props.user.idInHost;
                let len = userPreojects.length;
                let textLen = 'تعداد ' + len + '  پروژه با موفقیت دریافت شد.';
                this.setState({textOfProjectFromServer: textLen})
                this.setState({showGetProjectsFromServer: true});
                // Delete Current Project Then Insert New Project
                deleteProject(idUser).then(userPreojects.forEach(function (item) {
                    // console.log("Project Recived From Server:",item);
                    insertProjectToDatabese(item, idUser);
                }))

            }
            else {
                alert(userPreojects.message);
                this.setState({showOverly: false});
            }
        } catch (error) {
            console.error(error);
        }
    }

    //===========================================================================  USER PROJECTS
    /*    _deleteUserProjectFromDB(idUser){
            deleteUserProject(idUser);
        };
        async _fetchUserProjectFromApi( remember_Token ){
            const formData = new FormData();
            formData.append('remember_Token', remember_Token);
            url = 'http://aradebartar.ir/porseshname/index.php?r=api/projects/androidGetUserProject';
            try {
                let response = await axios({
                    url: url,
                    method: 'POST',
                    data: formData,
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'multipart/form-data'
                    }
                });
                let responseJson = await response.data;
                // console.log('userPPP: ' ,responseJson);
                if (responseJson.length > 0) {
                    responseJson.forEach(function (item) {
                        console.log(item);
                        insertToUserProject(item);
                    })
                }
                else {
                }
            } catch (error) {
                console.error(error);
            }
        }*/
}


const mapStateToProps = (state) => {
    return {
        user: state.user,
        globalVariables:state.globalVariables,

    }
}
const mapDispathToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(AllProject);
