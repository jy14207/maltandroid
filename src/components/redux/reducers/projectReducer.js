import {SET_PROJECT} from "../actions/type";

const initialstate = {
    id:null,
    idInHost:null,
    idCompany:null,
    subject:null,
    startDate:null,
    endDate:null,
    questionunmer:null,
    pasokhgooNumber:null,
    status:null,
    softDelete:null,
}

export default project = (state = initialstate, action = {}) => {
    switch (action.type){
        case SET_PROJECT:
            const {project} =action;
            // console.log(user);
            return{
                id:project.id,
                idInHost:project.idInHost,
                idCompany:project.idCompany,
                subject:project.subject,
                startDate:project.startDate,
                endDate:project.endDate,
                questionunmer:project.questionunmer,
                pasokhgooNumber:project.pasokhgooNumber,
                status:project.status,
                softDelete:project.softDelete,
            }
            break;

        default:
            return state;
    }
}