import EstyleSheet from 'react-native-extended-stylesheet';

export const questionStyle = EstyleSheet.create(
    {
        openQInput: {
            fontFamily: '$IS',
            fontSize: 14,
            height: 70,
            padding: 0,
            borderWidth: 1,
            borderRadius: 3,
            borderColor: '#898c8c',
            color: 'black',
            marginBottom: 10,
        },
        optionsStyle: {},
        questionMoreExplentionStyle: {},
        optionMoreExplentionStyle: {
            marginRight: 3,
            color: 'red',
            fontSize: 10,
            fontFamily: 'IRANSansMobile'
        }
    }
)