import React from 'react';
import {TouchableOpacity, Button, Text} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import {form} from '../../assets/styles/index'

export default class StoreButton extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const oneCardbuttonText = 'ثبت';
        return (
            <TouchableOpacity style={form.submitButton} onPress={this.props.onPress}>
                <Text style={[form.submitText]}
                      onPress={this.props.onPress}>{oneCardbuttonText}</Text>
                {this.props.showSubmitCheck ?
                    <Icon name='check' style={form.submitCheckIcon} size={15} color="#ffffff"  onPress={this.props.onPress}/> : null}
            </TouchableOpacity>
        )
    }
}
