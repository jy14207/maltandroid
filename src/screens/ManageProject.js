import React from 'react'
import {NetInfo, ImageBackground, ScrollView, ActivityIndicator, TouchableOpacity, Image} from 'react-native';
import {
    Container,
    View,
    Header,
    Right,
    Left,
    Content,
    Button,
    Text,
    Icon,
    Card,
    CardItem,
    Body,
    Title
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import {Overlay} from 'react-native-elements';
import axios from 'react-native-axios'

import {form} from "./../assets/styles/index";
import {common} from './../assets/css/common';

import {
    deleteSEC,
    insertToSEC,
    insertToUserProject,
    deleteUserProject,
    deleteProject
} from './../components/database/project';
import {deleteCityOfThisProject, insertCitiesToDatabese} from './../components/database/city'
import {getQuestionHaveImagePromise, getQuestionByQTypePromise} from './../components/database/question'
import {downloadFile, mkDir} from './../components/classes/WorkByFile'
import {
    deletePolicyOfThisProject,
    insertPolicySetToDatabese,
    deletePolicyListOfThisProject,
    insertPolicyListToDatabese
} from './../components/database/policySet';
import {
    deleteQuestionFromDB,
    deleteOptionsFromDBS,
    insertQuedtion,
    selectAllQuestion,
    insertOptionsToDB,
    deleteQuestionType,
    insertQuestionTypeToDB
} from './../components/database/questionAndOption'
import EstyleSheet from 'react-native-extended-stylesheet'
import {getAllPorseshnamehFromDB, updateUploadToHostAfterUploadPorseshname} from "../components/database/porseshnameh";
import {uploadPorseshnameh} from "../components/classes/UploadPorseshname";

class ManageProject extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showOverly: false,
            showGetCitiessFromServer: false,
            textOfCitiesFromServer: '',
            textOfPolicySetFromServer: '',
            showGetPolicySetFromServer: false,
            textOfQuestionFromServer: '',
            showGetQuestionFromServer: false,
            textOfOptionFromServer: '',
            showGetOptionFromServer: false,
            isDone: false,
            showDownloadNumberOfImagesFromApi: false,
            textOfDownloadNumberOfImagesFromApi: '',
            textOfDownloadNumberOfVideoQtypeFromApi: '',
            showDownloadNumberOfVideoQtypeFromApi: false,
            textOfDownloadNumberOfImageQtypeFromApi: '',
            showDownloadNumberOfImageQtypeFromApi: false,
            textOfDownloadNumberOfAudioQtypeFromApi: '',
            showDownloadNumberOfAudioQtypeFromApi: false,
            showOverlySend:false,
            showOverlySendText:false,
            textOfSendToServer:''



        }
    }

    componentDidMount() {
        mkDir('/public/imagesDownload/question')
        mkDir('/public/imagesDownload/options')
    }

    render() {
        /*const preview = this.state.isDone ? (<View>
                <Image style={{
                    width: 100,
                    height: 100,
                    backgroundColor: 'black',
                }}
                       source={{
                           uri: `file://${RNFS.DocumentDirectoryPath}/123.jpeg`,
                           scale: 1
                       }}
                />
                <Text>{`file://${RNFS.DocumentDirectoryPath}/123.jpeg`}</Text>
            </View>
        ) : null;*/
        return (
            <Container>
                <ImageBackground
                    source={require('./../assets/images/AppImage/projectManagement.jpg')}
                    style={{
                        flex: 1,
                        width: '100%',
                    }}
                    imageStyle={{borderRadius: 2}}>
                    <Header style={form.headerStyle}>
                        {/*                    <Left>
                        <Text style={[form.headerText, common.headerText]} onPress={() => Actions.jump('settings')}>تنظیمات</Text>
                    </Left>*/}
                        <Right>
                            <Title style={common.headerText}>{this.props.project.subject}</Title>
                            <TouchableOpacity style={{flexDirection: 'row'}}>
                                <Icon name='arrow-forward' onPress={() => Actions.replace('allProject')}
                                      style={{fontWeight: 500, color: 'white', marginLeft: 10}}/>
                            </TouchableOpacity>
                        </Right>
                    </Header>
                    <Content>
                        <ScrollView>
                            <View style={manageProjectCss.container}>
                                <View style={manageProjectCss.box0}>
                                    <Card bordered>
                                        <CardItem button
                                                  style={manageProjectCss.cardItem}
                                                  onPress={() => Actions.replace('porseshnamehManagement')}
                                        >
                                            <Body style={manageProjectCss.tabBody}>
                                            <Text style={[manageProjectCss.tabBodyText, common.bodyText]}
                                                  onPress={() => Actions.replace('porseshnamehManagement')}
                                            >مدیریت پرسشنامه ها</Text>
                                            </Body>
                                        </CardItem>
                                    </Card>
                                </View>
                            </View>
                            <View style={manageProjectCss.container}>
                                <View style={manageProjectCss.box1}>
                                    <Card bordered>
                                        <CardItem button
                                                  style={manageProjectCss.cardItem}
                                                  onPress={this._getQuestionsFromApi}
                                        >
                                            <Body style={manageProjectCss.tabBody}>
                                            <Text style={[manageProjectCss.tabBodyText, common.bodyText]}
                                                  onPress={this._getQuestionsFromApi}
                                            >دریافت سوالات</Text>
                                            </Body>
                                        </CardItem>
                                    </Card>
                                </View>
                                <View style={manageProjectCss.box2}>
                                    <Card bordered>
                                        <CardItem button
                                                  style={manageProjectCss.cardItem}
                                                  onPress={() => this._uploadPorseshnamehToHost()}
                                        >
                                            <Body style={manageProjectCss.tabBody}>
                                            <Text style={[manageProjectCss.tabBodyText, common.bodyText]}
                                                  onPress={() => this._uploadPorseshnamehToHost()}
                                            >ارسال پرسشنامه ها</Text>
                                            </Body>
                                        </CardItem>
                                    </Card>
                                </View>
                            </View>
                            <View style={manageProjectCss.container}>
                                <View style={manageProjectCss.box1}>
                                    <Card bordered>
                                        <CardItem button
                                                  style={manageProjectCss.cardItem}

                                        >
                                            <Body style={manageProjectCss.tabBody}>
                                            <Text style={[manageProjectCss.tabBodyText, common.bodyText]}
                                            >خلاصه وضعیت </Text>
                                            </Body>
                                        </CardItem>
                                    </Card>
                                </View>
                                <View style={manageProjectCss.box2}>
                                    <Card bordered>
                                        <CardItem button
                                                  style={manageProjectCss.cardItem}
                                            // onPress={() => this.getSelectedProjectFromDB(item.id)}
                                        >
                                            <Body style={manageProjectCss.tabBody}>
                                            <Text style={[manageProjectCss.tabBodyText, common.bodyText]}
                                                // onPress={() => this.getSelectedProjectFromDB(item.id)}
                                            >گزارشات</Text>
                                            </Body>
                                        </CardItem>
                                    </Card>
                                </View>
                            </View>
                        </ScrollView>
                        <Overlay
                            isVisible={this.state.showOverly}
                            windowBackgroundColor="rgba(100,100,100,0.8)">
                            <View>
                                <ActivityIndicator size="small" color="#0000ff"
                                                   style={{opacity: this.state.showOverly ? 1.0 : 0.0}}
                                                   animating={true}/>
                                <Text style={common.bodyText}>در حال دریافت بخش های پروژه . . .</Text>
                                <Text
                                    style={{opacity: this.state.showGetCitiessFromServer ? 1.0 : 0.0}}>{this.state.textOfCitiesFromServer}</Text>
                                <Text
                                    style={{opacity: this.state.showGetPolicySetFromServer ? 1.0 : 0.0}}>{this.state.textOfPolicySetFromServer}</Text>
                                <Text
                                    style={{opacity: this.state.showGetQuestionFromServer ? 1.0 : 0.0}}>{this.state.textOfQuestionFromServer}</Text>
                                <Text
                                    style={{opacity: this.state.showGetOptionFromServer ? 1.0 : 0.0}}>{this.state.textOfOptionFromServer}</Text>
                                <Text
                                    style={{opacity: this.state.showDownloadNumberOfImagesFromApi ? 1.0 : 0.0}}>{this.state.textOfDownloadNumberOfImagesFromApi}</Text>
                                <Text
                                    style={{opacity: this.state.showDownloadNumberOfVideoQtypeFromApi ? 1.0 : 0.0}}>{this.state.textOfDownloadNumberOfVideoQtypeFromApi}</Text>
                                <Text
                                    style={{opacity: this.state.showDownloadNumberOfImageQtypeFromApi ? 1.0 : 0.0}}>{this.state.textOfDownloadNumberOfImageQtypeFromApi}</Text>
                                <Text
                                    style={{opacity: this.state.showDownloadNumberOfAudioQtypeFromApi ? 1.0 : 0.0}}>{this.state.textOfDownloadNumberOfAudioQtypeFromApi}</Text>
                                <Button onPress={this._setStateToFalse}>
                                    <Text>بستن</Text>
                                </Button>
                            </View>
                        </Overlay>
                        <Overlay
                            isVisible={this.state.showOverlySend}
                            windowBackgroundColor="rgba(100,100,100,0.8)">
                            <View>
                                <ActivityIndicator size="small" color="#0000ff"
                                                   style={{opacity: this.state.showOverlySend ? 1.0 : 0.0}}
                                                   animating={true}/>
                                <Text style={common.bodyText}>در حال ارسال پرسشنامه ها . . .</Text>
                                <Text
                                    style={{opacity: this.state.showOverlySendText ? 1.0 : 0.0}}>{this.state.textOfSendToServer}</Text>
                                <Button onPress={this._setStateToFalse}>
                                    <Text>بستن</Text>
                                </Button>
                            </View>
                        </Overlay>
                    </Content>
                </ImageBackground>
            </Container>
        );
    }


    _setStateToFalse = () => {
        this.setState({
            showOverly: false,
            showGetCitiessFromServer: false,
            textOfCitiesFromServer: '',
            showGetOptionFromServer: false,
            showGetPolicySetFromServer: false,
            showGetQuestionFromServer: false,
            showDownloadNumberOfImagesFromApi: false,
            textOfPolicySetFromServer: '',
            textOfQuestionFromServer: '',
            textOfOptionFromServer: '',
            textOfDownloadNumberOfImagesFromApi: '',
            showOverlySend:false,
            showOverlySendText:false,
            textOfSendToServer:''
        });
    }

    _getQuestionsFromApi = () => {
        this.setState({showOverly: true});
        let idProject = this.props.project.idInHost;
        let remember_Token = this.props.user.remember_Token;


        NetInfo.getConnectionInfo().then((connectionInfo) => {
            if (connectionInfo.type != 'none') {
                // GET CITIES
                deleteCityOfThisProject(idProject)
                    .then(r => {
                        this._fetchCityFromApi(remember_Token, idProject);
                    });
                // GET POLICY SET
                deletePolicyOfThisProject(idProject)
                    .then(r => {
                        this._fetchPolicySetFromApi(remember_Token, idProject);
                    })

                // GET POLICY LIST
                deletePolicyListOfThisProject()
                    .then(r => {
                        this._fetchPolicyListFromApi(remember_Token);
                    });
                // GET QUESTIONS AND OPTIONS
                this._deleteQoestionsOptionsOfThisProjectFromDB(idProject).then(() => {
                    this._fetchQoestionsFromApi(remember_Token, idProject)
                        .then(() => {
                            console.log("در حال فچ کردن سوالات")
                            this._fetOptionsFromApi(remember_Token, idProject)
                                .then(() => {
                                    // Algortim Recive File
                                    // one :  SELECT idInHost from question where idProject= idProject and haveImage=Yes and download image question
                                    getQuestionHaveImagePromise(idProject).then((result) => {
                                        QhaveImg = [];
                                        for (i = 0; i < result.length; i++) {
                                            // console.log("result:" ,result.item(i));
                                            QhaveImg.push({
                                                fromUrl: this.props.globalVariables.homeURL + "/public/imagesUpload/question/" + result.item(i).idInHost + ".jpeg",
                                                toFile: "/public/imagesDownload/question/" + result.item(i).idInHost + ".jpeg"
                                            });
                                        }
                                        if (QhaveImg.length > 0) {
                                            downloadFile(QhaveImg, cb => {
                                                this.setState({
                                                    textOfDownloadNumberOfImagesFromApi: `تعداد ${cb} تصویر دریافت شد`,
                                                    showDownloadNumberOfImagesFromApi: true,
                                                });
                                            });
                                        }

                                    });

                                    // Two : SELECT videoUrl FROM QUESTION WHER IDqESTIONtYPE == 15  AND IDpROJECT ==idProject and download video from url
                                    getQuestionByQTypePromise(idProject, 15).then((result) => {
                                        QVideoType = [];
                                        for (i = 0; i < result.length; i++) {
                                            console.log("videoTypeQuestion:", result.item(i));
                                            QVideoType.push({
                                                fromUrl: result.item(i).videoURL,
                                                toFile: "/public/imagesDownload/question/" + result.item(i).idInHost + ".mp4"
                                            });
                                        }
                                        if (QVideoType.length > 0) {
                                            downloadFile(QVideoType, cbV => {
                                                this.setState({
                                                    textOfDownloadNumberOfVideoQtypeFromApi: `تعداد ${cbV} ویدئو دریافت شد`,
                                                    showDownloadNumberOfVideoQtypeFromApi: true,
                                                });
                                            });
                                        }
                                    });

                                    //  Download images Of ImageQuestionType
                                    getQuestionByQTypePromise(idProject, 16).then((result) => {
                                        QImageType = [];
                                        for (i = 0; i < result.length; i++) {
                                            // console.log("result:" ,result.item(i));
                                            let fileName = result.item(i).idProject.toString() + result.item(i).idQuestionType.toString() + result.item(i).rowNumber.toString() + ".jpeg";
                                            // console.log("fileName:", fileName);
                                            QImageType.push({
                                                fromUrl: this.props.globalVariables.homeURL + "/public/imagesUpload/question/" + fileName,
                                                toFile: "/public/imagesDownload/question/" + fileName,
                                            });
                                        }
                                        if (QImageType.length > 0) {

                                            downloadFile(QImageType, cb => {
                                                this.setState({
                                                    textOfDownloadNumberOfImageQtypeFromApi: `تعداد ${cb} سوال تصویر دار دریافت شد`,
                                                    showDownloadNumberOfImageQtypeFromApi: true,
                                                });
                                            });
                                        }
                                    });
                                    //  Download audio Of AudioQuestionType
                                    getQuestionByQTypePromise(idProject, 17).then((result) => {
                                        QAudioType = [];
                                        for (i = 0; i < result.length; i++) {
                                            console.log("result:", result.item(i));
                                            let fileName = result.item(i).idProject.toString() + result.item(i).idQuestionType.toString() + result.item(i).rowNumber.toString() + ".mp3";
                                            console.log("fileName:", fileName);
                                            QAudioType.push({
                                                fromUrl: this.props.globalVariables.homeURL + "/public/imagesUpload/question/" + fileName,
                                                toFile: "/public/imagesDownload/question/" + fileName,
                                            });
                                        }
                                        if (QAudioType.length > 0) {
                                            downloadFile(QAudioType, cb => {
                                                this.setState({
                                                    textOfDownloadNumberOfAudioQtypeFromApi: `تعداد ${cb} سوال صوتی دریافت شد`,
                                                    showDownloadNumberOfAudioQtypeFromApi: true,
                                                });
                                            });
                                        }
                                    });
                                })
                        });
                });

                //GET SEC FROM API
                deleteQuestionType(idProject)
                    .then(r => {
                        this._fetchQuestionType(remember_Token);
                    });

                //SEC
                deleteSEC(idProject)
                    .then(r => {
                        this._fetchSECFromApi(remember_Token, idProject);
                    });
                // USER PROJECT
                /*  deleteProject(this.props.user.idInHost)
                     .then(r=>{
                         this._fetchUserProjectFromApi(remember_Token);
                     });
                //GET IMAGE AND VIDEO FILE OF QUESTIONS AND OPTIONS*/
            } else {
                alert("دستگاه خودرا به اینترنت وصل کنید")
            }
        });
    }

    //=========================================================================================== CITIES OPRATIONS
    async _fetchCityFromApi(remember_Token, idProject) {
        const formData = new FormData();
        formData.append('remember_Token', remember_Token);
        formData.append('idProject', idProject);
        console.log("globalVariablesHomeURL :", this.props.globalVariables.homeURL)
        let url = this.props.globalVariables.homeURL + '/index.php?r=api/projects/androidGetCity';
        try {
            let response = await axios({
                url: url,
                method: 'POST',
                data: formData,
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data'
                }
            });
            let responseJson = await response.data;
            let citiesPreoject = responseJson;
            if (citiesPreoject.length > 0) {
                let len = citiesPreoject.length;
                let textLen = 'لیست شهرها بروز رسانی شد.';
                this.setState({textOfCitiesFromServer: textLen})
                this.setState({showGetCitiessFromServer: true});
                citiesPreoject.forEach(function (item) {
                    // console.log(item);
                    insertCitiesToDatabese(item);
                })
            }
            else {
                this.setState({textOfCitiesFromServer: citiesPreoject.message})
                this.setState({showGetCitiessFromServer: true});
            }
        } catch (error) {
            console.error(error);
        }
    }

    //=========================================================================================== POLICY SETS OPRATION
    _deletePolicySetFromDB(idProject) {
        deletePolicyOfThisProject(idProject);
    }

    async _fetchPolicySetFromApi(remember_Token, idProject) {
        const formData = new FormData();
        formData.append('remember_Token', remember_Token);
        formData.append('idProject', idProject);
        url = this.props.globalVariables.homeURL + '/index.php?r=api/projects/androidGetPolicySet';
        try {
            let response = await axios({
                url: url,
                method: 'POST',
                data: formData,
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data'
                }
            });
            let responseJson = await response.data;
            let policyPreoject = responseJson;
            if (policyPreoject.length > 0) {
                // console.log(this.state.textOfPolicySetFromServer);
                policyPreoject.forEach(function (item) {
                    // console.log('PolicySet:',item);
                    insertPolicySetToDatabese(item);
                })
            }
            else {
                /* this.setState({textOfPolicySetFromServer:policyPreoject.message})
                 this.setState({showGetPolicySetFromServer:true});*/
                // alert(policyPreoject.message);
                // this.setState({showOverly: false});
            }
        } catch (error) {
            console.error(error);
        }
    }

    //=========================================================================================== POLICY List OPRATION

    async _fetchPolicyListFromApi(remember_Token) {
        const formData = new FormData();
        formData.append('remember_Token', remember_Token);
        url = this.props.globalVariables.homeURL + '/index.php?r=api/projects/androidGetPolicyList';
        try {
            let response = await axios({
                url: url,
                method: 'POST',
                data: formData,
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data'
                }
            });
            let responseJson = await response.data;
            let policyPreoject = responseJson;
            if (policyPreoject.length > 0) {
                policyPreoject.forEach(function (item) {
                    // console.log('policy',item);
                    insertPolicyListToDatabese(item);
                })
            }
            else {
            }
        } catch (error) {
            console.error(error);
        }
    }

//=========================================================================== Question AND OPTIONS OPRATION
    _deleteQoestionsOptionsOfThisProjectFromDB = async (idProject) => {
        console.log("گزینه ها حذف شد")
        selectAllQuestion(idProject, (result) => {
            let len = result.rows.length;
            for (var i = 0; i < len; i++) {
                // console.log(result.rows.item(i).idInHost);
                deleteOptionsFromDBS(result.rows.item(i).idInHost);
            }
        })
        deleteQuestionFromDB(idProject);
    }

    async _fetchQoestionsFromApi(remember_Token, idProject) {
        const formData = new FormData();
        formData.append('remember_Token', remember_Token);
        formData.append('idProject', idProject);
        url = this.props.globalVariables.homeURL + '/index.php?r=api/Question/androidGetQuestionsAndOptions';
        try {
            let response = await axios({
                url: url,
                method: 'POST',
                data: formData,
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data'
                }
            });
            let responseJson = await response.data;
            if (responseJson.length > 0) {
                let len = responseJson.length;
                let textLen = 'تعداد ' + len + '  سوال با موفقیت دریافت شد.';
                this.setState({textOfQuestionFromServer: textLen})
                this.setState({showGetQuestionFromServer: true});
                responseJson.forEach(function (item) {
                    // console.log('سوال ها ',item);
                    insertQuedtion(item);
                })
            }
            else {
                this.setState({textOfQuestionFromServer: responseJson.message})
                this.setState({showGetQuestionFromServer: true});
                // alert(responseJson.message);
                // this.setState({showOverly: false});
            }
        } catch (error) {
            console.error(error);
        }
    }

    async _fetOptionsFromApi(remember_Token, idProject) {
        const formData = new FormData();
        formData.append('remember_Token', remember_Token);
        formData.append('idProject', idProject);
        url = this.props.globalVariables.homeURL + '/index.php?r=api/option/androidGetOptions';
        try {
            let response = await axios({
                url: url,
                method: 'POST',
                data: formData,
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data'
                }
            });
            let responseJson = await response.data;
            //console.log(responseJson);
            if (responseJson.length > 0) {
                let textLen = 'گزینه ها بروز رسانی شد . . .'
                this.setState({textOfOptionFromServer: textLen})
                this.setState({showGetOptionFromServer: true});
                responseJson.forEach(function (item) {
                    insertOptionsToDB(item);
                })
            }
            else {
                this.setState({textOfOptionFromServer: responseJson.message})
                this.setState({showGetOptionFromServer: true});
                // this.setState({showOverly: false});
            }
        } catch (error) {
            console.error(error);
        }
    }

    //=========================================================================== Question TYPE
    _deleteQuestionTypeFromDB(idProject) {
        deleteQuestionType(idProject);
    };

    async _fetchQuestionType(remember_Token) {
        url = this.props.globalVariables.homeURL + '/index.php?r=api/Question/androidGetQuestionsType';
        try {
            const formData = new FormData();
            formData.append('remember_Token', remember_Token);
            let response = await axios({
                url: url,
                method: 'POST',
                data: formData,
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data'
                }
            });
            let responseJson = await response.data;
            if (responseJson.length > 0) {
                responseJson.forEach(function (item) {
                    insertQuestionTypeToDB(item);
                })
            }
            else {
                alert(responseJson.message);
            }
        } catch (error) {
            console.error(error);
        }
    }

    //=========================================================================== SEC
    _deleteSECFromDB(idProject) {
        deleteSEC(idProject);
    };

    async _fetchSECFromApi(remember_Token, idProject) {
        const formData = new FormData();
        formData.append('remember_Token', remember_Token);
        formData.append('idProject', idProject);
        url = this.props.globalVariables.homeURL + '/index.php?r=api/projects/androidGetSEC';
        try {
            let response = await axios({
                url: url,
                method: 'POST',
                data: formData,
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data'
                }
            });
            let responseJson = await response.data;
            console.log('SEC: ', responseJson);
            console.log('SEC: ', responseJson.length);

            if (responseJson.length > 0) {
                let len = responseJson.length;
                console.log("len", responseJson.length);
                let textLen = 'طبقه اجتماعی بروز رسانی شد! ';
                this.setState({textOfPolicySetFromServer: textLen})
                this.setState({showGetPolicySetFromServer: true});
                responseJson.forEach(function (item) {
                    insertToSEC(item);
                })
            }
            else {
                this.setState({textOfPolicySetFromServer: responseJson.message})
                this.setState({showGetPolicySetFromServer: true});
            }
        } catch (error) {
            console.error(error);
        }
    }

    //===========================================================================  USER PROJECTS
    /*    _deleteUserProjectFromDB(idUser){
            deleteUserProject(idUser);
        };
        async _fetchUserProjectFromApi( remember_Token ){
            const formData = new FormData();
            formData.append('remember_Token', remember_Token);
            url = 'http://aradebartar.ir/porseshname/index.php?r=api/projects/androidGetUserProject';
            try {
                let response = await axios({
                    url: url,
                    method: 'POST',
                    data: formData,
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'multipart/form-data'
                    }
                });
                let responseJson = await response.data;
                // console.log('userPPP: ' ,responseJson);
                if (responseJson.length > 0) {
                    responseJson.forEach(function (item) {
                        //console.log(item);
                         insertToUserProject(item);
                    })
                }
                else {
                }
            } catch (error) {
                console.error(error);
            }
        }*/
    //================================================ Upload ALL PORSESHNAME To Host=======================
    _uploadPorseshnamehToHost = async () => {
        NetInfo.getConnectionInfo().then(async(connectionInfo)=>{
            if(connectionInfo.type!='none'){
                this.setState({showOverlySend:true});
                let idPtoject = this.props.project.idInHost;
                let idUser = this.props.user.idInHost;
                // گرفتن لیست پرسشنامه های پروژه که فیلد آپلودش برابر فالس باشه
                let porseshnameList = await getAllPorseshnamehFromDB(idPtoject, idUser);
                let porseshnamehNumberArray = [];
                let porseshnameListItrms = [];
                console.log("porseshnameList :", porseshnameList)
                let reapet = 0;
                // از اول لیست پیمایش کن تا آخر یک آرایه شامل شماره پرسشنامه های غیر تکراری درست کن
                for (let item = 0; item < porseshnameList.rows.length; item++) {
                    porseshnameListItrms.push(porseshnameList.rows.item(item))
                    // console.log("item : ", porseshnameList.rows.item(item))
                    reapet = porseshnamehNumberArray.includes(porseshnameList.rows.item(item).porseshnamehNumber);
                    if (reapet == false) {
                        porseshnamehNumberArray.push(porseshnameList.rows.item(item).porseshnamehNumber);
                    }
                }
                // console.log("porseshnameListItrms :",porseshnameListItrms);
                // با توجه به آرایه ای که شامل شماره پرسشنامه های غیر تکراری هست لیست اصلی را فیلتر کرده و دسته بندی میکنیم
                let onePorseshnameh = [];
                let allPorseshnamehReadyToUploads = [];

                porseshnamehNumberArray.map(async (item) => {
                    // console.log("item :", item);
                    onePorseshnameh = porseshnameListItrms.filter((row) => row.porseshnamehNumber === item.toString());
                    // console.log("onePorseshnameh:", onePorseshnameh);
                    //برای جلوگیری از رکورد های زیاد اینر جوین هر پرسشنامه را به عنوان یک آبجکت که شامل پاسخگو و کل جوابهایش است به سرور ارسال کن
                    let finalPorseshnameh = {
                        idInApp : onePorseshnameh[0].id,
                        porseshnamehNumber: onePorseshnameh[0].porseshnamehNumber,
                        idProject: onePorseshnameh[0].idProject,
                        idPorseshgar: onePorseshnameh[0].idPorseshgar,
                        completedDate: onePorseshnameh[0].completedDate,
                        startTime: onePorseshnameh[0].startTime,
                        endTime: onePorseshnameh[0].endTime,
                        Longitude: onePorseshnameh[0].Longitude,
                        latitude: onePorseshnameh[0].latitude,
                        pasokhgoo: {
                            name: onePorseshnameh[0].name,
                            fName: onePorseshnameh[0].fName,
                            homeAddress: onePorseshnameh[0].homeAddress,
                            workAddress: onePorseshnameh[0].workAddress,
                            cityName: onePorseshnameh[0].cityName,
                            cityArea: onePorseshnameh[0].cityArea,
                            phoneNumber: onePorseshnameh[0].phoneNumber,
                            mobileNumber: onePorseshnameh[0].mobileNumber,
                        },
                        answers: []
                    }

                    let answer = []
                    answer = onePorseshnameh.map(row => {
                        finalPorseshnameh.answers.push({
                            idQuestion: row.idQuestion,
                            idOption: row.idOption,
                            text: row.text,
                        })
                    })
                    allPorseshnamehReadyToUploads.push(finalPorseshnameh);
                })
                console.log("allPorseshnamehReadyToUploads: ", allPorseshnamehReadyToUploads)
                let params = {
                    remember_Token: this.props.user.remember_Token,
                    allPorseshnamehReadyToUploads: JSON.stringify(allPorseshnamehReadyToUploads),
                    uri: this.props.globalVariables.homeURL + '/index.php?r=api/login/sendPorseshnameFromApp'
                }
                if(allPorseshnamehReadyToUploads.length>0){
                    // console.log("params :" ,params)
                    let uploadResult = await uploadPorseshnameh(params);
                    let response = uploadResult.data.success;
                    // console.log("uploadResult: ",uploadResult);
                    if (response === true) {
                        // فیلد های uploadtohost و idInHost رو ویرایش کن که مجددا آپلود نشه// update UploadToHost Field in database set True
                        let countOfPorseshnameUpdated=0;
                        uploadResult.data.returnedIds.map(async item=> {
                            countOfPorseshnameUpdated++;
                            $tempArr=item.split(',');
                            $idInsertedToHost=$tempArr[0];
                            $idPorseshnameinApp=$tempArr[1];
                            updateUploadToHostAfterUploadPorseshname($idPorseshnameinApp,$idInsertedToHost);
                            console.log($tempArr);
                        });
                        this.setState({
                            textOfSendToServer:'تعداد'+countOfPorseshnameUpdated.toString()+'  پرسشنامه به سرور ارسال شد',
                            showOverlySendText:true
                        });
                    }
                    else {
                        alert(JSON.stringify(uploadResult.data.message));
                        // Actions.reset('auth')
                    }
                }
                else{
                    this.setState({
                        textOfSendToServer:'پرسشنامه ای که شرایط آپلود داشته باشد یافت نشد. بیشتر بدانید!',
                        showOverlySendText:true
                    });
                }
            }
            else{
                alert("دستگاه خود را به اینترنت متصل کنید.");
            }
        })


    }
}

const mapStateToProps = (state) => {
    return {
        project: state.project,
        user: state.user,
        globalVariables: state.globalVariables,

    }
}
const mapDispathToProps = dispatch => {
    return {
        setProject: project => {
            dispatch(setProject(project))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(ManageProject);


const manageProjectCss = EstyleSheet.create({
    container: {
        flexDirection: 'row-reverse',
        alignItems: 'stretch',
        paddingTop: 20,
        justifyContent: 'space-around'
    },
    cardItem: {
        backgroundColor: '#e6eaea'

    },
    tabBody: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    tabBodyText: {
        height: 100,
        lineHeight: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    box0: {
        width: '95%',
        height: '100%'

    },
    box1: {
        width: '45%',
        height: '100%'

    },
    box2: {
        width: '45%',
        height: '100%'

    }

})






