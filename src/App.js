import React, {Component} from 'react';
import {AppRegistry, Text, View, Image, ScrollView, StyleSheet, NetInfo} from 'react-native';
import {Router, Scene, Lightbox, Drawer} from 'react-native-router-flux';
import EStyleSheet from 'react-native-extended-stylesheet';
import Home from "./screens/Home";
import AllProject from "./screens/AllProjects";
import {connect, Provider} from 'react-redux'
import SQLite from 'react-native-sqlite-storage';

EStyleSheet.build({
    $statusBarColor: '#123123',
    $IS: 'IRANSansMobile',
    $ISBold: 'IRANSansMobile_Bold',
    $appThemeColor: '#2c3e50',
    $backGroundColor: '#337ab7',
    $blueBackgroundColor: '#ffffff',
    $yellowBackgroundColor: '#fff513',
    $redColor:'#FF2B23',
    $windowTitleFontSize:18,
    $cardTitleFontSize:16,
    $contentFontSize:15,
});

//My Component
import Login from './screens/Login'
import Splash from "./screens/Splash";
import DrawerLayout from "./screens/DrawerLayout";
import Settings from "./screens/Settings";
import store from "./components/redux/store/index";
import AllProjects from "./screens/AllProjects";
import SyncWithServer from "./screens/SyncWithServer";
import ManageProject from "./screens/ManageProject";
import PorseshnamehManagement from "./screens/PorseshnamehManagement";
import Pasokhgoo from "./screens/Pasokhgoo";
import openQuestion from "./components/questions/openQuestion";
import PorseshnamehSearch from './screens/PorseshnamehSearch';
import singleResponse from './components/questions/singleResponse';
import multipleChoice from './components/questions/multipleChoice';
import singleResponseOthers from './components/questions/singleResponseOthers';
import multipleChoiceOthers from './components/questions/multipleChoiceOthers';
import Numeric from './components/questions/Numeric';
import Spectral from './components/questions/Spectral';
import Email from './components/questions/Email';
// import Rating from './components/questions/Rating';
import Degree from './components/questions/Degree';
import WebsiteLink from './components/questions/WebsiteLink';
import Prioritize from './components/questions/Prioritize';
import Example from './components/questions/Example';
import NoResponse from "./components/questions/NoResponse";
import OpenOptions from "./components/questions/OpenOptions";
import DateFull from "./components/questions/DateFull";
import ShowVideo from "./components/questions/ShowVideo";
import Finish from "./components/questions/Finish";

export default class App extends Component {
    render() {
        const RouterWithRedux = connect()(Router)
        return (
            <Provider store={store}>
                <RouterWithRedux>
                    <Scene hideNavBar>
                        <Scene key='root' hideNavBar>
                            <Drawer  key='drawer' contentComponent={DrawerLayout} drawerPosition='right'>

                                <Scene hideNavBar>
                                    <Scene key="home" component={Home}/>
                                    <Scene key="settings" component={Settings}/>
                                    <Scene key="syncWithServer" component={SyncWithServer}/>
                                    <Scene key="manageProject" component={ManageProject}/>
                                    <Scene key="allProject" component={AllProject} initial/>

                                    <Scene key="porseshnamehSearch" component={PorseshnamehSearch}/>
                                    <Scene key="pasokhgoo" component={Pasokhgoo}/>



                                </Scene>
                            </Drawer>
                        </Scene>
                        <Lightbox key="auth">
                            <Scene hideNavBar>
                                <Scene key="login" component={Login}/>
                            </Scene>
                        </Lightbox>
                        <Lightbox key="newPorseshname">
                            <Scene hideNavBar>
                                <Scene key="porseshnamehManagement" component={PorseshnamehManagement}/>
                            </Scene>
                        </Lightbox>
                        <Scene key="splash" component={Splash} initial/>
                        <Scene key="openQuestion" component={openQuestion}/>
                        <Scene key="singleResponse" component={singleResponse}/>
                        <Scene key="multipleChoice" component={multipleChoice}/>
                        <Scene key="singleResponseOthers" component={singleResponseOthers}/>
                        <Scene key="multipleChoiceOthers" component={multipleChoiceOthers}/>
                        <Scene key="numeric" component={Numeric}/>
                        <Scene key="spectral" component={Spectral}/>
                        <Scene key="email" component={Email}/>
                        {/*<Scene key="rating" component={Rating}/>*/}
                        <Scene key="degree" component={Degree}/>
                        <Scene key="websiteLink" component={WebsiteLink}/>
                        <Scene key="prioritize" component={Prioritize}/>
                        <Scene key="example" component={Example}/>
                        <Scene key="noResponse" component={NoResponse}/>
                        <Scene key="openOptions" component={OpenOptions}/>
                        <Scene key="dateFull" component={DateFull}/>
                        <Scene key="showVideo" component={ShowVideo}/>
                        <Scene key="finished" component={Finish}/>
                    </Scene>

                </RouterWithRedux>
            </Provider>
        )
    }
}

