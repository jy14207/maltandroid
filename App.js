
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import SQLite from 'react-native-sqlite-storage';
import {I18nManager } from 'react-native';
I18nManager.forceRTL(false);
I18nManager.allowRTL(false);

type Props = {};
export default class App extends Component<Props> {
    componentDidMount() {
        db = SQLite.openDatabase({ name: "dataDB", createFromLocation: "~data.db" },
            this.openSuccess, this.openError);
    }
    openSuccess() {
        alert("Database is opened");
    }
    componentWillUnmount() {
        this.closeDatabase();
    }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
