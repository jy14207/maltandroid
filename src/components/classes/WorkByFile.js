import React from 'react';
import RNFS from 'react-native-fs';

export const mkDir = (path) => {
    RNFS.mkdir(`${RNFS.DocumentDirectoryPath}/${path}`)
}

export const downloadFile = (urlObject, callBack) => {
    let numberOfDownloadFiles=0;
    RNFS.unlink(`${RNFS.TemporaryDirectoryPath}`);
    // deleteFile(`${RNFS.TemporaryDirectoryPath}`)
    urlObject.map(unit => {
        console.log(unit.fromUrl);
        RNFS.downloadFile({
            fromUrl: unit.fromUrl,
            toFile: `${RNFS.DocumentDirectoryPath}/${unit.toFile}`,
            cacheable: false
        }).promise.then((r) => {
            // console.log("resultRNFS : " ,r);
            if(r.statusCode == 200 ){
                numberOfDownloadFiles++;
                callBack(numberOfDownloadFiles);
            }
        });

    })
}

export const deleteFile=( url )=>{
    RNFS.unlink(url);
}

/*
RNFS.exists(`${RNFS.DocumentDirectoryPath}/${unit.toFile}`)
    .then((result)=>{
  if(result){
      RNFS.unlink(`${RNFS.DocumentDirectoryPath}/${unit.toFile}`)
      .then(result=>{
          RNFS.downloadFile({
              fromUrl: unit.fromUrl,
              toFile: `${RNFS.DocumentDirectoryPath}/${unit.toFile}`,
              cacheable: false,
          }).promise.then((r) => {
              return("پاک شد سپس دانلود شد");
          });
      })
  }
  else {

  }
})*/
