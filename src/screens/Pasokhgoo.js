import React from 'react';
import {ScrollView, TextInput, Picker, TouchableOpacity} from 'react-native';
import {
    Container,
    Header,
    Title,
    Right,
    Left,
    Content,
    Button,
    Text,
    Card,
    CardItem,
    Body,
    View,
    Input,
    Item,
    Icon
} from 'native-base';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
// import Icon from 'react-native-vector-icons/FontAwesome'

// My Import
import {form} from "./../assets/styles/index";
import {card} from "../assets/css/SyncWithServer";
import {common} from './../assets/css/common';
import {questionLoadFunc} from "../components/questions/QuestionLoad";
import {insertPasokhgooTodatabase, updatePasokhgooTodatabase} from "../components/database/pasokhgoo";
import {cityList} from "../components/database/city";

class Pasokhgoo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            idPorseshname: null,
            name: null,
            fName: null,
            homeAddress: null,
            workAddress: null,
            cityName: null,
            cityArea: null,
            phoneNumber: 0,
            mobileNumber: 0,
            selectedCountry: null,
            cityData: [],
            pasokhgooRowInEdit: [],
            update: false,
            nameErrorBorderColor: '#898c8c',
            fNameErrorBorderColor: '#898c8c',
            homeAddressErrorBorderColor: '#898c8c',
            workAddressErrorBorderColor: '#898c8c',
            mobileAdressErrorBorderColor: '#898c8c',
        }
    }

    componentDidMount() {
        let idProject = this.props.project.idInHost;
        cityList(idProject, (result) => {
            const cityData = ['انتخاب کنید'];
            for (let i = 0; i < result.rows.length; i++) {
                cityData.push(result.rows.item(i).name)
            }
            this.setState({cityData})
            // console.log(cityData);

        });
        let pasokhgooRowInEdit = this.props.pasokhgooRow;

        if (pasokhgooRowInEdit) {

            this.setState({
                pasokhgooRowInEdit: this.props.pasokhgooRow,
                cityName: this.props.pasokhgooRow.cityName,
                name: this.props.pasokhgooRow.name,
                fName: this.props.pasokhgooRow.fName,
                homeAddress: this.props.pasokhgooRow.homeAddress,
                workAddress: this.props.pasokhgooRow.workAddress,
                cityName: this.props.pasokhgooRow.cityName,
                cityArea: this.props.pasokhgooRow.cityArea,
                phoneNumber: this.props.pasokhgooRow.phoneNumber,
                mobileNumber: this.props.pasokhgooRow.mobileNumber,
                update: true,
            })
        }
        // console.log('phoneNumber:', this.state.pasokhgooRowInEdit.phoneNumber);
    }

    countryList = () => {
        return ( this.state.cityData.map((x, i) => {
            return ( <Picker.Item label={x} key={i} value={x}/>)
        }));
    }
    insertToDatabase = () => {
        let submit = true;
        if (this.state.name == null | this.state.name == '') {
            this.setState({nameErrorBorderColor: '#ff1c24'})
            submit = false;
        }
        else
            this.setState({nameErrorBorderColor: '#898c8c'})

        if (this.state.fName == null | this.state.fName == '') {
            this.setState({fNameErrorBorderColor: '#ff1c24'})
            submit = false;
        }
        else
            this.setState({fNameErrorBorderColor: '#898c8c'})

        if (this.state.homeAddress == null | this.state.homeAddress == '') {
            this.setState({homeAddressErrorBorderColor: '#ff1c24'})
            submit = false;
        }
        else
            this.setState({homeAddressErrorBorderColor: '#898c8c'})

        if (this.state.workAddress == null | this.state.workAddress == '') {
            this.setState({workAddressErrorBorderColor: '#ff1c24'})
            submit = false;
        }
        else
            this.setState({workAddressErrorBorderColor: '#898c8c'})
        if (this.state.mobileNumber == null | this.state.mobileNumber == '' | this.state.mobileNumber.length != 11) {
            this.setState({mobileAdressErrorBorderColor: '#ff1c24'})
            submit = false;
        }
        else
            this.setState({workAddressErrorBorderColor: '#898c8c'});
        let cityName = this.state.cityName;
        if (cityName == null) {
            alert('نام شهر را انتخاب کنید');
            submit = false;
        }
        if (submit == true) {
            console.log("this.props.project.idInHost :" ,this.props.project.idInHost)
            let idProject = this.props.project.idInHost;
            let idPorseshnameh = this.props.porseshnameh.id;
            let pasokhgooRow = {
                idPorseshname: this.state.idPorseshname,
                name: this.state.name,
                fName: this.state.fName,
                homeAddress: this.state.homeAddress,
                workAddress: this.state.workAddress,
                cityName: this.state.cityName,
                cityArea: this.state.cityArea,
                phoneNumber: this.state.phoneNumber,
                mobileNumber: this.state.mobileNumber,
            }
            insertPasokhgooTodatabase(pasokhgooRow);
            questionLoadFunc(idProject, idPorseshnameh, 1);
        }
        else {
            alert('لطفا فیلدهای الزامی را مقدار دهی نمائید!');
        }
    }
    updateToDatabase = () => {
        let submit = true;
        if (this.state.name == null | this.state.name == '') {
            this.setState({nameErrorBorderColor: '#ff1c24'})
            submit = false;
        }
        else
            this.setState({nameErrorBorderColor: '#898c8c'})

        if (this.state.fName == null | this.state.fName == '') {
            this.setState({fNameErrorBorderColor: '#ff1c24'})
            submit = false;
        }
        else
            this.setState({fNameErrorBorderColor: '#898c8c'})

        if (this.state.homeAddress == null | this.state.homeAddress == '') {
            this.setState({homeAddressErrorBorderColor: '#ff1c24'})
            submit = false;
        }
        else
            this.setState({homeAddressErrorBorderColor: '#898c8c'})

        if (this.state.workAddress == null | this.state.workAddress == '') {
            this.setState({workAddressErrorBorderColor: '#ff1c24'})
            submit = false;
        }
        else
            this.setState({workAddressErrorBorderColor: '#898c8c'})
        if (this.state.mobileNumber == null | this.state.mobileNumber == '' | this.state.mobileNumber.length != 11) {
            this.setState({mobileAdressErrorBorderColor: '#ff1c24'})
            submit = false;
        }
        else
            this.setState({workAddressErrorBorderColor: '#898c8c'});
        let cityName = this.state.cityName;
        if (cityName == null) {
            alert('نام شهر را انتخاب کنید');
            submit = false;
        }
        if (submit == true) {
            let idProject = this.props.project.idInHost;
            let idPorseshnameh = this.state.idPorseshname;
            // console.log("idPorseshnameh123456789:", idPorseshnameh);
            let pasokhgooRow = {
                idPorseshname: this.state.idPorseshname,
                name: this.state.name,
                fName: this.state.fName,
                homeAddress: this.state.homeAddress,
                workAddress: this.state.workAddress,
                cityName: this.state.cityName,
                cityArea: this.state.cityArea,
                phoneNumber: this.state.phoneNumber,
                mobileNumber: this.state.mobileNumber,
            }
            // console.log('pasokhgooRowInEdit:',this.props.pasokhgooRow.id);
            updatePasokhgooTodatabase(pasokhgooRow, this.props.pasokhgooRow.id);
            // console.log("آی دی پرسشنامه در پاسخگووو :", idPorseshnameh);
            questionLoadFunc(idProject, idPorseshnameh, 1);
        }
        else {
            alert('لطفا فیلدهای الزامی را مقدار دهی نمائید!');
        }
    }

    componentWillMount() {
        let pasokhgooRowInEdit = this.props.pasokhgooRow;
        if (pasokhgooRowInEdit) {
            // alert(this.props.idPorseshnameh);
            // console.log('idPorseshnameh :', this.props.idPorseshnameh)
            this.setState({
                idPorseshname: this.props.idPorseshnameh,
            });
        }
        else {
            this.setState({
                idPorseshname: this.props.porseshnameh.id,
            });
        }
    }


    render() {
        const componenetTitle = 'پاسخگو';
        const oneCardTitle = 'مشخصات پاسخگو';
        const oneCardbuttonText = 'ثبت';

        return (
            <Container>
                <Header style={form.headerStyle}>
                    <Right>
                        <Title style={common.headerText}>{this.props.project.subject}</Title>
                        <Icon name='arrow-forward' onPress={() => Actions.replace('porseshnamehManagement')}
                              style={{fontWeight: 500, color: 'white', marginLeft: 10}}/>
                    </Right>
                </Header>
                <Content padder>
                    <ScrollView style={{height: "100%", flex: 1}}>
                        <Card style={card.parentCard}>
                            <CardItem  bordered style={card.cardItemHeader}>
                                <Text style={[card.cardTitle, common.titleText]}>{oneCardTitle}</Text>
                            </CardItem>
                            <CardItem style={card.cardItemBody}>
                                <View style={{flex: 1, flexDirection: 'column'}}>
                                    <Text style={[card.labelStyle, common.bodyText]}>نام : </Text>
                                    <Input
                                        style={[common.textInput, {borderColor: this.state.nameErrorBorderColor}]}
                                        value={this.state.name}
                                        getRef={input => {
                                            this.nameRef = input;
                                        }}
                                        onSubmitEditing={() => {
                                            this.fNameRef._root.focus();
                                        }}
                                        returnKeyType={"next"}
                                        onChangeText={name => this.setState({
                                            name: name,
                                            nameErrorBorderColor: '#898c8c'
                                        })}/>

                                    <Text style={[card.labelStyle, common.bodyText]}>نام خانوادگی : </Text>
                                    <Input style={[common.textInput, {borderColor: this.state.fNameErrorBorderColor}]}
                                           value={this.state.fName}
                                           ref={input => {
                                               this.fNameRef = input;
                                           }}
                                           onSubmitEditing={() => {
                                               this.addressRef._root.focus();
                                           }}
                                           returnKeyType={"next"}
                                           onChangeText={fName => this.setState({fName: fName})}/>

                                    <Text style={[card.labelStyle, common.bodyText]}>نام شهر: </Text>
                                    <Picker style={[common.textInput]}
                                            selectedValue={this.state.cityName}
                                            onValueChange={(value) => ( this.setState({cityName: value}) )}>
                                        {this.countryList()}
                                    </Picker>

                                    <Text style={[card.labelStyle, common.bodyText]}>آدرس محل سکونت : </Text>
                                    <Input
                                        style={[common.textInput, {borderColor: this.state.homeAddressErrorBorderColor}]}
                                        ref={input => {
                                            this.addressRef = input;
                                        }}
                                        onSubmitEditing={() => {
                                            this.workAddressRef._root.focus();
                                        }}
                                        returnKeyType={"next"}
                                        value={this.state.homeAddress}
                                        onChangeText={homeAddress => this.setState({
                                            homeAddress: homeAddress,
                                            homeAddressErrorBorderColor: '#898c8c'
                                        })}/>

                                    <Text style={[card.labelStyle, common.bodyText]}>آدرس محل کار : </Text>
                                    <Input
                                        style={[common.textInput, {borderColor: this.state.workAddressErrorBorderColor}]}
                                        ref={input => {
                                            this.workAddressRef = input;
                                        }}
                                        onSubmitEditing={() => {
                                            this.cityAreaRef._root.focus();
                                        }}
                                        returnKeyType={"next"}
                                        value={this.state.workAddress}
                                        onChangeText={value => this.setState({
                                            workAddress: value,
                                            workAddressErrorBorderColor: '#898c8c'
                                        })}/>

                                    <Text style={[card.labelStyle, common.bodyText]}>منطقه شهرداری : </Text>
                                    <Input style={common.textInput}
                                           ref={input => {
                                               this.cityAreaRef = input;
                                           }}
                                           onSubmitEditing={() => {
                                               this.phoneRef._root.focus();
                                           }}
                                           returnKeyType={"next"}
                                           keyboardType='phone-pad'
                                           value={this.state.cityArea ? `${this.state.cityArea}` : `${''}`}
                                           onChangeText={cityArea => this.setState({cityArea: cityArea})}/>
                                    <Text style={[card.labelStyle, common.bodyText]}>تلفن منزل یا محل کار : </Text>
                                    <Input style={common.textInput}
                                           ref={input => {
                                               this.phoneRef = input;
                                           }}
                                           onSubmitEditing={() => {
                                               this.mobileRef._root.focus();
                                           }}
                                           returnKeyType={"next"}
                                           value={this.state.phoneNumber == null ? `${''}` : `${this.state.phoneNumber}`}
                                           keyboardType='phone-pad'
                                           onChangeText={phoneNumber => this.setState({phoneNumber: phoneNumber})}/>

                                    <Text style={[card.labelStyle, common.bodyText]}>شماره همراه : </Text>
                                    <Input
                                        onSubmitEditing={()=>this.state.update === true ? this.updateToDatabase() : this.insertToDatabase()}
                                        style={[common.textInput, {borderColor: this.state.mobileAdressErrorBorderColor}]}
                                        ref={input => {
                                            this.mobileRef = input;
                                        }}

                                        value={this.state.mobileNumber ? `${this.state.mobileNumber}` : `${'0'}`}
                                        keyboardType='phone-pad'
                                        onChangeText={mobileNumber => this.setState({
                                            mobileNumber: mobileNumber,
                                            mobileAdressErrorBorderColor: '#898c8c'
                                        })}/>
                                    <TouchableOpacity style={{width: '30%'}}
                                                      onPress={this.state.update === true ? this.updateToDatabase : this.insertToDatabase}>
                                        <Button outline style={form.submitButton}
                                                onPress={this.state.update === true ? this.updateToDatabase : this.insertToDatabase}
                                               full >
                                            <Text style={[form.submitText, common.body]}
                                                  onPress={this.state.update === true ? this.updateToDatabase : this.insertToDatabase}>{oneCardbuttonText}</Text>
                                            {/*<Icon name='check' size={20} color="#ffffff" solid style={{marginRight: 10}}/>*/}
                                        </Button>
                                    </TouchableOpacity>
                                </View>
                            </CardItem>
                        </Card>
                    </ScrollView>
                </Content>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        project: state.project,
        porseshnameh: state.porseshnameh
    }
}
const mapDispathToProps = dispatch => {
    return {
        setPorseshnameh: porseshnameh => {
            dispatch(setPorseshnameh(porseshnameh))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(Pasokhgoo);