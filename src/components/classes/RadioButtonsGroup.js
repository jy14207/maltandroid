import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View,Input} from 'react-native';

export default class RadioGroup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            radioButtons: this.validate(this.props.radioButtons),
        };
    }

    validate(data) {
        let selected = false; // Variable to check if "selected: true" for more than one button.
        data.map(e => {
            e.color = e.color ? e.color : '#444';
            e.disabled = e.disabled ? e.disabled : false;
            e.label = e.label ? e.label : 'You forgot to give label';
            e.layout = e.layout ? e.layout : 'row';
            e.selected = e.selected ? e.selected : false;
            if (e.selected) {
                if (selected) {
                    e.selected = false; // Making "selected: false", if "selected: true" is assigned for more than one button.
                    console.log('Found "selected: true" for more than one button');
                } else {
                    selected = true;
                }
            }
            e.size = e.size ? e.size : 20;
            e.idInHost = e.idInHost ? e.idInHost : e.label;
        });
        if (!selected) {
            data[0].selected = false;
        }
        console.log("data :",data);
        return data;
    }

    onPress = label => {
        const radioButtons = this.state.radioButtons;
        const selectIndex = radioButtons.findIndex(e => e.label == label);
        for (i=0;i<radioButtons.length;i++){
            radioButtons[i].selected=false;
        }
            radioButtons[selectIndex].selected = true;
             console.log("radioButtons :",radioButtons)
            this.setState({radioButtons});
            this.props.onPress(this.state.radioButtons);
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={{flexDirection: this.props.flexDirection}}>
                    {this.state.radioButtons.map(data => (
                        <RadioButton
                            key={data.label}
                            data={data}
                            onPress={this.onPress}
                        />
                    ))}
                </View>
            </View>
        );
    }
}

class RadioButton extends Component {
    render() {
        const data = this.props.data;
        const opacity = data.disabled ? 0.2 : 1;
        let layout = {flexDirection: 'row',
            flexDirection: 'row',
            justifyContent: 'flex-end',};
        let margin = {marginRight: 5,fontSize:13, fontFamily:'IRANSansMobile',};
        let moreexplanationStyle = {marginRight: 3,color:'red',fontSize:10, fontFamily:'IRANSansMobile',};
        if (data.layout === 'column') {
            layout = {alignItems: 'center'};
            margin = {marginTop: 10};
        }
        return (
            <TouchableOpacity
                style={[layout, {opacity, marginHorizontal: 10, marginVertical: 5,}]}
                onPress={() => {
                    data.disabled ? null : this.props.onPress(data.label);
                }}>
                <Text style={[{alignSelf: 'center'},moreexplanationStyle ]}>{data.moreExplanation}</Text>
                <Text style={[{alignSelf: 'center'},margin ]}>{data.label}</Text>
                <View
                    style={[
                        styles.border,
                        {
                            borderColor: data.color,
                            width: data.size,
                            height: data.size,
                            borderRadius: data.size / 2,
                            alignSelf: 'center'
                        },
                    ]}>
                    {data.selected &&
                    <View
                        style={{
                            backgroundColor: data.color,
                            width: data.size / 2,
                            height: data.size / 2,
                            borderRadius: data.size / 2,
                        }}
                    />}
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        borderColor:'red',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    border: {
        borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
