import React from 'react';
import {Header , Left, Right , Title} from 'native-base'
import {form} from "./../../assets/styles/index";
import {common} from './../../assets/css/common';
import Icon from 'react-native-vector-icons/FontAwesome5'
import Modal from 'react-native-modalbox';
import {questionLoadFunc} from "../questions/QuestionLoad";


export default class QuestionHeader extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const componenetTitle = this.props.project.subject;

        return (
            <Header style={form.headerStyle}>
                <Left style={{flexDirection: 'row'}}>
                    <Icon name='angle-double-up' size={20} color="#ffffff" solid style={{
                        fontWeight: 500,
                        marginRight: 10,
                        color: 'white',
                        marginLeft: 10,
                        marginBottom: 4
                    }} onPress={()=> this.props.modal.open()}/>
                </Left>
                <Right>
                    <Title style={common.headerText}>{componenetTitle}</Title>
                    <Icon name='arrow-right' size={20} color="#ffffff" solid style={{
                        fontWeight: 500,
                        marginRight: 10,
                        color: 'white',
                        marginLeft: 10,
                        marginBottom: 4
                    }} onPress={this.props.myOnPress}/>
                </Right>
            </Header>
        )

    }
}