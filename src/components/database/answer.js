import {selectAnswerRow ,insertAnswer , updateAnswer} from './Model'

export const getAnswerPromiseOfQuestionRow=(idQuestion,idPorseshnameh)=>{
    // console.log("idQ:", idQuestion);
    return new Promise((resolve,reject)=>{
       selectAnswerRow(idQuestion,idPorseshnameh,(cbResult)=>{
           // console.log("idQuestion :",idQuestion)
           // console.log("idPorseshnameh :",idPorseshnameh)
           // console.log("answer :",cbResult.rows.item(0));
           // console.log("idQuestion :",idQuestion);
           // console.log("idPorseshnameh :",idPorseshnameh);
           resolve(cbResult.rows.item(0))
       })
    });
}
 export const storeAnswerToDB = async(idPorseshname,idQuestion,idOption,text,updateOrInsert,idAnswer)=>{
     let hasAnswer= await getAnswerPromiseOfQuestionRow(idQuestion,idPorseshname);
     if(hasAnswer){
         console.log("updateed")
         updateAnswer(idPorseshname,idQuestion,idOption,text,idAnswer);

     }
     else{
         console.log("inserted")
         insertAnswer(idPorseshname,idQuestion,idOption,text);
     }
 }