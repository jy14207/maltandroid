import React from 'react';
import {Container, Header, Title, Right, Left, Content, Card, CardItem, Body, Input, Item, Button} from 'native-base';

import {
    Text,
    StyleSheet,
    ScrollView,
    View,
    Dimensions,
    TextInput,
    ProgressBarAndroid,
    Linking,
    ImageBackground,
    TouchableOpacity
} from 'react-native';
import Modal from 'react-native-modalbox';
import * as Progress from 'react-native-progress';

import Snackbar from 'react-native-snackbar';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome5'

import {form} from "./../../assets/styles/index";
import {card} from "../../assets/css/SyncWithServer";
import {common} from './../../assets/css/common';
import {modal} from './../../assets/css/modal'

import {questionStyle} from '../../assets/css/questionStyle'
import {questionLoadFunc} from './QuestionLoad';
import {storeAnswerToDB} from './../database/answer';
import {getAnswerPromiseOfQuestionRow} from './../database/answer'

class Finish extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            row: [],
            answer: [],
            socialPolicyIs: false,
            stopPolicyIs: false,
            countPolicyIs: false,
            rowNumber: 0,
            percentage: 0,
            questionCount: 0,
            text: '',
            error: '#898c8c',
            update: false,
        }
    }

    componentDidMount() {
        this.setState({
            row: this.props.row,
            percentage: this.props.percentage,
            questionCount: this.props.questionCount,
            rowNumber: this.props.rowNumber,

        });
    }

    snackPolicy(socialPolicyMessage) {
        Snackbar.show({
            title: socialPolicyMessage,
            duration: Snackbar.LENGTH_INDEFINITE,
            action: {
                title: 'بستن',
                color: 'green',

            },
        })
    }

    nextPorseshnameh(){
        // UPDATE PORSESHNAMEH FOR LOCK
        //updatePorseshnameForLock

         // GO TO PorseshnamehManagement
        Actions.reset("newPorseshname");
    }


    render() {
        var progressColor = 'orange';
        if (this.state.percentage / 100 > 50) {
            progressColor = "green"
        }
        const componenetTitle =this.props.project.subject;
        const oneCardbuttonText = 'پرسشنامه بعدی';
        const btnExitText = 'خروج';
        return (
            <Container>
                <ImageBackground
                    source={require('./../../assets/images/AppImage/question.jpg')}
                    style={common.fullImageBackground}
                    imageStyle={{borderRadius: 2}}>
                    <Header style={form.headerStyle}>
                        <Left style={{flexDirection: 'row'}}>
                            {/*<Icon name='angle-double-up' size={20} color="#ffffff" solid style={{
                                fontWeight: 500,
                                marginRight: 10,
                                color: 'white',
                                marginLeft: 10,
                                marginBottom: 4
                            }} onPress={() => this.refs.modal4.open()}/>*/}
                        </Left>
                        <Right>
                            <Title style={common.headerText}>{componenetTitle}</Title>
                            <Icon name='arrow-right' size={20} color="#ffffff" solid style={{
                                fontWeight: 500,
                                marginRight: 10,
                                color: 'white',
                                marginLeft: 10,
                                marginBottom: 4
                            }} onPress={() => Actions.push('pasokhgoo')}/>
                        </Right>

                    </Header>
                    <Content padder style={{marginTop: 20}}>
                        <ScrollView style={{height: "100%", flex: 1}}>
                            <Card style={card.mainCard}>
                                <CardItem bordered style={[card.cardTitle, {justifyContent: 'center'}]}>
                                    <Text
                                        style={[card.cardTitle, common.finishedText]}>از پاسخگویی شما متشکریم</Text>
                                </CardItem>
                                <CardItem style={{minHeight: 150, flexDirection: 'column', paddingTop: 30}}>
                                    <Text style={{
                                        fontSize: 15,
                                        fontFamily: 'IRANSansMobile',
                                        color: 'red',
                                        marginBottom: 10
                                    }}>اتمام پرسشنامه بدلیل :</Text>
                                    <Text
                                        style={{
                                            fontSize: 16,
                                            fontFamily: 'IRANSansMobile_Bold',
                                            color: 'black'
                                        }}>{this.props.reason}
                                    </Text>


                                </CardItem>
                                <CardItem footer bordered style={{justifyContent:'space-around'}}>
                                    <TouchableOpacity onPress={this.exitFromApp}>
                                        <Button style={{
                                            backgroundColor: '#FF2B24',
                                            borderColor:'5187E8',
                                            borderWidth: 1,
                                            borderRadius: 5,
                                        }} onPress={this.storeAnswer}>
                                            <Text style={[form.submitText, {paddingLeft: 10, paddingRight: 5}]}
                                                  onPress={this.storeAnswer}>{btnExitText}</Text>
                                            <Icon name='sign-out-alt' size={20} color="#ffffff" solid style={{marginRight: 10}}
                                                  onPress={this.storeAnswer}/>
                                        </Button>

                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={this.nextPorseshnameh}>
                                        <Button style={{
                                            backgroundColor: '#2383E8',
                                            borderWidth: 1,
                                            borderRadius: 5,
                                        }} onPress={this.nextPorseshnameh}>
                                            <Text style={[form.submitText, {paddingLeft: 10, paddingRight: 5}]}
                                                  onPress={this.storeAnswer}>{oneCardbuttonText}</Text>
                                            <Icon name='check' size={20} color="#ffffff" solid style={{marginRight: 10}}
                                                  onPress={this.nextPorseshnameh}/>
                                        </Button>
                                    </TouchableOpacity>
                                </CardItem>
                            </Card>
                        </ScrollView>
                    </Content>
                </ImageBackground>
            </Container>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        porseshnameh: state.porseshnameh,
        project: state.project,
        user: state.user
    }
}
const mapDispathToProps = dispatch => {
    return {
        setPorseshnameh: porseshnameh => {
            dispatch(setPorseshnameh(porseshnameh))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(Finish);
