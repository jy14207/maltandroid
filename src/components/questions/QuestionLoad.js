import React from 'react';
import {Actions} from 'react-native-router-flux';

import {getQuestion, getQPromise, getQuestionCountPromise} from '../database/question';
import {getOptionFromDbPromise} from '../database/questionAndOption';
import {getAnswerPromiseOfQuestionRow} from '../database/answer'
import {getPolicySetForCheckPromise, getPolicyansToOther} from './../database/policy';

export const questionLoadFunc = async (idProject, idPorseshnameh, rowNumber) => {
    var socialPolicyIs = false;
    var stopPolicyIs = false;
    var countPolicyIs = false;

    const row = await getQPromise(idProject, rowNumber);
    console.log("row:", row);
    if (row === undefined) {
        let reason = 'اتمام سوالات'
        Actions.finished({reason});
    }
    else {
        // گرفتن پالیسی نمایش جواب سوال های دیگر در این سوال
        const policyansToOtherTextOptions = await getPolicyansToOther(idPorseshnameh, row.idInHost);
        // console.log("policyansToOtherTextOptions :",policyansToOtherTextOptions);

        //     گرفتن اینکه چه پالیسی در این سوال ست شده است
        const policy = await getPolicySetForCheckPromise(idProject, row.idInHost);
        let len = policy.length;
        for (let i = 0; i < len; i++) {
            let idPolicy = policy.item(i).idPolicy;
            if (idPolicy == "1") socialPolicyIs = true;
            if (idPolicy == "2") stopPolicyIs = true;
            if (idPolicy == "3") countPolicyIs = true;
        }

        const answer = await getAnswerPromiseOfQuestionRow(row.idInHost, idPorseshnameh);

        const questionCountPromise = await getQuestionCountPromise(idProject);

        const questionCount = questionCountPromise.countQ;
        const percentage = parseInt((rowNumber * 100) / questionCount);
        let rowsOptions = null;
        let options = [];
        console.log("QuestionRow : ", row);
        // console.log("answer : ",answer);
        // console.log("idQuestionType : ",row.idQuestionType);
        switch (row.idQuestionType) {
            case 1: // singleResponse  تک پاسخی
                rowsOptions = await getOptionFromDbPromise(row.idInHost);
                options = [];
                for (i = 0; i < rowsOptions.rows.length; ++i) {
                    options.push(rowsOptions.rows.item(i));
                }
                Actions.reset('singleResponse',{
                    row,
                    answer,
                    socialPolicyIs,
                    stopPolicyIs,
                    countPolicyIs,
                    questionCount,
                    percentage,
                    rowNumber,
                    options,
                    policyansToOtherTextOptions
                });
                break;
            case 2: // multipleChoice چند پاسخی
                rowsOptions = await getOptionFromDbPromise(row.idInHost);
                options = [];
                for (i = 0; i < rowsOptions.rows.length; ++i) {
                    options.push(rowsOptions.rows.item(i));
                }
                Actions.reset('multipleChoice',{
                    row,
                    answer,
                    socialPolicyIs,
                    stopPolicyIs,
                    countPolicyIs,
                    questionCount,
                    percentage,
                    rowNumber,
                    options,
                    policyansToOtherTextOptions
                });
                break;
            case 3: // openQuestion تشریحی
                // console.log('answ :' ,answer);
                Actions.reset('openQuestion',{
                    row,
                    answer,
                    socialPolicyIs,
                    stopPolicyIs,
                    countPolicyIs,
                    questionCount,
                    percentage,
                    rowNumber,
                    policyansToOtherTextOptions
                });
                break;
            case 4:// singleResponseOthers  تک پاسخی با سایر
                rowsOptions = await getOptionFromDbPromise(row.idInHost);
                options = [];
                for (i = 0; i < rowsOptions.rows.length; ++i) {
                    options.push(rowsOptions.rows.item(i));
                }
                Actions.reset('singleResponseOthers',{
                    row,
                    answer,
                    socialPolicyIs,
                    stopPolicyIs,
                    countPolicyIs,
                    questionCount,
                    percentage,
                    rowNumber,
                    options,
                    policyansToOtherTextOptions
                });

                break;
            case 5: // multipleChoiceOthers  چند پاسخی با سایر

                rowsOptions = await getOptionFromDbPromise(row.idInHost);
                options = [];
                for (i = 0; i < rowsOptions.rows.length; ++i) {
                    options.push(rowsOptions.rows.item(i));
                }
                Actions.reset('multipleChoiceOthers',{
                    row,
                    answer,
                    socialPolicyIs,
                    stopPolicyIs,
                    countPolicyIs,
                    questionCount,
                    percentage,
                    rowNumber,
                    options,
                    policyansToOtherTextOptions
                });
                break;
            case 6: //numeric عددی
                Actions.reset('numeric',{
                    row,
                    answer,
                    socialPolicyIs,
                    stopPolicyIs,
                    countPolicyIs,
                    questionCount,
                    percentage,
                    rowNumber,
                    policyansToOtherTextOptions
                });
                break;
            case 7:  //spectral  طیفی
                Actions.reset('spectral',{
                    row,
                    answer,
                    socialPolicyIs,
                    stopPolicyIs,
                    countPolicyIs,
                    questionCount,
                    percentage,
                    rowNumber,
                    policyansToOtherTextOptions
                });
                break;
            case 8:  // email ایمیل
                Actions.reset('email',{
                    row,
                    answer,
                    socialPolicyIs,
                    stopPolicyIs,
                    countPolicyIs,
                    questionCount,
                    percentage,
                    rowNumber,
                    policyansToOtherTextOptions
                });
                break;
            case 9:// degree درجه بندی
                Actions.reset('degree',{
                    row,
                    answer,
                    socialPolicyIs,
                    stopPolicyIs,
                    countPolicyIs,
                    questionCount,
                    percentage,
                    rowNumber,
                    policyansToOtherTextOptions
                });
                break;
            case 10://websitelink لینک وبسایت
                Actions.reset('websiteLink',{
                    row,
                    answer,
                    socialPolicyIs,
                    stopPolicyIs,
                    countPolicyIs,
                    questionCount,
                    percentage,
                    rowNumber,
                    policyansToOtherTextOptions
                });
                break;
            case 11://prioritize اولویت دهی
                rowsOptions = await getOptionFromDbPromise(row.idInHost);
                options = [];
                for (i = 0; i < rowsOptions.rows.length; ++i) {
                    options.push(rowsOptions.rows.item(i));
                }
                Actions.reset('prioritize',{
                    row,
                    answer,
                    socialPolicyIs,
                    stopPolicyIs,
                    countPolicyIs,
                    questionCount,
                    percentage,
                    rowNumber,
                    options,
                    policyansToOtherTextOptions
                });
                // Actions.example({row, answer, socialPolicyIs, stopPolicyIs, countPolicyIs, questionCount, percentage,rowNumber,options});
                break;
            case 12://noResponse متن بدون پاسخ
                Actions.reset('noResponse',{row, questionCount, percentage, rowNumber, policyansToOtherTextOptions});
                break;
            case 13://openOptions  گزینه های توضیح دار
                rowsOptions = await getOptionFromDbPromise(row.idInHost);
                options = [];
                var answerArray = [];
                for (i = 0; i < rowsOptions.rows.length; ++i) {
                    options.push(rowsOptions.rows.item(i));
                }
                if (answer != null) {
                    let idOptionsAnswer = answer.idOption.split(',');
                    let textAnswer = answer.text.split(',');
                    console.log("idOptionsAnswer.length :", idOptionsAnswer.length);
                    for (i = 0; i < idOptionsAnswer.length; i++) {
                        answerArray.push({idOption: idOptionsAnswer[i], text: textAnswer[i]})
                    }
                    console.log("answerArray :", answerArray)
                }
                console.log("answerArray2 :", answerArray)

                Actions.reset('openOptions',{
                    row,
                    answer,
                    answerArray,
                    answerArray,
                    socialPolicyIs,
                    stopPolicyIs,
                    countPolicyIs,
                    questionCount,
                    percentage,
                    rowNumber,
                    options,
                    policyansToOtherTextOptions
                });
                break;
            case 14://dateFull  تاریخ
                Actions.reset('dateFull',{
                    row,
                    answer,
                    socialPolicyIs,
                    stopPolicyIs,
                    countPolicyIs,
                    questionCount,
                    percentage,
                    rowNumber,
                    policyansToOtherTextOptions
                });
                break;
            case 15://Video  ویدئو
                Actions.reset('showVideo',{row, questionCount, percentage, rowNumber, policyansToOtherTextOptions});
                break;
            case 16://Image تصویر
                break;
            case 17://audio صوت
                break;
        }
    }

}

