import React from 'react';
import {
    Container,
    Header,
    Title,
    Right,
    Alert,
    Left,
    Content,
    Card,
    CardItem,
    Body,
    Input,
    Item,
    Button
} from 'native-base';

import {
    Text,
    StyleSheet,
    ScrollView,
    View,
    Dimensions,
    TextInput,
    ProgressBarAndroid,
    Linking,
    ImageBackground,
    TouchableOpacity
} from 'react-native';
import Modal from 'react-native-modalbox';
import * as Progress from 'react-native-progress';

import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome5'

import {form} from "./../../assets/styles/index";
import {card} from "../../assets/css/SyncWithServer";
import {common} from './../../assets/css/common';
import {modal} from './../../assets/css/modal'
import RadioGroup from 'react-native-radio-buttons-group'
import CheckboxGroup from './../classes/checkboxGroup'

import {questionStyle} from '../../assets/css/questionStyle'
import {questionLoadFunc} from './QuestionLoad';
import {storeAnswerToDB} from './../database/answer';
import {checkPolicyForJumpToOtherQuestion, checkPolicyForStopAnswer} from "../database/policy";
import QuestionHeader from "../ui/QuestionHeader";
import StoreButton from "../ui/StoreButton";


class multipleChoiceOthers extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            row: [],
            showSubmitCheck:false,
            answer: [],
            socialPolicyIs: false,
            stopPolicyIs: false,
            countPolicyIs: false,
            rowNumber: 0,
            percentage: 0,
            questionCount: 0,
            text: '',
            error: '#898c8c',
            update: false,
            selectedButton: null,
            otherIsSelected: false,
            otherInputValue: null,
            optionData:
                {
                    label: "",
                    value: "",
                    moreExplanation: "",
                    score: '',
                    selected: false,
                }
            ,
        }
    }

    componentWillMount() {
        var tempOptionData = [];
        console.log("this.props.answer :", this.props.answer);

        for (let i = 0; i < this.props.options.length; i++) {
            let selectedStatuse: false;
            if (this.props.answer != null) {
                selectedStatuse = this.props.answer.idOption.includes(this.props.options[i].idInHost);
            }
            tempOptionData.push({
                label: this.props.options[i].textOption,
                value: this.props.options[i].idInHost,
                moreExplanation: this.props.options[i].moreExplanation,
                score: this.props.options[i].score,
                selected: selectedStatuse,
            });
            if (this.props.answer != null && (this.props.options[i].idInHost == Number(this.props.answer.idOption))) {
                this.setState({selectedButton: this.props.options[i]});
                tempOptionData[i].selected = true;
            }
        }
        for (let i = 1; i < tempOptionData.length; i++) {
            if (tempOptionData[i]['label'] == "Other") {
                tempOptionData[i]['label'] = "سایر"
            }
        }
        this.setState({
            optionData: tempOptionData,
            rowNumber: this.props.rowNumber,
        });
        // console.log('rowNumberInComponentWillMountSingleResponseCase:',this.state.rowNumber)

    }

    componentDidMount() {
        this.setState({
            row: this.props.row,
            answer: this.props.answer,
            socialPolicyIs: this.props.socialPolicyIs,
            stopPolicyIs: this.props.stopPolicyIs,
            countPolicyIs: this.props.countPolicyIs,
            percentage: this.props.percentage,
            questionCount: this.props.questionCount,
            // rowNumber: this.props.rowNumber,

        });
        // console.log("this.props.answer :", this.props.answer);
        if (this.props.answer != null) {
            this.setState({
                update: true,
                otherInputValue: this.props.answer['text'],
            })
        }
    }


    jumpToQuestion = () => {
        var idProject = this.props.project.idInHost;
        var idPorseshnameh = this.props.porseshnameh.id;
        var rowNumber = this.state.rowNumber;
        // console.log('rowNumberInjumpToQuestionSingleResponseCase:',rowNumber)
        if (this.state.rowNumber != 0) {
            questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, this.state.rowNumber)
        }
        else {
            alert("شماره سوال را وارد کنید.")
        }
    }

    storeAnswer = async () => {
        let newRowNumber = 0, rowNumber = 0, stopedAnsweing = 0;
        if ((this.state.selectedButton == '' || this.state.selectedButton == null ) && this.state.row.allowNull == 'no') {
            alert("پاسخ به این سوال اجباری است");
        }
        else {
            var idQuestion = this.state.row.idInHost;
            let idPorseshnameh = this.props.porseshnameh.id;
            let selectedButton = this.state.selectedButton;
            let idProject = this.props.project.idInHost;
            // console.log("selectedButton In Store Func :", selectedButton.toString())

            this.setState({error: '#898c8c'});
            let text = this.state.otherInputValue;
            let othetIsSelected = this.state.otherIsSelected;
            if (othetIsSelected == true && (text == null | text == '')) {
                this.setState({otherErrorColor: 'red'});
            }
            else {
                if (this.state.update === false)
                   await storeAnswerToDB(idPorseshnameh, idQuestion, selectedButton.toString(), text, 'insert');
                else
                   await storeAnswerToDB(idPorseshnameh, idQuestion, selectedButton.toString(), text, 'update', this.state.answer['id']);

                for (const item of selectedButton) {
                    newRowNumber = await checkPolicyForJumpToOtherQuestion(idProject, idQuestion, item);
                    console.log("newRowNumber in loop: ", newRowNumber);
                    if (newRowNumber > 0) {
                        break
                    }
                }
                if (newRowNumber > 0)
                    rowNumber = newRowNumber;
                else
                    rowNumber = (Number(this.state.rowNumber) + 1);
                for (const item of selectedButton) {
                    stopedAnsweing = await checkPolicyForStopAnswer(idProject, idQuestion, item);
                    if (stopedAnsweing == 1) {
                        break
                    }
                }
                if (stopedAnsweing == 1) {
                    let reason = 'گزینه ای که انتخاب کرده اید باعث توقف مصاحبه شد.'
                    Actions.finished({reason});
                    return;
                }
                this.setState({showSubmitCheck:true});

                questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, rowNumber);
            }
        }
    };
    loadPreviousQuestion = () => {
        rowNumber = (Number(this.state.rowNumber) - 1);
        questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, rowNumber);

    };

    render() {
        var progressColor = 'orange';
        if (this.state.percentage / 100 > 50) {
            progressColor = "green"
        }

        var policy = false, socialPolicyMessage = false, stopPolicyIsMessage = false, countPolicyIsMessage = false;
        if (this.state.socialPolicyIs == true) {
            socialPolicyMessage = 'جواب این سوال در تعیین طبقه اجتماعی موثر است.';
            policy = true;
        }
        if (this.state.stopPolicyIs == true) {
            stopPolicyIsMessage = 'سیاست توقف مصاحبه برای این سوال لحاظ شده است.';
            policy = true;
        }
        if (this.state.countPolicyIs == true) {
            countPolicyIsMessage = 'سیاست تعداد خاص از پرسشنامه برای این سوال لحاظ شده است.';
            policy = true;
        }

        var policyansToOtherTextOptionsBool = false, policyansToOtherTextOptions = '';
        if (this.props.policyansToOtherTextOptions != '') {
            policyansToOtherTextOptions = this.props.policyansToOtherTextOptions;
            policyansToOtherTextOptionsBool = true;
        }

        const componenetTitle =this.props.project.subject;
        const oneCardbuttonText = 'ثبت';
        return (
            <Container>
                <ImageBackground
                    source={require('./../../assets/images/AppImage/question.jpg')}
                    style={common.fullImageBackground}
                    imageStyle={{borderRadius: 2}}>
                    <QuestionHeader
                        myOnPress={this.loadPreviousQuestion}
                        project={this.props.project}
                        modal={this.refs.modal4}
                    />
                    <Content padder style={{marginTop: 20}}>
                        {policy ? <View hide={policy} style={common.questionPolicyAlert}>
                            {socialPolicyMessage ?
                                <Text style={common.questionPolicyTextAlert}> - {socialPolicyMessage}</Text> : null}
                            {stopPolicyIsMessage ?
                                <Text style={common.questionPolicyTextAlert}> - {stopPolicyIsMessage}</Text> : null}
                            {countPolicyIsMessage ?
                                <Text style={common.questionPolicyTextAlert}> - {countPolicyIsMessage}</Text> : null}
                        </View> : null}
                        <ScrollView style={{height: "100%", flex: 1}}>
                            <Card>
                                <CardItem style={card.cardItemHeader}>
                                    <Text
                                        style={[card.cardTitle, common.titleText]}>{this.state.row.rowNumber + "- " + this.state.row.text}
                                        <Text
                                            style={[card.cardTitle, common.moreExplanationText]}>{this.state.row.moreExplanation}</Text>
                                        {policyansToOtherTextOptionsBool ?
                                            <Text
                                                style={[card.cardTitle, common.ansToOtherQTextStyle]}>/جواب هایی که به
                                                سوال های قبل داده اید : {policyansToOtherTextOptions}</Text>
                                            : null}</Text>
                                </CardItem>
                                <View style={{flex: 1, flexDirection: 'column'}}>
                                    <CardItem style={{paddingBottom:0}}>
                                        <CheckboxGroup
                                            callback={(selected, score) => {
                                                this.setState({selectedButton: selected});
                                                this.setState({scoreOfSelectedButton: score});
                                                console.log("score:", score)
                                                score.includes('999') ? this.setState({otherIsSelected: true}) : this.setState({
                                                    otherIsSelected: false,
                                                    otherInputValue: null
                                                })
                                            }}

                                            iconColor={"#444"}
                                            iconSize={20}
                                            checkedIcon="ios-checkmark-circle"
                                            uncheckedIcon="ios-radio-button-off"
                                            checkboxes={this.state.optionData}
                                            labelStyle={{
                                                color: '#444',
                                                paddingRight: 5,
                                                fontFamily: 'IRANSansMobile'
                                            }}
                                            rowStyle={{
                                                flexDirection: 'row',
                                                minHeight: 30,

                                            }}
                                            rowDirection={"column"}
                                        />
                                    </CardItem>
                                    <CardItem style={{paddingTop: 0, marginTop: 0, paddingRight: 40}}>
                                        {
                                            this.state.otherIsSelected &&
                                            <Input style={[common.textInput, {
                                                borderColor: this.state.otherErrorColor,
                                                margin: 0,
                                                paddingTop: 0
                                            }]}
                                                   value={this.state.otherInputValue ? `${this.state.otherInputValue}` : `${''}`}
                                                   onChangeText={otherInputValue => this.setState({
                                                       otherInputValue: otherInputValue,
                                                       otherErrorColor: '#d8d8d8'
                                                   })}/>
                                        }
                                    </CardItem>
                                </View>

                                <CardItem footer bordered>
                                    <StoreButton onPress={this.storeAnswer} showSubmitCheck={this.state.showSubmitCheck}/>

                                </CardItem>

                            </Card>
                        </ScrollView>
                    </Content>
                    <Modal style={[modal.modal, modal.modal4]} position={"bottom"} ref={"modal4"}>
                        <Card style={modal.modalMainCard}>
                            <CardItem bordered style={card.cardTitle}>
                                <Text style={[common.headerText, {color: 'white'}]}>اطلاعات بیشتر . . .</Text>
                            </CardItem>
                            <CardItem style={{flexDirection: 'column'}}>
                                <Text>{this.state.percentage}% - <Text
                                    style={common.bodyText}>( {this.state.row.rowNumber}
                                    از {this.state.questionCount})</Text></Text>
                                <Progress.Bar progress={this.state.percentage / 100} width={300}
                                              color={(this.state.percentage / 100) < 0.5 ? "red" : "green"}
                                              height={30}></Progress.Bar>
                            </CardItem>
                            <CardItem style={card.modalCardItem2}>
                                <Button info style={[common.bodyText, modal.modalButton]} onPress={this.jumpToQuestion}><Text
                                    onPress={this.jumpToQuestion} style={common.bodyText}> پرش </Text></Button>
                                <Input
                                    style={[common.textInput, modal.modalInput]}
                                    keyboardType='numeric'
                                    onSubmitEditing={()=>this.jumpToQuestion()}
                                    placeholder='شماره سوال؟'
                                    onChangeText={val => this.setState({rowNumber: val})}/>
                            </CardItem>
                            <View style={card.modalView}>
                                <Text style={[common.bodyText]}>بیشتر بدانید :
                                    <Text style={common.webLink} onPress={() => {
                                        Linking.openURL(this.props.user.webSiteLink)
                                    }}>{this.props.user.companyName} </Text>
                                </Text>
                            </View>
                        </Card>
                    </Modal>
                </ImageBackground>
            </Container>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        porseshnameh: state.porseshnameh,
        project: state.project,
        user: state.user
    }
}
const mapDispathToProps = dispatch => {
    return {
        setPorseshnameh: porseshnameh => {
            dispatch(setPorseshnameh(porseshnameh))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(multipleChoiceOthers);
