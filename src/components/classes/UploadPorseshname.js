import axios from 'react-native-axios';

/*export const uploadPorseshnameh = async (params)=>{
    // console.log("params :",params)
    const {remember_Token,porseshnameh,uri} = params;
    const formData = new FormData();
    formData.append("remember_Token",remember_Token);
    formData.append("porseshnameh",porseshnameh);
    console.log("formData: " ,formData);
    try {
        let response = await axios({
            url:uri,
            method: 'POST',
            data:formData,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data'
            }
        });
        let responseData = await response.data;
       console.log(responseData);
    }catch (error) {
        console.error(error);
    }
}*/
export const uploadPorseshnameh = (params)=>{
    // console.log("params :",params)
    const {remember_Token,allPorseshnamehReadyToUploads,uri} = params;
    const formData = new FormData();
    formData.append("remember_Token",remember_Token);
    formData.append("allPorseshnamehReadyToUploads",allPorseshnamehReadyToUploads);
    console.log("formData: " ,formData);
       return axios({
            url:uri,
            method: 'POST',
            data:formData,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data'
            }
        });
}
