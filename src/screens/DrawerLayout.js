import React from 'react';
import {Item, Text, View, Icon} from "native-base";
import {Image, ImageBackground, TouchableOpacity} from "react-native";
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import {logoutUser} from './../components/database/user'
import {drawer} from './../assets/styles/index'

class DrawerLayout extends React.Component {
    render() {
        // console.log("require images.jpg :",require('../assets/images/images2.jpg'))
        return (

            <View style={drawer.container}>
                <View style={{
                    width: '100%',
                    height: 220,
                    backgroundColor: '#f6f5e3',
                    direction: 'rtl',
                    borderColor: '#123456',
                    borderWidth: 1
                }}>
                    <ImageBackground
                        resizeMode="stretch"
                        // source={image ?  require(image)  : require(defaultImg)}
                         source={require('../assets/images/images.jpg')}
                        style={{width: '100%', height: '100%'}}>
                        <View style={{height: 70, width: '100%', paddingRight: 20, marginTop: 150}}>
                            <Text style={{
                                color: 'white',
                                fontFamily: 'IRANSansMobile',
                                fontSize: 20
                            }}>{this.props.user.companyName}</Text>
                            <Text style={{
                                color: 'white',
                                fontFamily: 'IRANSansMobile_Bold',
                                fontSize: 21
                            }}>{this.props.user.name + ' ' + this.props.user.fName}</Text>
                        </View>
                    </ImageBackground>


                </View>

                <View>

                    <Item style={drawer.item}>
                        <TouchableOpacity style={{flexDirection: 'row'}}>
                            <Text style={drawer.itemTitle}>تنظیمات</Text>
                            <Icon name="md-settings" style={drawer.itemIcon}/>
                        </TouchableOpacity>
                    </Item>
                    <Item style={drawer.item}>
                        <TouchableOpacity style={{flexDirection: 'row'}}>
                            <Text style={drawer.itemTitle}
                                  onPress={this.logout}>خروج</Text>
                            <Icon name="md-settings" style={drawer.itemIcon}/>
                        </TouchableOpacity>
                    </Item>
                </View>
            </View>
        )
    }

    logout() {
        logoutUser();
        Actions.replace('login');

    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}
const mapDispathToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(DrawerLayout);