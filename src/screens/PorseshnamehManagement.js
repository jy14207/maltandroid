import React from 'react';
import {ActivityIndicator, ScrollView, TextInput, TouchableOpacity, ImageBackground} from 'react-native';
import {
    Container,
    Header,
    Title,
    Right,
    Left,
    Content,
    Button,
    Text,
    Card,
    CardItem,
    Body,
    View,
    Input,
    Item,
    Icon
} from 'native-base';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import Moment from 'react-native-moment-jalaali'
// import Icon from 'react-native-vector-icons/FontAwesome'

// My Import
import {form} from "./../assets/styles/index";
import {card} from "../assets/css/SyncWithServer";
import {common} from './../assets/css/common';

// import  FontAwesome from 'react-native-vector-icons/FontAwesome5'

import {insertPorseshnameh , getPorseshnamehByNumber } from './../components/database/porseshnameh'
import {setPorseshnameh} from "./../components/redux/actions/index";


import {insertProjectToDatabese} from './../components/database/project';

import axios from 'react-native-axios'


class PorseshnamehManagement extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showOverly: false,
            startTime: '',
            porseshnamehNumber: '',
            idProject: this.props.project.idInHost,
            idPorseshgar: this.props.user.idInHost,
            completedDate: '',
            Longitude: '',
            latitude: '',
            porseshnameNumberErrorBorderColor: '#898c8c',


        }
    }

    componentDidMount() {
        var that = this;
        var hours = new Date().getHours(); //Current Hours
        var min = new Date().getMinutes(); //Current Minutes
        that.setState({
            startTime:
            hours + ':' + min,
            completedDate: Moment().format("jYYYY/jMM/jDD"),

        });
    }



    insertToDatabase = async () => {
        // console.log("insertToDatabase");
        // console.log("porseshnamehNumber :", this.state.porseshnamehNumber);
        // console.log("idInHost:",this.props.project.idInHost);
        //Error If Exist By Porseshnameh Number
        let porseshnamehNumber=this.state.porseshnamehNumber;
        let idProject=this.props.project.idInHost;
        const porseshnamehIsExist = await getPorseshnamehByNumber(porseshnamehNumber,idProject);
        console.log("porseshnamehIsExist : ",porseshnamehIsExist.rows.length);
        if (porseshnamehIsExist.rows.length >0){
            alert("شماره پرسشنامه تکراری است.");
            this.setState({porseshnameNumberErrorBorderColor:'#ff1c24'})
            // console.log("porseshnamehIsExistNumber:" ,porseshnamehIsExist);
        }
        else {
            var porseshnamehRow = {
                id: null,
                porseshnamehNumber: this.state.porseshnamehNumber,
                idProject: this.props.project.idInHost,
                idPorseshgar: this.props.user.idInHost,
                completedDate: this.state.completedDate,
                startTime: this.state.startTime,
                Longitude: '0.2563985',
                latitude: '3.6598745',
            }
            insertPorseshnameh(porseshnamehRow, (myResult) => {
                porseshnamehRow.id = myResult;
                let submit=true;
                if (this.state.porseshnamehNumber == null |this.state.porseshnamehNumber =='')
                {
                    this.setState({porseshnameNumberErrorBorderColor:'#ff1c24'})
                    submit=false;
                }
                else
                if (submit==true){
                    this.props.setPorseshnameh(porseshnamehRow);
                    Actions.replace('pasokhgoo')
                }

                // console.log(porseshnamehRow);




            });
        }

    }

    /*    render() {

            const TextInputs = [];

            for (let i = 0; i < 10; i += 1) {
                TextInputs.push(
                    <View>
                        <Text>ردیف + {i}</Text>
                        <TextInput style={{borderWidth:2,borderColor:'black'}}/>
                    </View>
                );
            }
            return (
                <View>
                    {TextInputs}
                </View>
            );
        }*/

    render() {
        const componenetTitle = 'مدیریت پرسشنامه ها';

        const oneCardTitle = 'درج پرسشنامه جدید';
        const oneCardbuttonText = 'ثبت';

        const twoCardTitle = 'جستجو';

        const twoCardbuttonText = 'جستجو';

        return (
            <Container>
                <Header style={form.headerStyle}>
                    <Left style={{flex: 1, flexDirection: 'row'}}>
                        <TouchableOpacity style={{flexDirection: 'row'}}>
                            <Icon name='search' onPress={() => Actions.replace('porseshnamehSearch')}
                                  size={20}
                                  style={{fontWeight: 500, color: 'white', paddingLeft: 5}}/>
                        </TouchableOpacity>
                    </Left>
                    <Right>
                        <Title style={common.headerText}>{this.props.project.subject}</Title>
                        <TouchableOpacity style={{flexDirection: 'row'}}>
                            <Icon name='arrow-forward' onPress={() => Actions.replace('manageProject')}
                                  style={{fontWeight: 500, color: 'white', marginLeft: 10}}/>
                        </TouchableOpacity>
                    </Right>
                </Header>
                <Content padder style={{marginTop: 20}}>
                    <ScrollView style={{height: "100%", flex: 1}}>
                        <Card style={card.parentCard}>
                            <CardItem  bordered style={card.cardItemHeader}>
                                <Text style={[card.cardTitle, common.titleText]}>{oneCardTitle}</Text>
                            </CardItem>
                            <CardItem style={card.cardItemBody}>
                                <View style={{flex: 1, flexDirection: 'column'}}>
                                    <Text style={[card.labelStyle, common.bodyText]}>شماره پرسشنامه : </Text>
                                    <Input style={[common.textInput,{borderColor:this.state.porseshnameNumberErrorBorderColor}]}
                                           keyboardType='phone-pad'
                                           onSubmitEditing={()=>this.insertToDatabase()}
                                           onChangeText={porseshnamehNumber => this.setState({porseshnamehNumber:porseshnamehNumber,porseshnameNumberErrorBorderColor:'#898c8c'})}/>

                                    <Text style={[card.labelStyle, common.bodyText]}>تاریخ تکمیل : </Text>
                                    <Input style={[common.textInput]} value={Moment().format("jYYYY/jMM/jDD")}
                                           editable={false}/>

                                    <Text style={[card.labelStyle, common.bodyText]}>ساعت شروع : </Text>
                                    <Input style={common.textInput} value={this.state.startTime} editable={false}/>

                                    <Text style={[card.labelStyle, common.bodyText]}>ساعت پایان : </Text>
                                    <Input style={common.textInput}
                                           placeholder='همزمان با تکمیل پرسشنامه ثبت خواهد شد.'
                                           editable={false}/>
                                    <TouchableOpacity style={{width:'35%',marginTop:15}} onPress={this.insertToDatabase}>
                                        <Button outline style={form.submitButton} onPress={this.insertToDatabase} full>
                                            <Text style={form.submitText} onPress={this.insertToDatabase}>ثبت و ادامه</Text>
                                        </Button>
                                    </TouchableOpacity>
                                </View>
                            </CardItem>
                        </Card>
                    </ScrollView>
                </Content>
            </Container>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        porseshnameh: state.porseshnameh,
        project: state.project,
        user: state.user
    }
}
const mapDispathToProps = dispatch => {
    return {
        setPorseshnameh: porseshnameh => {
            dispatch(setPorseshnameh(porseshnameh))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(PorseshnamehManagement);
