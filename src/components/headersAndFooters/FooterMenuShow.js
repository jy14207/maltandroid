import React from 'react';
import {Container, Header, Title, Right, Left, Content, Card, CardItem, Body, Input, Item, Button} from 'native-base';

import {
    Text,
    StyleSheet,
    ScrollView,
    View,
    Dimensions,
    TextInput,
    ProgressBarAndroid,
    Linking,
    ImageBackground,
    TouchableOpacity
} from 'react-native';
import Modal from 'react-native-modalbox';
import * as Progress from 'react-native-progress';

import Snackbar from 'react-native-snackbar';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome5'

import {form} from "./../../assets/styles/index";
import {card} from "../../assets/css/SyncWithServer";
import {common} from './../../assets/css/common';
import {modal} from './../../assets/css/modal'




class FooterMenuShow extends React.Component {
    constructor(props) {
        super(props);
        this.modal4 = React.createRef();
        this.state = {
            row: [],
            answer: [],
            socialPolicyIs: false,
            stopPolicyIs: false,
            countPolicyIs: false,
            rowNumber: 0,
            percentage: 0,
            questionCount: 0,
            text: '',
            error: '#898c8c',
            update: false,
            modalOpen:false,
        }
    }
    snackPolicy(socialPolicyMessage) {
        Snackbar.show({
            title: socialPolicyMessage,
            duration: Snackbar.LENGTH_INDEFINITE,
            action: {
                title: 'بستن',
                color: 'green',

            },
        })
    }

    jumpToQuestion = () => {
        var idProject = this.props.project.idInHost;
        var idPorseshnameh = this.props.porseshnameh.id;
        var rowNumber = this.state.rowNumber;
        if (this.state.rowNumber != 0) {
            questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, this.state.rowNumber)
        }
        else {
            alert("شماره سوال را وارد کنید.")
        }
    }

    componentDidMount(){
        console.log("componentWillReciveProps:",this.props.topp);
        this.setState({modalOpen:true})
        this.openModal();
         // this.props.showing==false ?()=> this.modal4.open():false;
    }
    componentWillReceiveProps (){
        console.log("componentWillReciveProps:",this.props.topp);
        this.setState({modalOpen:true})
        this.openModal();

    }

    openModal=()=>{
        if(this.state.modalOpen){
            console.log("allll",this.state.modalOpen);
            this.modal4.open();
        }
    }

    render() {
        return (
            <Content>
            {/*<Button style={{width:100,height:100}} onPress={() => this.refs.modal4.open()}/>*/}

            <Modal style={[modal.modal, modal.modal4]} position={"bottom"} ref={"modal4"}>
                <Card style={{flexDirection: 'column', margin: 0, width: '100%', height: 250,}}>
                    <CardItem bordered style={card.cardTitle}>
                        <Text style={[common.headerText, {color: 'white'}]}>اطلاعات بیشتر . . .</Text>
                    </CardItem>
                    <CardItem style={{flexDirection: 'column'}}>
                        <Text>{this.state.percentage}% - <Text
                            style={common.bodyText}>( {this.state.row.rowNumber}
                            از {this.state.questionCount})</Text></Text>
                        <Progress.Bar progress={this.state.percentage / 100} width={300}
                                      color={(this.state.percentage / 100) < 0.5 ? "red" : "green"}
                                      height={30}></Progress.Bar>
                    </CardItem>
                    <CardItem style={card.body}>
                        <Input style={[common.textInput, modal.modalInput]}
                               keyboardType='numeric'
                               placeholder='شماره سوال؟'
                               onChangeText={val => this.setState({rowNumber: val})}/>
                        <Button info style={[common.bodyText, modal.modalButton]} onPress={this.jumpToQuestion}><Text
                            onPress={this.jumpToQuestion} style={common.bodyText}> پرش </Text></Button>
                    </CardItem>
                    <CardItem style={{flexDirection: 'column'}}>
                        <Text style={[common.bodyText, {paddingBottom: 10,}]}>بیشتر بدانید :
                            <Text style={common.webLink} onPress={() => {
                                Linking.openURL(this.props.user.webSiteLink)
                            }}>{this.props.user.companyName} </Text>
                        </Text>
                    </CardItem>
                </Card>
            </Modal>
            </Content>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        porseshnameh: state.porseshnameh,
        project: state.project,
        user: state.user
    }
}
const mapDispathToProps = dispatch => {
    return {
        setPorseshnameh: porseshnameh => {
            dispatch(setPorseshnameh(porseshnameh))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(FooterMenuShow);