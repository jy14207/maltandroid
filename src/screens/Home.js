import React from 'react';
import {TouchableOpacity} from 'react-native';
import {Container, Header, Right, Left, Content, Button, Text, Icon, Card, CardItem, Body} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {form} from "./../assets/styles/index";
import {FlatList} from 'react-native';
import {Spinner} from 'native-base';
import {connect} from 'react-redux';

import {tab} from "./../assets/css/home";
import {common} from '../assets/css/common';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            page: 2,
            myArray: []
        }
    }

    render() {
        return (
            <Container>
                <Header style={form.headerStyle}>
                    <Left>
                        <TouchableOpacity>
                            <Text style={[form.headerText, common.headerText]} onPress={() => Actions.jump('settings')}>تنظیمات</Text>
                        </TouchableOpacity>
                    </Left>
                    <Right>
                        <TouchableOpacity>
                            <Icon name='menu' onPress={() => Actions.drawerOpen()}
                                  style={{fontWeight: 500, color: 'white'}}/>
                        </TouchableOpacity>
                    </Right>
                </Header>
                <Content style={{padding: 10}}>
                    <Card bordered>
                        <CardItem
                            button
                            style={tab.cardItem}
                            onPress={() => Actions.push('syncWithServer')}>
                            <Body style={tab.tabBody}>
                            <Text style={[tab.tabBodyText, common.titleText]}
                                  onPress={() => Actions.push('syncWithServer')}>همسان سازی داده ها با سرور</Text>
                            </Body>
                        </CardItem>
                    </Card>
                    <Card bordered>
                        <CardItem
                            button
                            style={tab.cardItem}
                            onPress={() => Actions.push('allProject')}>
                            <Body style={tab.tabBody}>
                            <Text style={[tab.tabBodyText, common.titleText]}
                                  onPress={() => Actions.push('allProject')}>پروژه های فعال</Text>
                            </Body>
                        </CardItem>
                    </Card>
                </Content>
            </Container>


        )
    }

}

const mapStateToProps = state => {
    return {
        user: state.user
    }
};

export default connect(mapStateToProps, null)(Home);