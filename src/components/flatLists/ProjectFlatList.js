import React from 'react';
import {View, FlatList, ImageBackground, Image ,TouchableOpacity} from 'react-native';
import {Card, CardItem, List, ListItem, Left, Body, Right, Text,} from 'native-base';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/AntDesign'

import {getAProjectFromDB} from './../database/project'
import {setProject} from "./../redux/actions/index";
import {common} from './../../assets/css/common';
import EstyleSheet from 'react-native-extended-stylesheet';


class ProjectFlatList extends React.Component {
    ListViewItemSeparator = () => {
        return (
            <View style={{height: 2, width: '100%', backgroundColor: '#808080'}}/>
        );
    };

    render() {
        const flatListItem = this.props.flatListItem;
        return (
            <FlatList
                style={{paddingBottom: 20}}
                data={flatListItem}
                // ItemSeparatorComponent={this.ListViewItemSeparator}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item}) => (
                    <Card bordered>
                        <TouchableOpacity
                            onPress={() => this.getSelectedProjectFromDB(item.id)}
                        >
                            <ImageBackground
                                source={require('../../assets/images/AppImage/projectCardBackground.png')}
                                style={{
                                    height: 140,
                                    flex: 1,
                                    width: '100%',
                                    borderRadius: 2,
                                    justifyContent: 'center',
                                    alignItems: 'flex-end',
                                }}
                                imageStyle={{borderRadius: 2}}>
                                <Text style={tab.tabBodyText}
                                      onPress={() => this.getSelectedProjectFromDB(item.id)}>{item.subject}</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                    </Card>
                )}
            />
        )
    }

    getSelectedProjectFromDB(id) {

        getAProjectFromDB(id, (pojectRow) => {
            console.log(pojectRow.rows.item(0).subject);
            let len = pojectRow.rows.length;
            if (len > 0) {
                var projeh = {
                    id: pojectRow.rows.item(0).id,
                    idInHost: pojectRow.rows.item(0).idInHost,
                    idCompany: pojectRow.rows.item(0).idCompany,
                    subject: pojectRow.rows.item(0).subject,
                    startDate: pojectRow.rows.item(0).startDate,
                    endDate: pojectRow.rows.item(0).endDate,
                    questionunmer: pojectRow.rows.item(0).questionunmer,
                    pasokhgooNumber: pojectRow.rows.item(0).pasokhgooNumber,
                    status: pojectRow.rows.item(0).status,
                    softDelete: pojectRow.rows.item(0).softDelete,
                }
                this.props.setProject(projeh);
                Actions.push('manageProject')

            }

            // console.log(this.props.user.remember_Token);
            // console.log(this.props.project.subject);


        });
    }
}

const mapStateToProps = (state) => {
    return {
        project: state.project,
        user: state.user
    }
}
const mapDispathToProps = dispatch => {
    return {
        setProject: project => {
            dispatch(setProject(project))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(ProjectFlatList);


const tab = EstyleSheet.create({
    cardItem: {
        backgroundColor: '#e6eaea',
    },
    tabBody: {
        borderWidth: 1,
        borderColor: 'red',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 2,
    },
    tabBodyText: {
        fontFamily:'$IS',
        fontSize:20,
        color: '#3f3f3f',
        lineHeight: 50,
        justifyContent: 'flex-end',
        alignItems: 'center',
        padding:10,
    }

})