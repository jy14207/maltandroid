import {SET_GLOBAL_VARIABLES} from "../actions/type";

const initialstate = {
    homeURL: null,

}
export default globalVariables = (state = initialstate, action = {}) => {
    switch (action.type) {
        case SET_GLOBAL_VARIABLES:
            const {globalvariables} = action;
            return {
                homeURL: globalvariables.homeURL,
            }
            break;
        default :
            return state;

    }
}