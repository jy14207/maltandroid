import { insertProject, selectAll , selectOne ,deleteQuery ,insertSEC ,insertToUserProjectTodb ,selectAllUserProject } from './Model';

export const insertProjectToDatabese=(userPreojectRow,idUser)=>{
    // console.log('WHEN INSERT FROM DEB IDUSER =',idUser)

    insertProject(userPreojectRow,idUser);
    // console.log("ProjectRow : ",userPreojectRow.id);
}

export const projectList = (idUser,projectListResult)=>{
    // console.log('WHEN GET FROM DEB IDUSER =',idUser)
    selectAllUserProject(idUser,(results)=>{
        projectListResult(results);
    });
}
export const getAProjectFromDB=(thisId,projectRow)=>{
    selectOne('project','id',thisId,(results)=>{
        projectRow(results);
    });
};
export const deleteSEC = (idProject) => {
    return new Promise((resolve,reject)=>{
        deleteQuery('sec','idProject' ,idProject,(cb)=>{
            resolve(cb);
        });

    })
};
export const insertToSEC=(row)=>{
    insertSEC(row);
};
export const deleteUserProject=(idUser)=>{
    deleteQuery('userproject','idUser',idUser);
}
export const insertToUserProject = (row)=>{
    insertToUserProjectTodb(row);
};
export const deleteProject=(idUser)=>{
    console.log("idUseraaaaaaaaaaaaa :" ,idUser);
    return new Promise((resolve,reject)=>{
        deleteQuery('project','idUser',idUser,resolve);
    })
}
