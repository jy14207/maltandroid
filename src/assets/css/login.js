import {StyleSheet} from 'react-native'
const styles = StyleSheet.create({
    container:{
        flex : 1,
        backgroundColor : '#337ab7',
        justifyContent : 'center',
        alignItems : 'center',
        flexDirection : 'row'
    },
    loginBox:{
        backgroundColor: 'white',
        flex : 1,
        marginRight : 30,
        marginLeft : 30,
        borderRadius : 5,
        elevation : 2,
        shadowColor : 'black' ,
        shadowOffset : { width : 0 , height: 2},
        shadowOpacity : .1,
        paddingBottom:20,
    },
    loginTitle:{
        color:'#505252',
        textAlign:'center',
        marginBottom:5,
        padding:5,
        fontFamily:"IRANSansMobile_Medium",
        fontSize:18

    },
    labelText:{
        textAlign: 'right',
        marginBottom : 3,
        marginTop:10,
        color : '#337ab7',
        fontFamily:"IRANSansMobile"

    },
    inputGroup:{
        marginRight:20,
        marginLeft:20,

    },
    inputText : {
        textAlign : 'right',
        borderColor : 'rgba(0,0,0,.1)',
        borderWidth : 1,
        padding : 10,
        borderRadius: 2,
        height: 40,
        fontFamily:"IRANSansMobile_Light",
        fontSize:13
    },
    loginButton:{
        marginRight:25,
        marginLeft:25,
        backgroundColor:'#337ab7',
        textAlign:'center',
        borderRadius:25,
        fontSize:18,
        marginTop:10,
        padding:5,
        color:'#ffffff',
        elevation:3,
        fontFamily:"IRANSansMobile"

    },
    forgetPassword:{
        textAlign:'center',
        marginTop:5,
        fontFamily:"IRANSansMobile",
        fontSize:12
    },
    menuText:{
        fontFamily:'IRANSansMobile_Bold',
        fontSize:20,
        color:'white'
    }



})
export default styles;