
import {SET_USER} from "./type";
import {SET_PROJECT} from "./type";
import {SET_PORSESHNAMEH} from './type'
import {SET_GLOBAL_VARIABLES} from './type'

export const setUser=(user)=>({
    type:SET_USER,
    user:user
})
export const setProject=(project)=>({
    type:SET_PROJECT,
    project:project
})
export const setPorseshnameh=(porseshnameh)=>({
    type:SET_PORSESHNAMEH,
    porseshnameh:porseshnameh
})
export const setGlobalVariables=(globalvariables)=>({
    type:SET_GLOBAL_VARIABLES,
    globalvariables:globalvariables
})