import {selectPolicySetFromDB, getPolicyAnsToOtherFromDB ,getPolicyForStopAnswerFromDB} from './Model'
import { getOptionFromDBPromise } from './questionAndOption';
import {getQuestionByIDPromise} from "./question";

// =====================================اعلان سیاست های ست شده به کاربر======================
// این تابع در زمانی که سوال لود شده است و میخواد به کاربر نمایش داده شود فراخوانی میگردد
export const getPolicySetForCheckPromise = (idProject, idQuestion) => {
    return new Promise((resolve, reject) => {
        selectPolicySetFromDB(idProject, idQuestion, (result) => {
            // console.log('cbPromise:', result.rows.item(0))
            resolve(result.rows);
        });
    });
}


// اعمال سیاست های ست شده به سوال
// این تابع زمانیکه سوال جواب داده شد وقبل اینکه سوال بعدی لود شود فراخوانی میگردد
// در.واقع این توابع چک میکنند که چه سوالی باید لود شود آیا باید مصاحبه خاتمه پیدا کند یا نه ، در واقع سیاست هایی همچون تعداد خاص از این پرسشنامه را چک میکند.

export const getPolicyansToOther = async (idPorseshnameh, idQuestion) => {
    console.log("Rows Of PolicySet Table Innerjoin Answer Table For PolicyansToOther");
    let rowsPolicyansToOther = await getPolicyAnsToOtherPromise(idPorseshnameh, idQuestion)
    let len = rowsPolicyansToOther.length;
    let arrayIdOptionsFromPolicySetTable ,arrayTextOptions=[];
    if (len > 0) {
        for (i = 0; i < rowsPolicyansToOther.length; i++) {
            arrayIdOptionsFromPolicySetTable = rowsPolicyansToOther.item(i).idOptionans.split(',');
        }
        for (i = 0; i < arrayIdOptionsFromPolicySetTable.length; i++) {
            let res = await getOptionFromDBPromise(arrayIdOptionsFromPolicySetTable[i]);
            arrayTextOptions.push(res.rows.item(0).textOption)
        }
        // console.log("arrayTextOptions : " ,arrayTextOptions);
    }
    return arrayTextOptions.toString();
}

// getPolicyAnsToOtherFromDB
export const getPolicyAnsToOtherPromise = (idPorseshnameh, idQuestion) => {
        return new Promise((resolve) => {
            getPolicyAnsToOtherFromDB(idPorseshnameh, idQuestion, (cb) => {
                // console.log("cbgetPolicyAnsToOtherFromDB :",cb)
                resolve(cb.rows)
            })
        })
    }

//======================================Check Policy For Jump To Other Question ===========================

export const checkPolicyForJumpToOtherQuestion = async(idProject,idQuestion,idOption)=>{
    return new Promise((resolve,reject)=>{
        getPolicyForStopAnswerFromDB(5,idProject,idQuestion,idOption,results=>{
            if(results != undefined){
                let jumpDestIdQ = results.JumpDestIdQ
                getQuestionByIDPromise(jumpDestIdQ).then(results=>{
                    resolve(results.item(0).rowNumber);//Jump To This Answer RowNumber
                })
            }
            else {
                resolve(0)//Continue Answering
            }
        });
    })
}

//======================================Check Policy For Stop Answer ===========================
export const checkPolicyForStopAnswer = async(idProject,idQuestion,idOption)=>{
    return new Promise((resolve,reject)=>{
        getPolicyForStopAnswerFromDB(2,idProject,idQuestion,idOption,results=>{
            if(results != undefined){
                console.log("نتیجههههههههههههههههههههههههههههههههه", results);
                resolve(1);//Stoped Answer
            }
            else {
                resolve(0)//Continue Answer
            }
        });
    })
}
