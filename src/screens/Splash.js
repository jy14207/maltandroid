import React from 'react';
import {StatusBar} from 'react-native';
import {Text, View, ActivityIndicator} from "react-native";
import {Container, Header, Content, Spinner} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import styles from './../assets/styles';
import {setUser,setGlobalVariables} from './../components/redux/actions/index'
import SQLite from 'react-native-sqlite-storage';
import {selectUser} from './../components/database/user';

const db = SQLite.openDatabase({name: "maltDB", createFromLocation: "~malt.db"}, this.openSuccess, this.openError);

class Splash extends React.Component {
    constructor(props) {
        super(props);
    }
    componentWillMount() {
        this.getUser();
        var global={
            homeURL:'http://aradebartar.ir//porseshname'
        }
        this.props.setGlobalVariables(global);
    }

    render() {
        return (
            <Container style={styles.index.container}>
                <Text style={styles.index.title}>پرسشگر </Text>
                <Text style={styles.index.text}>سامانه تکمیل پرسشنامه های تحقیقات بازار </Text>
                <Spinner color='green'/>
                <StatusBar backgroundColor='#2c3e50'/>
            </Container>
        )
    }

    getUser () {
        db.transaction((tx) => {
            let sql = "SELECT * FROM users"
            tx.executeSql(sql, [], (tx, results) => {
                let len=results.rows.length;
                setTimeout(() => {
                    if (len > 0) {
                        var user = {
                            id: results.rows.item(0).id,
                            idInHost: results.rows.item(0).idInHost,
                            level: results.rows.item(0).level,
                            idCompany: results.rows.item(0).idCompany,
                            name: results.rows.item(0).name,
                            fName: results.rows.item(0).fName,
                            nationalCode: results.rows.item(0).nationalCode,
                            username: results.rows.item(0).username,
                            password: results.rows.item(0).password,
                            homeAdd: results.rows.item(0).homeAdd,
                            remember_Token: results.rows.item(0).remember_Token,
                            status: results.rows.item(0).status,
                            companyName: results.rows.item(0).companyName,
                            webSiteLink: results.rows.item(0).webSiteLink
                        }
                        this.props.setUser(user);
37269021
                        Actions.reset('root');
                        // Actions.reset('splash');
                    } else {
                        Actions.reset('auth');
                        // Actions.reset('splash');
                    }
                }, 0)
            });
        });
    }

}


const mapStateToProps = (state) => {
    return {
        user: state.user,
        globalVariables:state.globalVariables,
    }
}
const mapDispathToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        },
        setGlobalVariables: global => {
            dispatch(setGlobalVariables(global))
        }


    }
}
export default connect(mapStateToProps, mapDispathToProps)(Splash)