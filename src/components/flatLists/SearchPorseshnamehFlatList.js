import React from 'react';
import {View, Text, FlatList, ActivityIndicator, TouchableOpacity , Alert} from 'react-native';
import {ListItem, SearchBar} from 'react-native-elements';
import {Card, CardItem, List, Left, Body, Right} from 'native-base';
import Icon from 'react-native-vector-icons/AntDesign'
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux'

import {common} from './../../assets/css/common';
import EstyleSheet from 'react-native-extended-stylesheet';
import {getPorseshnamehFromDB ,getPorseshnamehById} from './../database/porseshnameh'
import {deletePorseshnameh} from './../database/porseshnameh'
import {selectPasokhgoo} from './../database/pasokhgoo';
import {setPorseshnameh} from "./../redux/actions/index";
import {updateUploadToHostFieldSet0WhenEdit} from "../database/porseshnameh";

class SearchPorseshnamehFlatList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: [],
            error: null,
        };
        this.arrayholder = [];
    }

    componentDidMount() {
        this.getPorseshnamehListFromDB(this.props.idProject,this.props.user.idInHost)
    }

    getPorseshnamehListFromDB = async (idProject,idUserInHost) => {
        const PorseshnamehFromDB = await getPorseshnamehFromDB(idProject,idUserInHost);
        var temp = [];
        console.log("PorseshnamehFromDB :", PorseshnamehFromDB);
        for (let i = 0; i < PorseshnamehFromDB.rows.length; ++i) {
            temp.push(PorseshnamehFromDB.rows.item(i));
        }
        this.setState({
            data: temp,
        });
        this.arrayholder = temp;
        console.log("arrayholder", this.arrayholder);
    };

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: '100%',
                    backgroundColor: '#CED0CE',
                }}
            />
        );
    };
    searchFilterFunction = text => {
        this.setState({
            value: text,
        });
        const newData = this.arrayholder.filter(item => {
            // const itemData = `${item.name.title.toUpperCase()} ${item.name.first.toUpperCase()} ${item.name.last.toUpperCase()}`;
            const itemData = `${item.name}${item.fName}${item.porseshnamehNumber}${item.completedDate}`;
            const textData = text;
            return itemData.indexOf(textData) > -1;
        });
        this.setState({
            data: newData,
        });
    };


    renderHeader = () => {
        return (
            <SearchBar
                placeholder="شماره پرسشنامه،نام پاسخگو،تاریخ ثبت ..."
                lightTheme
                round
                showLoadingIcon={true}
                onChangeText={text => this.searchFilterFunction(text)}
                autoCorrect={false}
                value={this.state.value}
                inputStyle={{fontFamily: '$IS', fontSize: 16,}}
            />
        );
    };
    edit = async (idPorseshnameh)=>{
      let pasokhgooResult = await  selectPasokhgoo(idPorseshnameh);
      let porseshnamehRow= await getPorseshnamehById(idPorseshnameh)
        updateUploadToHostFieldSet0WhenEdit(porseshnamehRow.rows.item(0).id)
        let selectedPorseshname ={
            id:  porseshnamehRow.rows.item(0).id,
            porseshnamehNumber:  porseshnamehRow.rows.item(0).porseshnamehNumber,
            idProject:  porseshnamehRow.rows.item(0).idProject,
            idPorseshgar: porseshnamehRow.rows.item(0).idPorseshgar,
            completedDate: porseshnamehRow.rows.item(0).completedDate,
            startTime: porseshnamehRow.rows.item(0).startTime,
            Longitude: porseshnamehRow.rows.item(0).longitude,
            latitude: porseshnamehRow.rows.item(0).latitude,
        }
        // console.log("selectedPorseshname : ", selectedPorseshname);
        this.props.setPorseshnameh(selectedPorseshname);

        let pasokhgooRow=pasokhgooResult.rows.item(0);

      console.log('porseshnameProps : ',this.props.porseshnameh);

      Actions.pasokhgoo({pasokhgooRow,idPorseshnameh})
    }
    deleteConfirm = (idPorseshnameh)=>{
        Alert.alert(
            'حذف پرسشنامه',
            'آیا مطمین هستید؟!',
            [
                {text: 'خیر'},
                {text: 'بله', onPress: () => this.delete(idPorseshnameh), style: 'cancel'},
            ]
        );
    }

    delete = async (idPorseshnameh)=>{
       let d = await deletePorseshnameh(idPorseshnameh);
       // console.warn(d);
        const filteredData = this.state.data.filter(item => item.id !== idPorseshnameh);
        this.setState({ data: filteredData });
        // Actions.push('porseshnamehSearch');
        this.getPorseshnamehListFromDB(this.props.idProject)
    }
    detail = (idPorseshnameh)=>{
        alert(idPorseshnameh);
    }

    render() {
        // console.log("thisFlatListItem : ", this.state.thisFlatListItem);
        const flatListItem = this.props.flatListItem;

        return (
            <View style={{flex: 1}}>
                <FlatList
                    data={this.state.data}
                    ItemSeparatorComponent={this.renderSeparator}
                    keyExtractor={(item, index) => index.toString()}
                    ListHeaderComponent={this.renderHeader}
                    renderItem={({item}) => (
                        <View style={{flexDirection: 'row', backgroundColor: '#f3f7f7'}}>
                            <View style={{width: '100%', flexDirection: 'column',}}>
                                <View style={{flexDirection: 'row-reverse', width: '100%'}}>
                                    <View style={{paddingRight: 10, width: '70%'}}>
                                        <Text
                                            style={[tab.tabBodyText, common.bodyText, {fontSize: 18}]}>{item.name + ' ' + item.fName}</Text>
                                    </View>
                                    <View style={{
                                        paddingLeft: 10,
                                        width: '30%',
                                        flexDirection: 'row',
                                        alignItems:'center'
                                    }}>
                                        <TouchableOpacity style={{flexDirection: 'row',paddingRight:10}}>
                                            <Icon name="menufold" size={23} backgroundColor="#3b5998" color="#4CAE1F" onPress={()=>this.detail(item.id)}/>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{flexDirection: 'row',paddingRight:10}}>
                                            <Icon name="delete" size={23} backgroundColor="#3b5998" color="red" onPress={()=>this.deleteConfirm(item.id)}/>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{flexDirection: 'row',paddingRight:10}}>
                                            <Icon name="edit" size={23} backgroundColor="#3b5998" color="blue" onPress={()=>this.edit(item.id)}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{flexDirection: 'row-reverse', justifyContent: 'center'}}>
                                    <View style={{paddingLeft: 15}}>
                                        <Text style={[tab.tabBodyText, common.bodyText]}>شماره پرسشنامه :
                                            <Text
                                                style={[tab.tabBodyText, common.bodyText]}>{item.porseshnamehNumber}</Text>
                                        </Text>
                                    </View>
                                    <View>
                                        <Text style={[tab.tabBodyText, common.bodyText]}>تاریخ تکمیل :
                                            <Text style={[tab.tabBodyText, common.bodyText]}>{item.completedDate}</Text>
                                        </Text>
                                    </View>
                                </View>
                            </View>

                        </View>
                    )}
                />
            </View>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        porseshnameh: state.porseshnameh,
        user: state.user
    }
}
const mapDispathToProps = dispatch => {
    return {
        setPorseshnameh: porseshnameh => {
            dispatch(setPorseshnameh(porseshnameh))
        }
    }
}
export default connect(mapStateToProps,mapDispathToProps)(SearchPorseshnamehFlatList)
const tab = EstyleSheet.create({
    cardItem: {
        backgroundColor: '#e6eaea'

    },
    tabBody: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    tabBodyText: {
        height: 50,
        lineHeight: 50,
        justifyContent: 'center',
        alignItems: 'center'
    }

})