import React from 'react';
import {
    Container,
    Header,
    Title,
    Right,
    Alert,
    Left,
    Content,
    Card,
    CardItem,
    Body,
    Input,
    Item,
    Button
} from 'native-base';

import {
    Text,
    StyleSheet,
    ScrollView,
    View,
    Dimensions,
    TextInput,
    ProgressBarAndroid,
    Linking,
    ImageBackground,
    TouchableOpacity
} from 'react-native';
//Import Basic React Native Component
import Video from 'react-native-video';
//Import React Native Video to play video
import MediaControls, {PLAYER_STATES} from 'react-native-media-controls';


import Modal from 'react-native-modalbox';
import * as Progress from 'react-native-progress';

import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome5'
import RNFS from 'react-native-fs'
import {form} from "./../../assets/styles/index";
import {card} from "../../assets/css/SyncWithServer";
import {common} from './../../assets/css/common';
import {modal} from './../../assets/css/modal'
import RadioGroup from 'react-native-radio-buttons-group'
import CheckboxGroupForOpenOptionQType from '../classes/CheckboxGroupForOpenOptionQType'

import {questionStyle} from '../../assets/css/questionStyle'
import {questionLoadFunc} from './QuestionLoad';
import {storeAnswerToDB} from './../database/answer';


class ShowVideo extends React.Component {
    videoPlayer;

    constructor(props) {
        super(props);
        this.state = {
            rowNumber:this.props.rowNumber,
            row: [],
            currentTime: 0,
            duration: 0,
            isFullScreen: false,
            isLoading: true,
            paused: false,
            playerState: PLAYER_STATES.PLAYING,
            screenType: 'content',
            videoFileName:null,
        }
    }

    componentDidMount() {
        this.setState({
            row: this.props.row,
            percentage: this.props.percentage,
            videoFileName :`${RNFS.DocumentDirectoryPath}/public/imagesDownload/question/${this.props.row.idInHost}.mp4`,

        });

    }

    onSeek = seek => {
        //Handler for change in seekbar
        this.videoPlayer.seek(seek);
    };

    onPaused = playerState => {
        //Handler for Video Pause
        this.setState({
            paused: !this.state.paused,
            playerState,
        });
    };

    onReplay = () => {
        //Handler for Replay
        this.setState({playerState: PLAYER_STATES.PLAYING});
        this.videoPlayer.seek(0);
    };

    onProgress = data => {
        const {isLoading, playerState} = this.state;
        // Video Player will continue progress even if the video already ended
        if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
            this.setState({currentTime: data.currentTime});
        }
    };

    onLoad = data => this.setState({duration: data.duration, isLoading: false});

    onLoadStart = data => this.setState({isLoading: true});

    onEnd = () => this.setState({playerState: PLAYER_STATES.ENDED});

    onError = () => alert('Oh! ', error);

    exitFullScreen = () => {
        alert('Exit full screen');
    };

    enterFullScreen = () => {
    };

    onFullScreen = () => {
        if (this.state.screenType == 'content')
            this.setState({screenType: 'cover'});
        else this.setState({screenType: 'content'});
    };
    renderToolbar = () =>{
        (
            <View>
                <Text> toolbar </Text>
            </View>
        );
    }
    onSeeking = currentTime => this.setState({currentTime});

    jumpToQuestion = () => {
        var idProject = this.props.project.idInHost;
        var idPorseshnameh = this.props.porseshnameh.id;
        var rowNumber = this.state.rowNumber;
        // console.log('rowNumberInjumpToQuestionSingleResponseCase:',rowNumber)
        if (this.state.rowNumber != 0) {
            questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, this.state.rowNumber)
        }
        else {
            alert("شماره سوال را وارد کنید.")
        }
    };

    storeAnswer = () => {
        let rowNumber = (Number(this.state.rowNumber) + 1);
        questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, rowNumber);
    };
    loadPreviousQuestion = () => {
        rowNumber = (Number(this.state.rowNumber) - 1);
        questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, rowNumber);

    };

    render() {
        var progressColor = 'orange';
        if (this.state.percentage / 100 > 50) {
            progressColor = "green"
        }

        var policyansToOtherTextOptionsBool = false, policyansToOtherTextOptions = '';
        if (this.props.policyansToOtherTextOptions != '') {
            policyansToOtherTextOptions = this.props.policyansToOtherTextOptions;
            policyansToOtherTextOptionsBool = true;
        }


        const componenetTitle =this.props.project.subject;
        const oneCardbuttonText = 'ادامه';
        return (
            <Container>
                <ImageBackground
                    source={require('./../../assets/images/AppImage/question.jpg')}
                    style={common.fullImageBackground}
                    imageStyle={{borderRadius: 2}}>
                    <QuestionHeader
                        myOnPress={this.loadPreviousQuestion}
                        project={this.props.project}
                        modal={this.refs.modal4}
                    />
                    <Content padder style={{marginTop: 20}}>
                        <ScrollView style={{height: "100%", flex: 1}}>
                            <Card>
                                <CardItem bordered style={card.cardTitle}>
                                    <Text
                                        style={[card.cardTitle, common.titleText]}>{this.state.row.text}</Text>
                                    {policyansToOtherTextOptionsBool ?
                                        <Text
                                            style={[card.cardTitle, common.ansToOtherQTextStyle]}>/جواب هایی که به سوال های قبل داده اید : {policyansToOtherTextOptions}</Text>
                                        : null}
                                </CardItem>
                                <CardItem style={{minHeight: 150, flexDirection: 'column', paddingTop: 30}}>
                                    <Text style={{
                                        fontSize: 15,
                                        fontFamily: 'IRANSansMobile',
                                        color: 'red',
                                        marginBottom: 10
                                    }}>محل نمایش ویدئو:</Text>
                                    <Text
                                        style={{
                                            fontSize: 16,
                                            fontFamily: 'IRANSansMobile_Bold',
                                            color: 'black'
                                        }}>{this.state.row.text}
                                    </Text>

                                    <Video
                                        onEnd={this.onEnd}
                                        onLoad={this.onLoad}
                                        onLoadStart={this.onLoadStart}
                                        onProgress={this.onProgress}
                                        paused={this.state.paused}
                                        ref={videoPlayer => (this.videoPlayer = videoPlayer)}
                                        resizeMode={this.state.screenType}
                                        onFullScreen={this.state.isFullScreen}
                                        source={{uri: this.state.videoFileName}}
                                        style={styles.mediaPlayer}
                                        volume={10}
                                    />
                                    <MediaControls
                                        duration={this.state.duration}
                                        isLoading={this.state.isLoading}
                                        mainColor="#333"
                                        onFullScreen={this.onFullScreen}
                                        onPaused={this.onPaused}
                                        onReplay={this.onReplay}
                                        onSeek={this.onSeek}
                                        onSeeking={this.onSeeking}
                                        playerState={this.state.playerState}
                                        progress={this.state.currentTime}
                                        toolbar={this.renderToolbar()}
                                    />


                                </CardItem>
                                <CardItem footer bordered>
                                    <TouchableOpacity style={{width: '100%'}} onPress={this.storeAnswer}>
                                        <Button style={form.submitButton} full onPress={this.storeAnswer}>
                                            <Text style={[form.submitText, {paddingLeft: 10, paddingRight: 5}]}
                                                  onPress={this.storeAnswer}>{oneCardbuttonText}</Text>
                                            <Icon name='check' size={20} color="#ffffff" solid style={{marginRight: 10}}
                                                  onPress={this.storeAnswer}/>
                                        </Button>
                                    </TouchableOpacity>
                                </CardItem>
                            </Card>
                        </ScrollView>
                    </Content>
                    <Modal style={[modal.modal, modal.modal4]} position={"bottom"} ref={"modal4"}>
                        <Card style={modal.modalMainCard}>
                            <CardItem bordered style={card.cardTitle}>
                                <Text style={[common.headerText, {color: 'white'}]}>اطلاعات بیشتر . . .</Text>
                            </CardItem>
                            <CardItem style={{flexDirection: 'column'}}>
                                <Text>{this.state.percentage}% - <Text
                                    style={common.bodyText}>( {this.state.row.rowNumber}
                                    از {this.state.questionCount})</Text></Text>
                                <Progress.Bar progress={this.state.percentage / 100} width={300}
                                              color={(this.state.percentage / 100) < 0.5 ? "red" : "green"}
                                              height={30}></Progress.Bar>
                            </CardItem>
                            <CardItem style={card.modalCardItem2}>
                                <Button info style={[common.bodyText, modal.modalButton]} onPress={this.jumpToQuestion}><Text
                                    onPress={this.jumpToQuestion} style={common.bodyText}> پرش </Text></Button>
                                <Input
                                    style={[common.textInput, modal.modalInput]}
                                    keyboardType='numeric'
                                    onSubmitEditing={()=>this.jumpToQuestion()}
                                    placeholder='شماره سوال؟'
                                    onChangeText={val => this.setState({rowNumber: val})}/>
                            </CardItem>
                            <View style={card.modalView}>
                                <Text style={[common.bodyText]}>بیشتر بدانید :
                                    <Text style={common.webLink} onPress={() => {
                                        Linking.openURL(this.props.user.webSiteLink)
                                    }}>{this.props.user.companyName} </Text>
                                </Text>
                            </View>
                        </Card>
                    </Modal>
                </ImageBackground>
            </Container>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        porseshnameh: state.porseshnameh,
        project: state.project,
        user: state.user
    }
};
const mapDispathToProps = dispatch => {
    return {
        setPorseshnameh: porseshnameh => {
            dispatch(setPorseshnameh(porseshnameh))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(ShowVideo);


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    toolbar: {
        marginTop: 30,
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 5,
    },
    mediaPlayer: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        backgroundColor: 'black',
    },
});
