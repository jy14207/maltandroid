import {
    insertPorseshnamehToDB, selectPorseshnamehList, deleteQuery, selectPorseshnamehById, selectPorseshnamehByNumber,
    selectAllPorseshnameFromDB, updatePorseshnamehAfterUpload, updatePorseshnamehWhenEdit
} from './Model';

export const insertPorseshnameh = (porseshnamehRow,insertedId) => {
    insertPorseshnamehToDB (porseshnamehRow, (cbResult) => {
        // console.log('idInPorseshnameh.js:',cbResult);
        insertedId(cbResult);
    });
}
export const getPorseshnamehFromDB=(idProject,idUserInHost)=>{
    return new Promise((resolve,reject)=>{
        selectPorseshnamehList(idProject,idUserInHost,(cbResult)=>{
            // console.log("cbResult :",cbResult)
            resolve(cbResult);
        })
    })
}
export const getAllPorseshnamehFromDB=(idProject,idUserInHost)=>{
    return new Promise((resolve,reject)=>{
        selectAllPorseshnameFromDB(idProject,(cbResult)=>{
            resolve(cbResult);
        })
    })
}

export const deletePorseshnameh=(id)=>{
    console.log('deleteID :',id);
    return new Promise((resolve,reject)=>{
    resolve(deleteQuery('prseshname','id' ,id));
    })
}
export const getPorseshnamehById=(idPorseshnameh)=>{
    return new Promise((resolve,reject)=>{
        selectPorseshnamehById(idPorseshnameh,(cbResult)=>{
            resolve(cbResult);
        })
    });
}

export const getPorseshnamehByNumber =(porseshnamehNumber,idProject) => {
    // console.log("getPorseshnamehByNumber");
    return new Promise((resolve,reject)=>{
        selectPorseshnamehByNumber(porseshnamehNumber,idProject,(cbResult)=>{
            resolve(cbResult);
        });
    })
}
export const updateUploadToHostAfterUploadPorseshname =(id,idInsertedInHost)=>{
    updatePorseshnamehAfterUpload(id,idInsertedInHost);
}
export const updateUploadToHostFieldSet0WhenEdit =(id)=>{
    updatePorseshnamehWhenEdit(id);
}