import {SET_PORSESHNAMEH} from "../actions/type";

const initialstate = {
    id: null,
    porseshnamehNumber: null,
    idProject: null,
    idPorseshgar: null,
    completedDate: null,
    startTime: null,
    Longitude: null,
    latitude: null,
}
export default porseshnameh = (state = initialstate, action = {}) => {
    switch (action.type) {
        case SET_PORSESHNAMEH:
            const {porseshnameh} = action;
            return {
                id: porseshnameh.id,
                porseshnamehNumber:porseshnameh.porseshnamehNumber,
                idProject: porseshnameh.idProject,
                idPorseshgar: porseshnameh.idPorseshgar,
                completedDate: porseshnameh.completedDate,
                startTime: porseshnameh.startTime,
                Longitude: porseshnameh.longitude,
                latitude: porseshnameh.latitude,
            }
            break;
        default :
            return state;

    }
}