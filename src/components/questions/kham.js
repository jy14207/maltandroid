import React from 'react';
import {Container, Header, Title, Right, Left, Content, Card, CardItem, Body, Input, Item, Button} from 'native-base';

import {
    Text, StyleSheet, ScrollView, View, Dimensions, TextInput, ProgressBarAndroid, Linking
} from 'react-native';
import Modal from 'react-native-modalbox';
import * as Progress from 'react-native-progress';

import Snackbar from 'react-native-snackbar';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome5'

import {form} from "./../../assets/styles/index";
import {card} from "../../assets/css/SyncWithServer";
import {common} from './../../assets/css/common';
import {modal} from './../../assets/css/modal'

import {questionLoadFunc} from './QuestionLoad';
import QuestionHeader from "../ui/QuestionHeader";

class openQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            row: [],
            answer: [],
            socialPolicyIs: false,
            stopPolicyIs: false,
            countPolicyIs: false,
            rowNumber: 0,
            percentage: 0,
            questionCount: 0
        }
    }

    componentDidMount() {
        console.log("4");

        this.setState({
            row: this.props.row,
            answer: this.props.answer,
            socialPolicyIs: this.props.socialPolicyIs,
            stopPolicyIs: this.props.stopPolicyIs,
            countPolicyIs: this.props.countPolicyIs,
            percentage: this.props.percentage,
            questionCount: this.props.questionCount
        });

    }

    snackPolicy(socialPolicyMessage) {
        Snackbar.show({
            title: socialPolicyMessage,
            duration: Snackbar.LENGTH_INDEFINITE,
            action: {
                title: 'بستن',
                color: 'green',

            },
        })
    }

    jumpToQuestion = () => {
        var idProject = this.props.project.idInHost;
        var idPorseshnameh = this.props.porseshnameh.id;
        var rowNumber = this.state.rowNumber;
        // alert(this.state.rowNumber);
        if (this.state.rowNumber != 0) {
            questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, this.state.rowNumber)
        }
        else {
            alert("شماره سوال را وارد کنید.")
        }

    }

    render() {
        var progressColor = 'orange';
        if (this.state.percentage / 100 > 50) {
            progressColor = "green"
        }

        var policy = false, socialPolicyMessage = false, stopPolicyIsMessage = false, countPolicyIsMessage = false;
        if (this.state.socialPolicyIs == true) {
            socialPolicyMessage = 'جواب این سوال در تعیین طبقه اجتماعی موثر است.';
            policy = true;
        }
        if (this.state.stopPolicyIs == true) {
            stopPolicyIsMessage = 'سیاست توقف مصاحبه برای این سوال لحاظ شده است.';
            policy = true;
        }
        if (this.state.countPolicyIs == true) {
            countPolicyIsMessage = 'سیاست تعداد خاص از پرسشنامه برای این سوال لحاظ شده است.';
            policy = true;
        }

        console.log("socialPolicyIs=", this.state.socialPolicyIs);

        const componenetTitle = this.props.project.subject;
        const oneCardbuttonText = 'ثبت';
        return (
            <Container>
                <QuestionHeader
                    myOnPress={this.loadPreviousQuestion}
                    project={this.props.project}
                    modal={this.refs.modal4}
                />
                <Content padder style={{marginTop: 20}}>
                    {policy ? <View hide={policy} style={common.questionPolicyAlert}>
                        {socialPolicyMessage ?
                            <Text style={common.questionPolicyTextAlert}> - {socialPolicyMessage}</Text> : null}
                        {stopPolicyIsMessage ?
                            <Text style={common.questionPolicyTextAlert}> - {stopPolicyIsMessage}</Text> : null}
                        {countPolicyIsMessage ?
                            <Text style={common.questionPolicyTextAlert}> - {countPolicyIsMessage}</Text> : null}
                    </View> : null}
                    <ScrollView style={{height: "100%", flex: 1}}>
                        <Card>
                            <CardItem bordered style={card.cardTitle}>
                                <Text
                                    style={[card.cardTitle, common.titleText]}>{this.state.row.rowNumber + "- " + this.state.row.text}
                                    <Text
                                        style={[card.cardTitle, common.moreExplanationText]}>{this.state.row.moreExplanation}</Text></Text>
                            </CardItem>
                            <CardItem style={card.body}>
                                {/*<Text>{this.props.row.text}</Text>*/}
                                <Text>salam+{this.props.socialPolicyIs}</Text>
                            </CardItem>
                            <CardItem footer bordered>
                                <Button style={form.submitButton} full>
                                    <Text style={[form.submitText, {
                                        paddingLeft: 10,
                                        paddingRight: 5
                                    }]}>{oneCardbuttonText}</Text>
                                    <Icon name='check' size={20} color="#ffffff" solid style={{marginRight: 10}}/>
                                </Button>
                            </CardItem>
                        </Card>
                    </ScrollView>
                </Content>
                <Modal style={[modal.modal, modal.modal4]} position={"bottom"} ref={"modal4"}>
                    <Card style={{flexDirection: 'column', margin: 0, width: '100%', height: 250,}}>
                        <CardItem bordered style={card.cardTitle}>
                            <Text style={[common.headerText, {color: 'white'}]}>اطلاعات بیشتر . . .</Text>
                        </CardItem>
                        <CardItem style={{flexDirection: 'column'}}>
                            <Text>{this.state.percentage}% - <Text style={common.bodyText}>( {this.state.row.rowNumber}
                                از {this.state.questionCount})</Text></Text>
                            <Progress.Bar progress={this.state.percentage / 100} width={300}
                                          color={(this.state.percentage / 100) < 0.5 ? "red" : "green"}
                                          height={30}></Progress.Bar>
                        </CardItem>
                        <CardItem style={card.body}>
                            <Input style={[common.textInput, modal.modalInput]}
                                   keyboardType='numeric'
                                   placeholder='شماره سوال؟'
                                   onChangeText={val => this.setState({rowNumber: val})}/>
                            <Button info style={[common.bodyText, modal.modalButton]}
                                    onPress={this.jumpToQuestion}><Text onPress={this.jumpToQuestion}
                                                                        style={common.bodyText}> پرش </Text></Button>
                        </CardItem>
                        <CardItem style={{flexDirection: 'column'}}>
                            <Text style={[common.bodyText, {paddingBottom: 10,}]}>بیشتر بدانید :
                                <Text style={common.webLink} onPress={() => {
                                    Linking.openURL(this.props.user.webSiteLink)
                                }}>{this.props.user.companyName} </Text>
                            </Text>
                        </CardItem>
                    </Card>
                </Modal>
            </Container>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        porseshnameh: state.porseshnameh,
        project: state.project,
        user: state.user
    }
}
const mapDispathToProps = dispatch => {
    return {
        setPorseshnameh: porseshnameh => {
            dispatch(setPorseshnameh(porseshnameh))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(openQuestion);

/*
            showSubmitCheck:false,
            this.setState({showSubmitCheck:true});
            <StoreButton onPress={this.storeAnswer} showSubmitCheck={this.state.showSubmitCheck}/>

*/
