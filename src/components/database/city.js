import { deleteQuery ,insertCity,selectCityList} from './Model';

export const deleteCityOfThisProject =(idProject )=>{
    return new Promise((resolve,reject)=>{
        deleteQuery("city" , 'idProject' , idProject,resolve);
    })
}

export const insertCitiesToDatabese=(citiesRows)=>{
    insertCity(citiesRows);
}

export const cityList =(idProject,cityListResult)=>{
    selectCityList(idProject,(results)=>{
        cityListResult(results);
    })
}