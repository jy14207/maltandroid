import React from 'react';
import {Container, Header, Title, Right, Left, Content, Card, CardItem, Body, Input, Item, Button} from 'native-base';

import {
    Text,
    StyleSheet,
    ScrollView,
    View,
    Dimensions,
    TextInput,
    ProgressBarAndroid,
    Linking,
    ImageBackground,
    TouchableOpacity,
    WebView,
} from 'react-native';
import Modal from 'react-native-modalbox';
import * as Progress from 'react-native-progress';

import Snackbar from 'react-native-snackbar';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome5'

import {form} from "./../../assets/styles/index";
import {card} from "../../assets/css/SyncWithServer";
import {common} from './../../assets/css/common';
import {modal} from './../../assets/css/modal'

import {questionStyle} from '../../assets/css/questionStyle'
import {questionLoadFunc} from './QuestionLoad';
import {storeAnswerToDB} from './../database/answer';
import {getAnswerPromiseOfQuestionRow} from './../database/answer'
import QuestionHeader from "../ui/QuestionHeader";
import StoreButton from "../ui/StoreButton";

class NoResponse extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            row: [],
            showSubmitCheck:false,
            answer: [],
            socialPolicyIs: false,
            stopPolicyIs: false,
            countPolicyIs: false,
            rowNumber: 0,
            percentage: 0,
            questionCount: 0,
            text: '',
            error: '#898c8c',
            update: false,
        }
    }

    componentDidMount() {
        this.setState({
            row: this.props.row,
            percentage: this.props.percentage,
            questionCount: this.props.questionCount,
            rowNumber: this.props.rowNumber,

        });
    }

    snackPolicy(socialPolicyMessage) {
        Snackbar.show({
            title: socialPolicyMessage,
            duration: Snackbar.LENGTH_INDEFINITE,
            action: {
                title: 'بستن',
                color: 'green',

            },
        })
    }

    jumpToQuestion = () => {
        var idProject = this.props.project.idInHost;
        var idPorseshnameh = this.props.porseshnameh.id;
        var rowNumber = this.state.rowNumber;
        if (this.state.rowNumber != 0 && this.state.rowNumber != null && this.state.rowNumber != '') {
            console.log('rowNumberInJump:', this.state.rowNumber)
            questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, this.state.rowNumber)
        }
        else {
            alert("شماره سوال را وارد کنید.")
        }
    };

    storeAnswer = () => {
        this.setState({showSubmitCheck:true});
        let rowNumber = (Number(this.state.rowNumber) + 1);
        questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, rowNumber);
    };
    loadPreviousQuestion = () => {
        rowNumber = (Number(this.state.rowNumber) - 1);
        questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, rowNumber);

    };

    render() {
        var policyansToOtherTextOptionsBool = false, policyansToOtherTextOptions = '';
        if (this.props.policyansToOtherTextOptions != '') {
            policyansToOtherTextOptions = this.props.policyansToOtherTextOptions;
            policyansToOtherTextOptionsBool = true;
        }

        var progressColor = 'orange';
        if (this.state.percentage / 100 > 50) {
            progressColor = "green"
        }
        const componenetTitle = this.props.project.subject;
        const oneCardbuttonText = 'ادامه';
        return (
            <Container>
                <ImageBackground
                    source={require('./../../assets/images/AppImage/question.jpg')}
                    style={common.fullImageBackground}
                    imageStyle={{borderRadius: 2}}>
                    <QuestionHeader
                        myOnPress={this.loadPreviousQuestion}
                        project={this.props.project}
                        modal={this.refs.modal4}
                    />
                    <Content padder style={{marginTop: 20}}>
                        <ScrollView style={{height: "100%", flex: 1}}>
                            <Card style={card.mainCard}>
                                <CardItem bordered style={card.cardItemHeader}>
                                    <Text
                                        style={[card.cardTitle, common.moreExplanationText]}>{this.state.row.moreExplanation}</Text>
                                    {policyansToOtherTextOptionsBool ?
                                        <Text
                                            style={[card.cardTitle, common.ansToOtherQTextStyle]}>جواب هایی که به سوال های قبل داده اید : {policyansToOtherTextOptions}</Text>
                                        : null}
                                </CardItem>
                                <CardItem style={{minHeight: 150, flexDirection: 'column', paddingTop: 30}}>
                                    <Text style={{
                                        fontSize: 15,
                                        fontFamily: 'IRANSansMobile',
                                        color: 'red',
                                        marginBottom: 10
                                    }}>این یک متن بدون پاسخ است :</Text>

                                    <Text
                                        style={{
                                            fontSize: 16,
                                            fontFamily: 'IRANSansMobile_Bold',
                                            color: 'black',
                                            textAlign:'justify'
                                        }}>{this.state.row.text}
                                    </Text>


                                </CardItem>
                                <CardItem footer bordered>
                                    <StoreButton onPress={this.storeAnswer} showSubmitCheck={this.state.showSubmitCheck}/>
                                </CardItem>
                            </Card>
                        </ScrollView>
                    </Content>
                    <Modal style={[modal.modal, modal.modal4]} position={"bottom"} ref={"modal4"}>
                        <Card style={{flexDirection: 'column', margin: 0, width: '100%', height: 250,}}>
                            <CardItem bordered style={card.cardTitle}>
                                <Text style={[common.headerText, {color: 'white'}]}>اطلاعات بیشتر . . .</Text>
                            </CardItem>
                            <CardItem style={{flexDirection: 'column'}}>
                                <Text>{this.state.percentage}% - <Text
                                    style={common.bodyText}>( {this.state.row.rowNumber}
                                    از {this.state.questionCount})</Text></Text>
                                <Progress.Bar progress={this.state.percentage / 100} width={300}
                                              color={(this.state.percentage / 100) < 0.5 ? "red" : "green"}
                                              height={30}></Progress.Bar>
                            </CardItem>
                            <CardItem style={card.body}>


                            </CardItem>
                            <CardItem style={{flexDirection: 'column'}}>
                                <Text style={[common.bodyText, {paddingBottom: 10,}]}>بیشتر بدانید :
                                    <Text style={common.webLink} onPress={() => {
                                        Linking.openURL(this.props.user.webSiteLink)
                                    }}>{this.props.user.companyName} </Text>
                                </Text>
                            </CardItem>
                        </Card>
                    </Modal>
                </ImageBackground>
            </Container>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        porseshnameh: state.porseshnameh,
        project: state.project,
        user: state.user
    }
}
const mapDispathToProps = dispatch => {
    return {
        setPorseshnameh: porseshnameh => {
            dispatch(setPorseshnameh(porseshnameh))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(NoResponse);
