import React from 'react';
import {
    Container,
    Header,
    Title,
    Right,
    Alert,
    Left,
    Content,
    Card,
    CardItem,
    Body,
    Input,
    Item,
    Button
} from 'native-base';

import {
    Text,
    StyleSheet,
    ScrollView,
    View,
    Dimensions,
    TextInput,
    ProgressBarAndroid,
    Linking,
    ImageBackground,
    TouchableOpacity,
    LinearGradient,
    ActivityIndicator,
} from 'react-native';
import Modal from 'react-native-modalbox';
import * as Progress from 'react-native-progress';

import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome5'

import {form} from "./../../assets/styles/index";
import {card} from "../../assets/css/SyncWithServer";
import {common} from './../../assets/css/common';
import {modal} from './../../assets/css/modal'
import RadioGroup from '../classes/RadioButtonsGroup'

import {questionStyle} from '../../assets/css/questionStyle'
import {questionLoadFunc} from './QuestionLoad';
import {storeAnswerToDB} from './../database/answer'
import {checkPolicyForJumpToOtherQuestion, checkPolicyForStopAnswer} from "../database/policy";
import MyButton from "../ui/MyButton";
import StoreButton from "../ui/StoreButton";
import QuestionHeader from "../ui/QuestionHeader";

class singleResponse extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            row: [],
            showSubmitCheck: false,
            answer: [],
            socialPolicyIs: false,
            stopPolicyIs: false,
            countPolicyIs: false,
            rowNumber: 0,
            percentage: 0,
            questionCount: 0,
            text: '',
            error: '#898c8c',
            update: false,
            selectedButton: null,
            optionData:
                {
                    label: '',
                    idInHost: '',
                    moreExplanation: '',
                    selected: false,

                }
            ,
        }
    }

    componentWillMount() {
        var tempOptionData = [];
        for (let i = 0; i < this.props.options.length; i++) {
            tempOptionData.push({
                label: this.props.options[i].textOption,
                idInHost: this.props.options[i].idInHost,
                moreExplanation: this.props.options[i].moreExplanation,
                selected: false,
            });
            if (this.props.answer != null && (this.props.options[i].idInHost == Number(this.props.answer.idOption))) {
                this.setState({selectedButton: this.props.options[i]});
                tempOptionData[i].selected = true;
            }
        }
        this.setState({
            optionData: tempOptionData,
            rowNumber: this.props.rowNumber,
        });
        // console.log('rowNumberInComponentWillMountSingleResponseCase:',this.state.rowNumber)

    }

    componentDidMount() {
        this.setState({
            row: this.props.row,
            answer: this.props.answer,
            socialPolicyIs: this.props.socialPolicyIs,
            stopPolicyIs: this.props.stopPolicyIs,
            countPolicyIs: this.props.countPolicyIs,
            percentage: this.props.percentage,
            questionCount: this.props.questionCount,
            // rowNumber: this.props.rowNumber,

        });
        // console.log('rowNumberInComponentDidMountSingleResponseCase:',this.state.rowNumber)

        if (this.props.answer != null) {
            this.setState({
                update: true,
                text: this.props.answer['text'],
            })
        }
    }


    jumpToQuestion = () => {
        var idProject = this.props.project.idInHost;
        var idPorseshnameh = this.props.porseshnameh.id;
        var rowNumber = this.state.rowNumber;
        // console.log('rowNumberInjumpToQuestionSingleResponseCase:',rowNumber)
        if (this.state.rowNumber != 0) {
            questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, this.state.rowNumber)
        }
        else {
            alert("شماره سوال را وارد کنید.")
        }
    }

    storeAnswer = async () => {
        let idProject = this.props.project.idInHost;
        let newRowNumber = 0, rowNumber = 0, stopedAnsweing = 0;

        if (this.state.selectedButton == null && this.state.row.allowNull == 'no') {
            this.setState({error: 'red'});
            alert("پاسخ به این سوال اجباری است");
        }
        else {
            var idQuestion = this.state.row.idInHost;
            let idPorseshnameh = this.props.porseshnameh.id;
            let selectedButton = this.state.selectedButton;
            this.setState({error: '#898c8c'});
            let text = this.state.text;
            if (this.state.update === false)
                await storeAnswerToDB(idPorseshnameh, idQuestion, selectedButton.idInHost, null, 'insert');
            else
                await storeAnswerToDB(idPorseshnameh, idQuestion, selectedButton.idInHost, null, 'update', this.state.answer['id']);
            newRowNumber = await checkPolicyForJumpToOtherQuestion(idProject, idQuestion, selectedButton.idInHost);
            if (newRowNumber > 0)
                rowNumber = newRowNumber;
            else
                rowNumber = (Number(this.state.rowNumber) + 1);
            stopedAnsweing = await checkPolicyForStopAnswer(idProject, idQuestion, selectedButton.idInHost);
            // console.log("stopedAnsweing:", stopedAnsweing)
            if (stopedAnsweing == 1) {
                let reason = 'گزینه ای که انتخاب کرده اید باعث توقف مصاحبه شد.'
                Actions.finished({reason});
                return;
            }
            this.setState({showSubmitCheck: true});
            questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, rowNumber);
        }
    }
    loadPreviousQuestion = () => {
        rowNumber = (Number(this.state.rowNumber) - 1);
        questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, rowNumber);

    };
    onPress = data => {
        this.setState({optionData: data});
        let selectedButton = this.state.optionData.find(e => e.selected == true);
        this.setState({selectedButton: selectedButton});
    };

    render() {
        var progressColor = 'orange';
        if (this.state.percentage / 100 > 50) {
            progressColor = "green"
        }

        var policy = false, socialPolicyMessage = false, stopPolicyIsMessage = false, countPolicyIsMessage = false;
        if (this.state.socialPolicyIs == true) {
            socialPolicyMessage = 'جواب این سوال در تعیین طبقه اجتماعی موثر است.';
            policy = true;
        }
        if (this.state.stopPolicyIs == true) {
            stopPolicyIsMessage = 'سیاست توقف مصاحبه برای این سوال لحاظ شده است.';
            policy = true;
        }
        if (this.state.countPolicyIs == true) {
            countPolicyIsMessage = 'سیاست تعداد خاص از پرسشنامه برای این سوال لحاظ شده است.';
            policy = true;
        }
        var policyansToOtherTextOptionsBool = false, policyansToOtherTextOptions = '';
        if (this.props.policyansToOtherTextOptions != '') {
            policyansToOtherTextOptions = this.props.policyansToOtherTextOptions;
            policyansToOtherTextOptionsBool = true;
        }

        const componenetTitle = this.props.project.subject;
        const oneCardbuttonText = 'ثبت';
        const colors = {
            blue: '#4285f4',
            gray: '#d8d8d8',
            grayDark: '#444',
            green: '#0f9d58',
            red: '#db4437',
            white: 'white'
        };
        return (
            <Container>
                <ImageBackground
                    source={require('./../../assets/images/AppImage/question.jpg')}
                    style={common.fullImageBackground}
                    imageStyle={{borderRadius: 2}}>
                    <QuestionHeader
                        myOnPress={this.loadPreviousQuestion}
                        project={this.props.project}
                        modal={this.refs.modal4}/>
                    <Content padder style={{marginTop: 20}}>
                        {policy ? <View hide={policy} style={common.questionPolicyAlert}>
                            {socialPolicyMessage ?
                                <Text style={common.questionPolicyTextAlert}> - {socialPolicyMessage}</Text> : null}
                            {stopPolicyIsMessage ?
                                <Text style={common.questionPolicyTextAlert}> - {stopPolicyIsMessage}</Text> : null}
                            {countPolicyIsMessage ?
                                <Text style={common.questionPolicyTextAlert}> - {countPolicyIsMessage}</Text> : null}
                        </View> : null}
                        <ScrollView style={{height: "100%", flex: 1}}>
                            <Card style={card.mainCard}>
                                <CardItem style={card.cardItemHeader}>
                                    <Text
                                        style={[card.cardTitle, common.titleText]}>{this.state.row.rowNumber + "- " + this.state.row.text}
                                        <Text
                                            style={[card.cardTitle, common.moreExplanationText]}>{this.state.row.moreExplanation}</Text>
                                        {policyansToOtherTextOptionsBool ?
                                            <Text
                                                style={[card.cardTitle, common.ansToOtherQTextStyle]}>/جواب هایی که به
                                                سوال های قبل داده اید : {policyansToOtherTextOptions}</Text>
                                            : null}</Text>
                                </CardItem>
                                <CardItem>
                                    <View style={{flex: 1, flexDirection: 'column'}}>
                                        <RadioGroup radioButtons={this.state.optionData} onPress={this.onPress}/>
                                    </View>
                                </CardItem>
                                <CardItem footer bordered>
                                    <StoreButton onPress={this.storeAnswer}
                                                 showSubmitCheck={this.state.showSubmitCheck}/>
                                </CardItem>
                            </Card>
                        </ScrollView>
                    </Content>
                    <Modal style={[modal.modal, modal.modal4]} position={"bottom"} ref={"modal4"}>
                        <Card style={modal.modalMainCard}>
                            <CardItem bordered style={card.cardTitle}>
                                <Text style={[common.headerText, {color: 'white'}]}>اطلاعات بیشتر . . .</Text>
                            </CardItem>
                            <CardItem style={{flexDirection: 'column'}}>
                                <Text>{this.state.percentage}% - <Text
                                    style={common.bodyText}>( {this.state.row.rowNumber}
                                    از {this.state.questionCount})</Text></Text>
                                <Progress.Bar progress={this.state.percentage / 100} width={300}
                                              color={(this.state.percentage / 100) < 0.5 ? "red" : "green"}
                                              height={30}></Progress.Bar>
                            </CardItem>
                            <CardItem style={card.modalCardItem2}>
                                <Button info style={[common.bodyText, modal.modalButton]} onPress={this.jumpToQuestion}><Text
                                    onPress={this.jumpToQuestion} style={common.bodyText}> پرش </Text></Button>
                                <Input
                                    style={[common.textInput, modal.modalInput]}
                                    keyboardType='numeric'
                                    onSubmitEditing={() => this.jumpToQuestion()}
                                    placeholder='شماره سوال؟'
                                    onChangeText={val => this.setState({rowNumber: val})}/>
                            </CardItem>
                            <View style={card.modalView}>
                                <Text style={[common.bodyText]}>بیشتر بدانید :
                                    <Text style={common.webLink} onPress={() => {
                                        Linking.openURL(this.props.user.webSiteLink)
                                    }}>{this.props.user.companyName} </Text>
                                </Text>
                            </View>
                        </Card>
                    </Modal>
                </ImageBackground>
            </Container>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        porseshnameh: state.porseshnameh,
        project: state.project,
        user: state.user
    }
};
const mapDispathToProps = dispatch => {
    return {
        setPorseshnameh: porseshnameh => {
            dispatch(setPorseshnameh(porseshnameh))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(singleResponse);
