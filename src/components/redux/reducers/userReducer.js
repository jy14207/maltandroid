import {SET_USER} from "../actions/type";

const initialstate = {
    id:null,
    idInHost:null,
    level:null,
    idCompany:null,
    name:null,
    fName:null,
    nationalCode:null,
    username:null,
    password:null,
    homeAdd:null,
    remember_Token:null,
    status:null,
    companyName:null,
    webSiteLink:null,
}

export default user = (state = initialstate, action = {}) => {
    switch (action.type){
        case SET_USER:
            const {user} =action;
            // console.log(user);
            return{
                id:user.id,
                idInHost:user.idInHost,
                level:user.level,
                idCompany:user.idCompany,
                name:user.name,
                fName:user.fName,
                nationalCode:user.nationalCode,
                username:user.username,
                password:user.password,
                homeAdd:user.homeAdd,
                remember_Token:user.remember_Token,
                status:user.status,
                companyName:user.companyName,
                webSiteLink:user.webSiteLink,
            }
            break;

        default:
            return state;
    }
}