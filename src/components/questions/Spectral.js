import React from 'react';
import {Container, Header, Title, Right, Left, Content, Card, CardItem, Body, Input, Item, Button} from 'native-base';

import {
    Text,
    StyleSheet,
    ScrollView,
    View,
    Dimensions,
    TextInput,
    ProgressBarAndroid,
    Linking,
    ImageBackground,
    TouchableOpacity,
    Slider
} from 'react-native';
import Modal from 'react-native-modalbox';
import * as Progress from 'react-native-progress';

import Snackbar from 'react-native-snackbar';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome5'

import {form} from "./../../assets/styles/index";
import {card} from "../../assets/css/SyncWithServer";
import {common} from './../../assets/css/common';
import {modal} from './../../assets/css/modal'

import {questionStyle} from '../../assets/css/questionStyle'
import {questionLoadFunc} from './QuestionLoad';
import {storeAnswerToDB} from './../database/answer';
import {getAnswerPromiseOfQuestionRow} from './../database/answer'
import QuestionHeader from "../ui/QuestionHeader";
import StoreButton from "../ui/StoreButton";

class Numeric extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            row: [],
            showSubmitCheck:false,
            answer: [],
            socialPolicyIs: false,
            stopPolicyIs: false,
            countPolicyIs: false,
            rowNumber: 0,
            percentage: 0,
            questionCount: 0,
            text: '',
            error: '#898c8c',
            update: false,
            maxMinErrorValue: '',
            maximumRange: false,
            sliderValue: 0,
            sliderAnswerValue: 0
        }
    }

    componentDidMount() {
        console.log(this.props.answer);
        this.setState({
            row: this.props.row,
            answer: this.props.answer,
            socialPolicyIs: this.props.socialPolicyIs,
            stopPolicyIs: this.props.stopPolicyIs,
            countPolicyIs: this.props.countPolicyIs,
            percentage: this.props.percentage,
            questionCount: this.props.questionCount,
            rowNumber: this.props.rowNumber,
            sliderValue: 0,
            sliderAnswerValue: 0


        });
        if (this.props.answer != null) {
            this.setState({
                update: true,
                sliderValue: this.props.answer['text'],
                sliderAnswerValue: this.props.answer['text'],
            })
        }
        else
            this.setState({
                sliderValue: 0,
                sliderAnswerValue: 0,
            })
    }


    jumpToQuestion = () => {
        var idProject = this.props.project.idInHost;
        var idPorseshnameh = this.props.porseshnameh.id;
        var rowNumber = this.state.rowNumber;
        if (this.state.rowNumber != 0 && this.state.rowNumber != null && this.state.rowNumber != '') {
            console.log('rowNumberInJump:', this.state.rowNumber)
            questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, this.state.rowNumber)
        }
        else {
            alert("شماره سوال را وارد کنید.")
        }
    };

    storeAnswer = async() => {

        if (this.state.sliderValue == '' && this.state.row.allowNull == 'no') {
            this.setState({error: 'red'});
            alert('خطا، جواب به این سوال اجباری است')

        }
        else {
            this.setState({maximumRange: false});
            this.setState({error: '#898c8c'});
            var idQuestion = this.state.row.idInHost;
            let idPorseshnameh = this.props.porseshnameh.id;
            let text = this.state.sliderValue;
            if (this.state.update === false)
              await  storeAnswerToDB(idPorseshnameh, idQuestion, 'spectral', text, 'insert');
            else
               await storeAnswerToDB(idPorseshnameh, idQuestion, 'spectral', text, 'update', this.state.answer['id']);

            this.setState({showSubmitCheck:true});
            let rowNumber = (Number(this.state.rowNumber) + 1);
            questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, rowNumber);
        }
    };
    loadPreviousQuestion=()=>{
        let rowNumber = (Number(this.state.rowNumber) - 1);
        questionLoadFunc(this.props.project.idInHost, this.props.porseshnameh.id, rowNumber);

    };

    render() {
        var errorMessage = '';
        var progressColor = 'orange';
        if (this.state.percentage / 100 > 50) {
            progressColor = "green"
        }

        var policy = false, socialPolicyMessage = false, stopPolicyIsMessage = false, countPolicyIsMessage = false;
        if (this.state.socialPolicyIs == true) {
            socialPolicyMessage = 'جواب این سوال در تعیین طبقه اجتماعی موثر است.';
            policy = true;
        }
        if (this.state.stopPolicyIs == true) {
            stopPolicyIsMessage = 'سیاست توقف مصاحبه برای این سوال لحاظ شده است.';
            policy = true;
        }
        if (this.state.countPolicyIs == true) {
            countPolicyIsMessage = 'سیاست تعداد خاص از پرسشنامه برای این سوال لحاظ شده است.';
            policy = true;
        }

        var policyansToOtherTextOptionsBool = false, policyansToOtherTextOptions = '';
        if (this.props.policyansToOtherTextOptions != '') {
            policyansToOtherTextOptions = this.props.policyansToOtherTextOptions;
            policyansToOtherTextOptionsBool = true;
        }

        const componenetTitle =this.props.project.subject;
        const oneCardbuttonText = 'ثبت';
        return (
            <Container>
                <ImageBackground
                    source={require('./../../assets/images/AppImage/question.jpg')}
                    style={common.fullImageBackground}
                    imageStyle={{borderRadius: 2}}>
                    <QuestionHeader
                        myOnPress={this.loadPreviousQuestion}
                        project={this.props.project}
                        modal={this.refs.modal4}
                    />
                    <Content padder style={{marginTop: 20}}>
                        {policy ? <View hide={policy} style={common.questionPolicyAlert}>
                            {socialPolicyMessage ?
                                <Text style={common.questionPolicyTextAlert}> - {socialPolicyMessage}</Text> : null}
                            {stopPolicyIsMessage ?
                                <Text style={common.questionPolicyTextAlert}> - {stopPolicyIsMessage}</Text> : null}
                            {countPolicyIsMessage ?
                                <Text style={common.questionPolicyTextAlert}> - {countPolicyIsMessage}</Text> : null}
                        </View> : null}
                        <ScrollView style={{height: "100%", flex: 1}}>
                            <Card style={card.mainCard}>
                                <CardItem bordered style={card.cardItemHeader}>
                                    <Text
                                        style={[card.cardTitle, common.titleText]}>{this.state.row.rowNumber + "- " + this.state.row.text}
                                        <Text
                                            style={[card.cardTitle, common.moreExplanationText]}>{this.state.row.moreExplanation}</Text>
                                        {policyansToOtherTextOptionsBool ?
                                            <Text
                                                style={[card.cardTitle, common.ansToOtherQTextStyle]}>/جواب هایی که به
                                                سوال های قبل داده اید : {policyansToOtherTextOptions}</Text>
                                            : null}</Text>
                                </CardItem>
                                <CardItem>
                                    <View style={{flex: 1, flexDirection: 'column'}}>
                                        <View style={{
                                            flex: 1,
                                            justifyContent: 'center',
                                        }}>
                                            <Text style={{color: 'black'}}>{this.state.sliderValue}</Text>
                                            <Slider
                                                style={{width: '100%'}}
                                                maximumValue={this.props.row.rangeValue}
                                                minimumValue={0}
                                                minimumTrackTintColor="#307ecc"
                                                maximumTrackTintColor="#000000"
                                                step={1}
                                                value={parseInt(this.state.sliderAnswerValue)}
                                                onValueChange={(sliderValue) => this.setState({sliderValue})}
                                            />
                                            <View style={{
                                                width: '100%',
                                                flex: 1,
                                                flexDirection: 'row',
                                                justifyContent: 'center'
                                            }}>
                                                <View style={{
                                                    width: '30%',
                                                    display: 'flex',
                                                    flexDirection: 'row',
                                                    justifyContent: 'flex-start'
                                                }}><Text style={{
                                                    fontSize: 13,
                                                    fontFamily: 'IRANSansMobile'
                                                }}>{(this.props.row.leftLeble ? this.props.row.leftLeble : '')}</Text></View>
                                                <View style={{
                                                    width: '30%',
                                                    display: 'flex',
                                                    flexDirection: 'row',
                                                    justifyContent: 'center'
                                                }}><Text style={{
                                                    fontSize: 13,
                                                    fontFamily: 'IRANSansMobile'
                                                }}>{this.props.row.centerLable ? this.props.row.centerLable : ''}</Text></View>
                                                <View style={{
                                                    width: '30%',
                                                    display: 'flex',
                                                    flexDirection: 'row',
                                                    justifyContent: 'flex-end'
                                                }}><Text style={{
                                                    fontSize: 13,
                                                    fontFamily: 'IRANSansMobile'
                                                }}>{this.props.row.rigthLable ? this.props.row.rigthLable : ''}</Text></View>

                                            </View>
                                        </View>
                                    </View>
                                </CardItem>
                                <CardItem footer bordered>
                                    <StoreButton onPress={this.storeAnswer} showSubmitCheck={this.state.showSubmitCheck}/>

                                </CardItem>
                            </Card>
                        </ScrollView>
                    </Content>
                    <Modal style={[modal.modal, modal.modal4]} position={"bottom"} ref={"modal4"}>
                        <Card style={modal.modalMainCard}>
                            <CardItem bordered style={card.cardTitle}>
                                <Text style={[common.headerText, {color: 'white'}]}>اطلاعات بیشتر . . .</Text>
                            </CardItem>
                            <CardItem style={{flexDirection: 'column'}}>
                                <Text>{this.state.percentage}% - <Text
                                    style={common.bodyText}>( {this.state.row.rowNumber}
                                    از {this.state.questionCount})</Text></Text>
                                <Progress.Bar progress={this.state.percentage / 100} width={300}
                                              color={(this.state.percentage / 100) < 0.5 ? "red" : "green"}
                                              height={30}></Progress.Bar>
                            </CardItem>
                            <CardItem style={card.modalCardItem2}>
                                <Button info style={[common.bodyText, modal.modalButton]} onPress={this.jumpToQuestion}><Text
                                    onPress={this.jumpToQuestion} style={common.bodyText}> پرش </Text></Button>
                                <Input
                                    style={[common.textInput, modal.modalInput]}
                                    keyboardType='numeric'
                                    onSubmitEditing={()=>this.jumpToQuestion()}
                                    placeholder='شماره سوال؟'
                                    onChangeText={val => this.setState({rowNumber: val})}/>
                            </CardItem>
                            <View style={card.modalView}>
                                <Text style={[common.bodyText]}>بیشتر بدانید :
                                    <Text style={common.webLink} onPress={() => {
                                        Linking.openURL(this.props.user.webSiteLink)
                                    }}>{this.props.user.companyName} </Text>
                                </Text>
                            </View>
                        </Card>
                    </Modal>
                </ImageBackground>
            </Container>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        porseshnameh: state.porseshnameh,
        project: state.project,
        user: state.user
    }
};
const mapDispathToProps = dispatch => {
    return {
        setPorseshnameh: porseshnameh => {
            dispatch(setPorseshnameh(porseshnameh))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(Numeric);


