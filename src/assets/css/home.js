import EstyleSheet from 'react-native-extended-stylesheet';


export const tab = EstyleSheet.create({
    cardItem:{
        backgroundColor:'#e6eaea'

    },
    tabBody: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    tabBodyText: {
        height: 100,
        lineHeight: 100,
        justifyContent: 'center',
        alignItems: 'center'
    }

})