import React, {Component} from "react";
import {
    Text,
    View,
    Dimensions,
    TouchableOpacity,
    TextInput,
} from 'react-native';

var {width, height} = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Ionicons';
import {common} from './../../assets/css/common';

export default class CheckboxGroupForOpenOptionQType extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pageWidth: Dimensions.get('window').width,
            pageHeight: Dimensions.get('window').height,
            selected: [],
            score: [],
            textInputVal: [],
            itemComplete: []
        };
        this.componentWillMount = this.componentWillMount.bind(this);
    }


    componentWillMount = () => {
        this.props.checkboxes.map((checkbox, index) => {
                if (checkbox.selected) {
                    let itemText = null
                    console.log("this.props.answerarray :", this.props.answerarray);

                    if (this.props.answerarray) {
                        // alert(JSON.stringify(this.props.answerarray))
                        this.props.answerarray.map((item) => {
                            if(item.idOption == checkbox.value)
                            {
                                itemText=item.text;
                                // alert("itemText : "+itemText);
                            }
                        })
                    }
                    this._onSelect(checkbox.value, itemText)
                }
            }
        )
    }
    _onSelect = (item, itemText) => {
        console.log("item :", item);
        console.log("itemText :", itemText);

        var itemCompleted = this.state.itemComplete;
        let insertOrRemove = 'insert';
        for (let i = 0; i < itemCompleted.length; i++) {
            if (item == Number(itemCompleted[i].idOptionInHost)) {
                itemCompleted.splice(i, 1);
                insertOrRemove = 'remove';
            }
        }
        if (insertOrRemove == 'insert') {
            itemCompleted.push({idOptionInHost: item, val: itemText})
        }
        console.log("itemCompleted :", itemCompleted);

        var selected = this.state.selected;
        if (selected.indexOf(item) == -1) {
            selected.push(item);
            this.setState({
                selected: selected,
            })
        } else {
            selected = selected.filter(i => i != item)
            this.setState({
                selected: selected,
            })
        }
        this.setState({itemCompleted: itemCompleted});
        this.props.callback(itemCompleted)
    }

    _onchengeInputText = (item, val) => {
        // console.log("item :", item)
        // console.log("val :", val)
        let itemComplete = this.state.itemComplete;
        for (i = 0; i < itemComplete.length; i++) {
            if (itemComplete[i].idOptionInHost == item) {
                itemComplete[i].val = val;
            }
        }
        this.setState({itemComplete: itemComplete})
        console.log("itemComplete:", itemComplete);
        this.props.callback(itemComplete);

    }

    _isSelected = (item) => {
        const selected = this.state.selected
        if (selected.indexOf(item) == -1) {
            return false
        }
        return true
    }
    _getTextFromAnswerArray = (itemId) => {
        // console.log("itemID :",itemId);
        // console.log("answerarray :",this.props.answerarray);

        for (let item of this.props.answerarray) {
            if (item.idOption == itemId)
                return item.text
        }
        return null;

    }

    render() {
        const {checkboxes, iconColor, iconSize, labelStyle, checkedIcon, uncheckedIcon, rowStyle, rowDirection} = this.props;
        let moreexplanationStyle = {marginRight: 3, color: 'red', fontSize: 10, fontFamily: 'IRANSansMobile',};
        // let answerArray=answer.text.split(",");

        return (
            <View
                style={{
                    flex: 1,
                    flexDirection: rowDirection,
                    padding: 5,
                    justifyContent: 'flex-end',
                    alignItems: 'flex-end',
                    fontFamily: 'IRANSansMobile'
                }}
            >
                {checkboxes.map((checkbox, index) => {
                    // console.log("checkbox:",checkbox);
                    return (
                        <TouchableOpacity
                            key={index}
                            style={rowStyle}
                            onPress={() => {
                                this._onSelect(checkbox.value)
                            }}>

                            <View style={{flexDirection: 'row-reverse'}}>
                                {this._isSelected(checkbox.value) ?
                                    <Icon name={checkedIcon} color={iconColor} size={iconSize}/> :
                                    <Icon name={uncheckedIcon} color={iconColor} size={iconSize}
                                          style={{fontWeight: 500}}/>}
                                <Text style={labelStyle}>{checkbox.label}</Text>
                                <Text
                                    style={[{alignSelf: 'center'}, moreexplanationStyle]}>{checkbox.moreExplanation}</Text>
                            </View>
                            {this._isSelected(checkbox.value) ?
                                <View>
                                    <TextInput
                                        style={[common.textInput, {paddingRight: 5, paddingLeft: 5,}]}
                                        onChangeText={val => this._onchengeInputText(checkbox.value, val)}
                                        defaultValue={this._getTextFromAnswerArray(checkbox.value)}/>
                                </View> : null}
                        </TouchableOpacity>
                    )
                })}
            </View>
        );
    }
}
