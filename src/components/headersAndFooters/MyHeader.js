import React from 'react';
import {Container, Header, Title, Right, Left, Content, Card, CardItem, Body, Input, Item, Button} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome5'
import Modal from 'react-native-modalbox';
import * as Progress from 'react-native-progress';
 import FooterMenuShow from './FooterMenuShow'


import {form} from '../../assets/styles/index';
import {common} from '../../assets/css/common';
class MyHeader extends React.Component{
    constructor(props){
        super(props);
        this.state={
            componenetTitle:''
        }
    }
    componentWillMount(){
        this.setState({componenetTitle:this.props.componenetTitle})

    }
    onpress =(i)=>{
        console.log("salam");
        this.props.onpressh(i)

    }
    render(){
        return(
            <Header style={form.headerStyle}>
                <Left style={{flexDirection: 'row'}}>
                    <Icon name='angle-double-up' size={20} color="#ffffff" solid style={{
                        fontWeight: 500,
                        marginRight: 10,
                        color: 'white',
                        marginLeft: 10,
                        marginBottom: 4
                    }} onPress={() => this.onpress("ali")}/>
                </Left>
                <Right>
                    <Title style={common.headerText}>{this.state.componenetTitle}</Title>
                    <Icon name='arrow-right' size={20} color="#ffffff" solid style={{
                        fontWeight: 500,
                        marginRight: 10,
                        color: 'white',
                        marginLeft: 10,
                        marginBottom: 4
                    }} onPress={() => Actions.push('pasokhgoo')}/>
                </Right>
            </Header>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        porseshnameh: state.porseshnameh,
        project: state.project,
        user: state.user
    }
}
const mapDispathToProps = dispatch => {
    return {
        setPorseshnameh: porseshnameh => {
            dispatch(setPorseshnameh(porseshnameh))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(MyHeader);