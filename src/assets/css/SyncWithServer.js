import EStyleSheet from 'react-native-extended-stylesheet'

export const card = EStyleSheet.create({
    componenetTitle: {
        fontSize: 18,
        fontFamily: 'IRANSansMobile'
    },
    cardTitle: {
        borderColor:'$backGroundColor',
        flexDirection: 'row-reverse',
        backgroundColor:'$backGroundColor',
        borderTopRightRadius:4,
        borderTopLeftRadius:4,
        borderBottomRightRadius:0,
        borderBottomLeftRadius:0,
    },
    body: {
        borderWidth:1,
    },
    mainCard:{
      borderRadius:4,
    },
    labelStyle:{
        fontSize:'$contentFontSize',
        flexDirection: 'row-reverse',

    },
    footersButton: {
        fontSize: 17,
        fontFamily: 'IRANSansMobile',
    },



    parentCard:{
        borderWidth:0,
        borderTopLeftRadius:4,
        borderTopRightRadius:4,
        elevation: 0,
        shadowOpacity: 0,
    },
    cardItemHeader:{
        borderColor:'$backGroundColor',
        flexDirection: 'row-reverse',
        backgroundColor:'$backGroundColor',
        borderColor:'$backGroundColor',
        borderWidth:1,
        borderBottomWidth:0,
        borderTopLeftRadius:4,
        borderTopRightRadius:4,
        borderBottomRightRadius:0,
        borderBottomLeftRadius:0
    },
    cardItemBody: {
        borderWidth:1,
        borderTopWidth:0,
        borderColor:'$backGroundColor',
        borderTopRightRadius:0,
        borderTopLeftRadius:0
    },
    modalCardItem2:{
        borderWidth:0,
    },
    modalView:{
        display:'flex',
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        flexWrap:'wrap',
    },
    circleGradient: {
        margin: 1,
        backgroundColor: "white",
        borderRadius: 5
    }

})
