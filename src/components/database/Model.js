import React from 'react';
import SQLite from 'react-native-sqlite-storage';

const db = SQLite.openDatabase({name: "maltDB", createFromLocation: "~malt.db"}, this.openSuccess, this.openError);

// =========================================================== SELECT ALL ============================================================
export const selectAll = (tableName, cb) => {
    db.transaction(
        tx => {
            let sql = "SELECT * FROM " + tableName;
            tx.executeSql(sql, [], (tx, results) => {
                cb(results);
            });
            ;
        },
        null,
        null
    );
}
// =========================================================== SELECT ALL USER PROJECT ============================================================
export const selectAllUserProject = (idUser, cb) => {
    db.transaction(
        tx => {
            let sql = "SELECT * FROM project WHERE status ='Active' AND idUser = " + idUser;
            // let sql = "SELECT userproject.* , project.* FROM project INNER JOIN userproject ON project.idInHost = userproject.idProject WHERE userproject.idUser='"+idUser+"' AND project.status='Active'";
            tx.executeSql(sql, [], (tx, results) => {
                cb(results);
                // console.log('result = ',results);
            });
            ;
        },
        null,
        null
    );
}

// =========================================================== SELECT ONE ============================================================
export const selectOne = (tableName, fieldName, value, cb) => {
    db.transaction(
        tx => {
            let sql = " SELECT * FROM " + tableName + "  WHERE " + fieldName + " = " + value;
            // console.log(sql);
            tx.executeSql(sql, [], (tx, results) => {
                // console.log(results);
                cb(results);
            });
            ;
        },
        null,
        null
    );
}

// =========================================================== DELETE ============================================================
export const deleteQuery = (tableName, field_name, where_condition,cb) => {

    db.transaction((tx) => {
        let sql = "DELETE FROM " + tableName + " WHERE " + field_name + " = " + where_condition;
        tx.executeSql(sql, [], (tx, results) => {
            cb(results);
        });
    });
}


export const clearTable = (tableName,cb) => {
    db.transaction((tx) => {
        let sql = "DELETE FROM " + tableName;
        tx.executeSql(sql, [], (tx, results) => {
            cb(results);
        });
    });
}

export const insertToUser = (newUser) => {
    db.transaction((tx) => {
        let sql = "INSERT INTO users (idInHost,level,idCompany,name,fName,nationalCode,username, password,homeAdd,remember_Token,status ,companyName, webSiteLink) VALUES ('" + newUser.id + "','" + newUser.level + "','" + newUser.idCompany + "','" + newUser.name + "','" + newUser.fName + "','" + newUser.nationalCode + "','" + newUser.username + "','" + newUser.password + "','" + newUser.homeAdd + "','" + newUser.remember_Token + "','" + newUser.status + "','" + newUser.companyName + "','"+newUser.webSiteLink+"')";
        tx.executeSql(sql, [], (tx, results) => {
            return true;
        });
    });
}

export const insertProject = (userPreojectRow, idUser) => {
    selectOne('project', 'idInHost', userPreojectRow.id, (results) => {
        if (results.rows.length == 0) {
            // console.log(idUser);
            db.transaction((tx) => {
                let sql = "INSERT INTO project (idInHost,idUser,idCompany,subject,startDate,endDate,questionunmer, pasokhgooNumber,status,softDelete) VALUES ('" + userPreojectRow.id + "','" + idUser + "','" + userPreojectRow.idCompany + "','" + userPreojectRow.subject + "','" + userPreojectRow.startDate + "','" + userPreojectRow.endDate + "','" + userPreojectRow.questionunmer + "','" + userPreojectRow.pasokhgooNumber + "','" + userPreojectRow.status + "','" + userPreojectRow.softDelete + "')";
                tx.executeSql(sql, [], (tx, results) => {
                    return true;
                });
            });
        }
        else {
            // console.log(idUser);

            db.transaction((tx) => {
                let sql = "UPDATE  project SET idUser='" + idUser + "' , idInHost = '" + userPreojectRow.id + "',idCompany = '" + userPreojectRow.idCompany + "',subject = '" + userPreojectRow.subject + "',startDate = '" + userPreojectRow.startDate + "',endDate = '" + userPreojectRow.endDate + "',questionunmer = '" + userPreojectRow.questionunmer + "', pasokhgooNumber = '" + userPreojectRow.pasokhgooNumber + "' WHERE idInHost =" + userPreojectRow.id;
                // console.log(sql);
                tx.executeSql(sql, [], (tx, results) => {
                    return true;
                });
            });
        }
    });
}

export const insertCity = (citiesRows) => {
    db.transaction((tx) => {
        let sql = "INSERT INTO city (idProject,name) VALUES ('" + citiesRows.idProject + "','" + citiesRows.name + "')";
        tx.executeSql(sql, [], (tx, results) => {
            // console.log('result:', results)
            // console.log('result:', results.insertId)
            return true;
        });
    });
}

export const insertPolicySet = (policyRow) => {
    db.transaction((tx) => {
        let sql = "INSERT INTO policyset (idPolicy,idProject,idQuestion,idOption,count,idOtherQ, JumpDestIdQ) VALUES ('" + policyRow.idPolicy + "','" + policyRow.idProject + "','" + policyRow.idQuestion + "','" + policyRow.idOption + "','" + policyRow.count + "','" + policyRow.idOtherQ + "','" + policyRow.JumpDestIdQ + "')";
        tx.executeSql(sql, [], (tx, results) => {
            // console.log("resultsOfInsert:", results.insertId);
            return results.insertId;
        });
    });
}

export const insertPolicyList = (policyListRow) => {
    db.transaction((tx) => {
        let sql = "INSERT INTO policyslist (name,caption) VALUES ('" + policyListRow.name + "','" + policyListRow.caption + "')";
        tx.executeSql(sql, [], (tx, results) => {
            return true;
        });
    });
}

export const insertQuedtionToDB = (QuestionRow) => {
    db.transaction((tx) => {
        let sql = "INSERT INTO questions (idInHost,idProject,rowNumber,idQuestionType,minNumberType,maxNumberType,rangeValue, rigthLable,centerLable,leftLeble,questionCode,text,moreExplanation,allowNull,haveImage,videoURL) VALUES ('" + QuestionRow.id + "','" + QuestionRow.idProject + "','" + QuestionRow.rowNumber + "','" + QuestionRow.idQuestionType + "','" + QuestionRow.minNumberType + "','" + QuestionRow.maxNumberType + "','" + QuestionRow.rangeValue + "','" + QuestionRow.rigthLable + "','" + QuestionRow.centerLable + "','" + QuestionRow.leftLeble + "','" + QuestionRow.questionCode + "','" + QuestionRow.text + "','" + QuestionRow.moreExplanation + "','" + QuestionRow.allowNull + "','" + QuestionRow.haveImage + "','"+QuestionRow.videoURL+"')";
        tx.executeSql(sql, [], (tx, results) => {
            return true;
        });
    });
}

// =========================================================== SELECT ALL QuestionFromDB============================================================
export const selectAllQuestionFromDB = (idProject, cb) => {
    db.transaction(
        tx => {
            let sql = " SELECT * FROM questions  WHERE idProject = " + idProject;
            tx.executeSql(sql, [], (tx, results) => {
                cb(results);
            });
            ;
        },
        null,
        null
    );
}

//=====================================================getQuestionHaveImage=================================================================
export const getQuestionHaveImageFromDB =(idProject,cb)=>{
    db.transaction(
        tx => {
            let sql = " SELECT idInHost FROM questions  WHERE idProject = " + idProject +" AND haveImage = 'Yes'";
            console.log("sql :" ,sql);
            tx.executeSql(sql, [], (tx, results) => {
                cb(results);
            });
            ;
        },
        null,
        null
    );
}
//=====================================================getQuestionByQTypeFromDB=================================================================
export const getQuestionByQTypeFromDB =(idProject,idQuestionType,cb)=>{
    db.transaction(
        tx => {
            let sql = " SELECT * FROM questions  WHERE idProject = " + idProject +" AND idQuestionType = "+ idQuestionType;
            console.log("sql :" ,sql);
            tx.executeSql(sql, [], (tx, results) => {
                cb(results);
            });
            ;
        },
        null,
        null
    );
}

//=====================================================insertOption=================================================================


export const insertOption = (row) => {
    db.transaction((tx) => {
        let sql = "INSERT INTO options (idInHost,idQuestion,textOption,moreExplanation,score,haveImage) VALUES ('" + row.id + "','" + row.idQuestion + "','" + row.textOption + "','" + row.moreExplanation + "','" + row.score + "','" + row.haveImage + "')";
        tx.executeSql(sql, [], (tx, results) => {
            return true;
        });
    });
};

export const insertQuestionType = (row) => {
    db.transaction((tx) => {
        let sql = "INSERT INTO questiontype (text,pLabel) VALUES ('" + row.text + "','" + row.pLabel + "')";
        tx.executeSql(sql, [], (tx, results) => {
            return true;
        });
    });
};

export const insertSEC = (row) => {
    db.transaction((tx) => {
        let sql = "INSERT INTO sec (idProject,SECName,SECScoreMin,SECScoreMax,caption,count) VALUES ('" + row.idProject + "','" + row.SECName + "','" + row.SECScoreMin + "','" + row.SECScoreMax + "','" + row.caption + "','" + row.count + "')";
        tx.executeSql(sql, [], (tx, results) => {
            return true;
        });
    });
};

export const insertToUserProjectTodb = (row) => {
    db.transaction((tx) => {
        let sql = "INSERT INTO userproject (idCompany,idUser,idProject) VALUES ('" + row.idCompany + "','" + row.idUser + "','" + row.idProject + "')";
        tx.executeSql(sql, [], (tx, results) => {
            return true;
        });
    });
};

// =========================================================== SELECT ALL Porseshname FOR UPLOAD========================
export const selectAllPorseshnameFromDB = (idProject, cb) => {
    db.transaction(
        tx => {
            let sql = "SELECT prseshname.id, prseshname.porseshnamehNumber , prseshname.idProject , prseshname.idPorseshgar , prseshname.completedDate,prseshname.startTime , prseshname.endTime,prseshname.Longitude , prseshname.latitude ,pasokhgoo.name, pasokhgoo.fName,pasokhgoo.homeAddress, pasokhgoo.workAddress, pasokhgoo.cityName, pasokhgoo.cityArea, pasokhgoo.phoneNumber, pasokhgoo.mobileNumber,answer.idQuestion, answer.idOption, answer.text FROM prseshname INNER JOIN pasokhgoo ON prseshname.id=pasokhgoo.idPorseshname INNER JOIN answer ON prseshname.id=answer.idPorseshname WHERE prseshname.uploadedToHost = 0 AND prseshname.idProject= " + idProject;
            console.log(sql);
            tx.executeSql(sql, [], (tx, results) => {
                cb(results);
            });
            ;
        },
        null,
        null
    );
}

// =========================================================== INSERT Porseshname=======================================

export const insertPorseshnamehToDB = (porseshnamehRow, cb) => {
    db.transaction((tx) => {
        let sql = "INSERT INTO prseshname (porseshnamehNumber,idProject,idPorseshgar,completedDate,startTime,Longitude,latitude) VALUES ('" + porseshnamehRow.porseshnamehNumber + "','" + porseshnamehRow.idProject + "','" + porseshnamehRow.idPorseshgar + "','" + porseshnamehRow.completedDate + "','" + porseshnamehRow.startTime + "','" + porseshnamehRow.Longitude + "','" + porseshnamehRow.latitude + "')";
        // console.log(sql);
        tx.executeSql(sql, [], (tx, results) => {
            // console.log('result0:', results.insertId)
            cb(results.insertId);
        });

    });
}
// =========================================================== INSERT Pasokhgoo ========================================

export const insertPasokhgooToDB = (pasokhgooRow, cb) => {
    db.transaction((tx) => {

        let sql = "INSERT INTO pasokhgoo (idPorseshname,name,fName,homeAddress,workAddress,cityName,cityArea,phoneNumber,mobileNumber) VALUES ('" + pasokhgooRow.idPorseshname + "','" + pasokhgooRow.name + "','" + pasokhgooRow.fName + "','" + pasokhgooRow.homeAddress + "','" + pasokhgooRow.workAddress + "','" + pasokhgooRow.cityName +"','"+ pasokhgooRow.cityArea + "','" + pasokhgooRow.phoneNumber + "','"+pasokhgooRow.mobileNumber+"')";
        // let sql = "INSERT INTO pasokhgoo (idPorseshname ,fName) VALUES (12,'sadas')";
        // console.log(sql);
        tx.executeSql(sql, [], (tx, results) => {
           // console.log(results);
        });

    });
}

export const updatePasokhgoo = (pasokhgooRow, id) => {

    db.transaction((tx) => {
        let sql = "UPDATE  pasokhgoo SET name='" + pasokhgooRow.name + "' , fName = '" + pasokhgooRow.fName + "',homeAddress = '" + pasokhgooRow.homeAddress + "',workAddress = '" + pasokhgooRow.workAddress + "',cityName = '" + pasokhgooRow.cityName + "',cityArea = '" + pasokhgooRow.cityArea + "',phoneNumber = '" + pasokhgooRow.phoneNumber + "', mobileNumber = '" + pasokhgooRow.mobileNumber + "' WHERE id =" + id;
        // console.log(sql);
        tx.executeSql(sql, [], (tx, results) => {
            return true;
        });
    });
}




export const selectCityList = (idProject, cb) => {
    db.transaction(
        tx => {
            let sql = "SELECT * FROM city WHERE   idProject = " + idProject;
            tx.executeSql(sql, [], (tx, results) => {
                cb(results);
                // console.log('result = ',results);
            });
        },
        null,
        null
    );
}

// =========================================================== SELECT Question =========================================
export const selectOneQuestion = (idProject , rowNumber, cb) => {
    db.transaction(
        tx => {
            let sql = " SELECT * FROM questions  WHERE idProject = " + idProject +" AND rowNumber = " + rowNumber;
            console.log("sql:" ,sql)
            tx.executeSql(sql, [], (tx, results) => {
                console.log("resultsofselectOneQuestion :",results.rows);
                cb(results);
            });
        },
        null,
        null
    );
}
//======================================================================================================================
export const selectAnswerRow=(idQuestion,idPorseshnameh,cb)=>{
    db.transaction(
        tx=>{
            let sql = " SELECT * FROM answer  WHERE idQuestion = " + idQuestion +" AND idPorseshname = " + idPorseshnameh;
            tx.executeSql(sql, [], (tx, results) => {
                cb(results);
            });
        }
    )

}
//======================================================================================================================
export const selectPolicySetFromDB=(idProject,idQuestion,cb)=>{
    db.transaction(
        tx=>{
            let sql = " SELECT * FROM policyset  WHERE idProject = " + idProject +" AND idQuestion = " + idQuestion;
            // console.log(sql);
            tx.executeSql(sql, [], (tx, results) => {
                // console.log("result : " ,results);
                cb(results);
            });
        }
    )

}
//======================================================================================================================
export const getQuestionCount=(idProject,cb)=>{
    db.transaction(
        tx=>{
            let sql = " SELECT Count(id) AS countQ FROM questions  WHERE idProject = " + idProject ;
            // console.log(sql);
            tx.executeSql(sql, [], (tx, results) => {
                // console.log("result : " ,results);
                cb(results);
            });
        }
    )

}
//==================================================================================================================================================================
export const insertAnswer = (idPorseshname,idQuestion,idOption,text) => {
    db.transaction((tx) => {
        let sql = "INSERT INTO answer (idPorseshname,idQuestion,idOption,text) VALUES ('" + idPorseshname + "','" + idQuestion + "','" + idOption + "','" + text + "')";
        console.log("INSERT sql :",sql);
        tx.executeSql(sql, [], (tx, results) => {
            return true;
        });
    });
};
//==================================================================================================================================================================
export const updateAnswer = (idPorseshname,idQuestion,idOption,text,idAnswer) => {
    db.transaction((tx) => {
        let sql = "UPDATE  answer SET idPorseshname='" + idPorseshname + "' , idQuestion = '" + idQuestion + "',idOption = '" + idOption + "',text = '" + text  + "' WHERE id =" + idAnswer;
        // console.log("UPDATE sql :",sql);
        tx.executeSql(sql, [], (tx, results) => {
            return true;
        });
    });
};

//======================================================================================================================
export const selectPorseshnamehList =(idProject,idUserInHost,cb)=>{
    console.log("idUserInHost:",idUserInHost);

    db.transaction(
        tx=>{
            let sql ="SELECT prseshname.id, prseshname.porseshnamehNumber,prseshname.completedDate,pasokhgoo.name,pasokhgoo.fName FROM prseshname INNER JOIN pasokhgoo ON prseshname.id=pasokhgoo.idPorseshname   WHERE prseshname.idProject =" + idProject +" AND prseshname.idPorseshgar = "+ idUserInHost;;
            // console.log(sql)
            tx.executeSql(sql, [], (tx, results) => {
                cb(results);
            });
        }
    )

}
//======================================================================================================================
export const selectPorseshnamehById =(idPorseshnameh,cb)=>{
    db.transaction(
        tx=>{
            let sql ="SELECT prseshname.id, prseshname.porseshnamehNumber,prseshname.completedDate,pasokhgoo.name,pasokhgoo.fName FROM prseshname INNER JOIN pasokhgoo ON prseshname.id=pasokhgoo.idPorseshname   WHERE prseshname.id=" + idPorseshnameh;
            //console.log(sql)
            tx.executeSql(sql, [], (tx, results) => {
                cb(results);
            });
        }
    )

}

//======================================================================================================================
export const selectPorseshnamehByNumber =(porseshnamehNumber,idProject,cb)=>{
    console.log("selectPorseshnamehByNumber ");
    db.transaction(
        tx=>{
            let sql ="SELECT porseshnamehNumber FROM prseshname WHERE porseshnamehNumber='" + porseshnamehNumber +"' AND idProject = '"+ idProject+"'";
            // console.log(sql)
            tx.executeSql(sql, [], (tx, results) => {
                cb(results);
            });
        }
    )

}


//======================================================================================================================

export const selectOnePasokhgoo = (idPorseshname, cb) => {
    db.transaction(
        tx => {
            let sql = "SELECT * FROM pasokhgoo WHERE   idPorseshname = " + idPorseshname;
            tx.executeSql(sql, [], (tx, results) => {
                // console.log('result = ',results);
                cb(results);
            });
        },
        null,
        null
    );
}
//======================================================================================================================
export const getOptions =(idQuestion,cb)=>{
    db.transaction(
        tx=>{
            let sql ="SELECT * FROM options  WHERE idQuestion=" + idQuestion;
            //console.log(sql)
            tx.executeSql(sql, [], (tx, results) => {
                cb(results);
            });
        }
    )

}
//===============================================================getPolicyAnsToOtherFromDB==============================
export const getPolicyAnsToOtherFromDB =(idPorseshnameh,idQuestion,cb)=>{
    db.transaction(
        tx=>{
            let sql =`SELECT answer.idOption AS idOptionans,policyset.* FROM answer INNER JOIN policyset on answer.idQuestion=policyset.idQuestion WHERE answer.idPorseshname=${idPorseshnameh} AND policyset.idOtherQ=${idQuestion} AND policyset.idPolicy=4`;
            // console.log(sql)
            tx.executeSql(sql, [], (tx, results) => {
                cb(results);
            });
        }
    )

}
//======================================================================================================================
export const getOptionByIdInHost =(idInHost,cb)=>{
    db.transaction(
        tx=>{
            let sql =`SELECT * FROM options  WHERE idInHost=${idInHost} LIMIT 1 `;
            console.log(sql)
            tx.executeSql(sql, [], (tx, results) => {
                // console.log("resultsSQL :",results);
                cb(results);
            });
        }
    )

}

//======================================================================================================================

export const getPolicyForStopAnswerFromDB =(idPolicy,idProject,idQuestion,idOption,cb)=>{
    db.transaction(
        tx=>{
            let sql =`SELECT * FROM policyset WHERE idPolicy=${idPolicy} AND idProject=${idProject} AND idQuestion=${idQuestion} AND idOption=${idOption}`;
            tx.executeSql(sql, [], (tx, results) => {
                cb(results.rows.item(0));
            });
        }
    )
}
//======================================================================================================================
export const getQByIdInHostFromDB =(idQuestionInHost,cb)=>{
    db.transaction(
        tx=>{
            let sql =`SELECT rowNumber FROM questions  WHERE idInHost=${idQuestionInHost} LIMIT 1 `;
            tx.executeSql(sql, [], (tx, results) => {
                cb(results);
            });
        }
    )

}

export const updatePorseshnamehAfterUpload = (id, idInHost) => {

    db.transaction((tx) => {
        let sql = "UPDATE  prseshname SET idInHost='" + idInHost + "' , uploadedToHost = 1 WHERE id =" + id;
        console.log(sql);
        tx.executeSql(sql, [], (tx, results) => {
            return true;
        });
    });
}
export const updatePorseshnamehWhenEdit= (id) => {

    db.transaction((tx) => {
        let sql = "UPDATE  prseshname SET  uploadedToHost = 0 WHERE id =" + id;
        console.log(sql);
        tx.executeSql(sql, [], (tx, results) => {
            return true;
        });
    });
}