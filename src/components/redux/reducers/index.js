import { combineReducers} from 'redux';
import user from './userReducer';
import project from './projectReducer';
import porseshnameh from './porseshnamehReducer'
import globalVariables from './globalVariables'

export default combineReducers({
    user,
    project,
    porseshnameh,
    globalVariables,
});
