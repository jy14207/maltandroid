import {deleteQuestionsQuery , insertQuedtionToDB , selectAllQuestionFromDB ,
    deleteQuery ,insertOption , clearTable ,insertQuestionType ,getOptions ,getOptionByIdInHost} from './Model';

export const deleteQuestionFromDB = (idProject)=>{
    deleteQuery('questions' , 'idProject' , idProject);
};

export const insertQuedtion=(QuestionRow)=>{
    insertQuedtionToDB(QuestionRow);

};

export const selectAllQuestion=(idProject,questionRows)=>{
    selectAllQuestionFromDB(idProject,(results)=>{
        questionRows(results);
    });
};

export const deleteOptionsFromDBS = (idQuestion)=>{
    deleteQuery('options','idQuestion',idQuestion);
}

export const insertOptionsToDB=(optionRow)=>{
    insertOption(optionRow);
}
export const deleteQuestionType=(idProject)=>{
    return new Promise((resolve,reject)=>{
        clearTable('questiontype',resolve);
    })
};

export const insertQuestionTypeToDB=(row)=>{
    insertQuestionType(row);
}
export const getOptionFromDbPromise=(idQuestion)=>{
    return new Promise((resolve,reject)=>{
        getOptions(idQuestion,(result)=>{
            resolve(result);
        })
    })
}
export const getOptionFromDBPromise =(idOptionsInHost)=>{
    return new Promise((resolve,reject)=>{
        getOptionByIdInHost(idOptionsInHost,(result)=>{
            console.log("result111111111111 :",result);
            resolve(result);
        })
    })
}
