import {deleteQuery , insertPolicySet , clearTable , insertPolicyList} from "./Model";

export const deletePolicyOfThisProject =(idProject) => {
    return new Promise((resolve,reject)=>{
        deleteQuery('policyset' , 'idProject' , idProject,cb=>{
            console.log("deletePolicyOfThisProject :", cb);
            resolve(cb);
        });
    })
}
export const insertPolicySetToDatabese=(policyRow)=>{
   insertPolicySet(policyRow);

}

//=========================================================================================== POLICY List OPRATION
export const deletePolicyListOfThisProject =() => {
    return new Promise((resolve,reject)=>{
        clearTable('policyslist',resolve) })
}

export const insertPolicyListToDatabese=(policyListRow)=>{
    insertPolicyList(policyListRow);

}


