import React from 'react';
import {
    View,
    Text,
    TextInput,
    NetInfo,
    ActivityIndicator,
    ImageBackground,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {
    Container, Header, Title, Button, Icon,
    Left, Right, Body, Content, Item, Input,
    Form
} from "native-base";
import {connect} from 'react-redux';
import axios from 'react-native-axios'
import Spinner from 'react-native-loading-spinner-overlay';
import SQLite from 'react-native-sqlite-storage';

import {form} from './../assets/styles';
import {index} from "../assets/styles/index";

import {insertUser} from './../components/database/user';
import {setUser} from "./../components/redux/actions/index";
import {common} from './../assets/css/common';


class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            spinner: false,
            username: '',
            password: '',
            error: '',
            netCheck: ''
        }
    }

    render() {
        const error = this.state.error;
        const componenetTitle = 'ثبت نام';

        return (
            <Container>
                <ImageBackground
                    source={require('./../assets/images/AppImage/LoginPage.jpg')}
                    style={{
                        resizeMode: 'stretch',
                        flex: 1,
                        width: '100%',
                    }}
                    imageStyle={{borderRadius: 2}}>
                    <Spinner
                        visible={this.state.spinner}
                        cancelable={true}
                        size='large'
                        overlayColor='rgba(100,100,100,0.8)'/>
                    <Header style={form.headerStyle} androidStatuseBarColor='#337ab7'>
                        <Left style={{flex: 1, flexDirection: 'row'}}>
                            <TouchableOpacity>
                                <Title style={common.headerText}>{componenetTitle}</Title>
                            </TouchableOpacity>
                        </Left>
                        <Right>
                            <TouchableOpacity style={{flexDirection: 'row'}}>
                                <Title style={common.headerText}>راهنما</Title>
                            </TouchableOpacity>
                        </Right>
                    </Header>
                    <Content style={index.content}>
                        <ActivityIndicator size="large" color="#0000ff"
                                           style={{opacity: this.state.showProgress ? 1.0 : 0.0}} animating={true}/>
                        <Form style={form.styleForm}>
                            <Item style={form.item} rounded error={error !== ''}>
                                <Input
                                    style={form.inputs}
                                    getRef={input => {
                                        this.nameRef = input;
                                    }}
                                    onSubmitEditing={() => {
                                        this.fNameRef._root.focus();
                                    }}
                                    returnKeyType={"next"}
                                    placeholder='نام کاربری'
                                    onChangeText={username => this.setState({username})}
                                />
                                <Icon active name='md-person'/>
                            </Item>
                            <Text style={[form.error, this._checkError(error)]}>{this.state.error}</Text>
                            <Item style={form.item} rounded error={error !== ''}>
                                <Input
                                    ref={input => {
                                        this.fNameRef = input;
                                    }}
                                    placeholder='گذر واژه'
                                    style={form.inputs}
                                    direction='ltr'
                                    secureTextEntry={true}
                                    onChangeText={this.setPasswordState.bind(this)}
                                    onSubmitEditing={() => this.login()}
                                />
                                <Icon active name='md-key'/>
                            </Item>
                            <Text style={[form.error, this._checkError(error)]}>{this.state.error}</Text>
                            <TouchableOpacity>
                                <Button style={form.submitButton} onPress={this.login} full>
                                    <Text style={form.submitText}>ورود</Text>
                                </Button>
                            </TouchableOpacity>
                        </Form>
                    </Content>
                </ImageBackground>
            </Container>
        )
    }

    setUsenameState(text) {
        this.setState({
            username: text
        })
    }

    setPasswordState(text) {
        this.setState({
            password: text
        });
    }

    login = () => {
        let {username, password} = this.state;
        if (username === '' | password === '') {
            this.setState({error: 'فیلدهای ورودی نمی توانند خالی باشند'})
        }
        else {
            NetInfo.getConnectionInfo().then((connectionInfo) => {
                if (connectionInfo.type != 'none') {
                    this.setState({spinner: !this.state.spinner});
                    this.requestLoginFromApi({username, password})
                } else {
                    alert("دستگاه خودرا به اینترنت وصل کنید")
                }
            });

        }
        return;
    }

    _checkError(field) {
        return {display: field === '' ? 'none' : 'flex'};
    }

    async requestLoginFromApi(params) {
        let {username, password} = params;
        const formData = new FormData();
        formData.append('username', username);
        formData.append('password', password);

        url = this.props.globalVariables.homeURL + '/index.php?r=login/loginFromAndroid';
        console.log("url:", url)
        try {
            let response = await axios({
                url: url,
                method: 'POST',
                data: formData,
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data'
                }
            });
            console.log("response:", response);
            if (response.status === 200) {
                let dataUser = await response.data;
                if (dataUser.id) {
                    this.setState({spinner: !this.state.spinner});
                    insertUser(dataUser);
                    this.getUser()
                    Actions.reset('root');
                }
                else {
                    alert("خطای ناشناخته ای رخ داده است، لطفا با مدیر پروژه تماس بگیرید");
                }
                this.setState({spinner: !this.state.spinner});
            }
            else{
                alert("اتصال به سرور امکان پذیر نیست لطفا این کد را به مدیر پروژه اعلام کنید."+ response.status);
            }
        } catch (error) {
            alert(JSON.stringify(error));
        }
    }

    getUser() {
        const db = SQLite.openDatabase({name: "maltDB", createFromLocation: "~malt.db"}, null, null);

        db.transaction((tx) => {
            let sql = "SELECT * FROM users"
            tx.executeSql(sql, [], (tx, results) => {
                let len = results.rows.length;
               const timeOut= setTimeout(() => {
                    if (len > 0) {
                        var user = {
                            id: results.rows.item(0).id,
                            idInHost: results.rows.item(0).idInHost,
                            level: results.rows.item(0).level,
                            idCompany: results.rows.item(0).idCompany,
                            name: results.rows.item(0).name,
                            fName: results.rows.item(0).fName,
                            nationalCode: results.rows.item(0).nationalCode,
                            username: results.rows.item(0).username,
                            password: results.rows.item(0).password,
                            homeAdd: results.rows.item(0).homeAdd,
                            remember_Token: results.rows.item(0).remember_Token,
                            status: results.rows.item(0).status,
                            companyName: results.rows.item(0).companyName,
                            webSiteLink: results.rows.item(0).webSiteLink
                        }
                        this.props.setUser(user);
                        Actions.reset('root');
                    } else {
                        Actions.reset('auth');
                    }
                }, 3000)
            });
        });
    }
}
const mapStateToProps = (state) => {
    return {
        globalVariables: state.globalVariables,
    }
}

const mapDispathToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(Login)

