import EstyleSheet from 'react-native-extended-stylesheet';

export const common = EstyleSheet.create({
    headerText: {
        fontSize: '$windowTitleFontSize',
        fontFamily: '$IS',
    },
    fullImageBackground: {
        flex: 1,
        width: '100%',
    },
    titleText: {
        fontSize: 17,
        fontFamily: '$IS',
        color: '$blueBackgroundColor'
    },
    bodyText: {
        fontSize: '$contentFontSize',
        fontFamily: '$IS'
    },
    textInput: {
        fontFamily: '$IS',
        fontSize: '$contentFontSize',
        height: 40,
        padding: 2,
        borderWidth: 1,
        borderRadius: 3,
        borderColor: '#898c8c',
        color: 'black',
        marginBottom: 10,
    },
    moreExplanationText: {
        fontSize: 13,
        fontFamily: '$IS',
        color: '$yellowBackgroundColor',
        marginRight: 5,
    },
    questionPolicyAlert: {
        backgroundColor: '#fff3cd',
        borderWidth: 2,
        borderColor: '#ffeeba',
        borderRadius: 5,
        padding: 3,
        margin: 5,
    },
    questionPolicyTextAlert: {
        fontSize: 12,
        fontFamily: '$IS',
        marginBottom: 2,
    },
    webLink: {
        color: '#353df7',
        textDecorationLine: 'underline'
    },
    ansToOtherQTextStyle: {
        fontSize: 13,
        fontFamily: '$IS',
        color: '$redColor',
        marginRight: 5,
    },
    finishedText: {
        fontSize: 15,
        fontFamily: '$ISBold',
        color: '$yellowBackgroundColor',
    }
})