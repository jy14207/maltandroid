import EstyleSheet from 'react-native-extended-stylesheet'

export const modal = EstyleSheet.create({

    wrapper: {
        flex: 1,
        flexDirection:'row'
    },

    modal: {
        margin:0,
        padding:0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewModal:{
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom:20
    },
    modal4: {
        paddingTop:0,
        height: 250,
        borderTopRightRadius:4,
        borderTopLeftRadius:4,
    },
    modalInput:{
        width:100,
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
    },
    modalButton:{
        justifyContent:'center',
        alignItems:'center',
        height:40,
        width:50,
        borderBottomRightRadius:0,
        borderTopRightRadius:0
    },
    modalMainCard:{
        flexDirection: 'column',
        margin: 0,
        width: '100%',
        height: 250,
        borderWidth: 0,
        borderTopRightRadius:4,
        borderTopLeftRadius:4,

    }


});