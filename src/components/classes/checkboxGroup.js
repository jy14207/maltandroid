import React, {Component} from "react";
import {
    Text,
    View,
    Dimensions,
    TouchableOpacity,
} from 'react-native';

var {width, height} = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Ionicons';

export default class CheckboxGroup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pageWidth: Dimensions.get('window').width,
            pageHeight: Dimensions.get('window').height,
            selected: [],
            score:[],
        };
    }

    componentDidMount = () => {
        this.props.checkboxes.map(checkbox => {
                if (checkbox.selected) {
                    this._onSelect(checkbox.value,checkbox.score)
                }
            }
        )
    }

    getNewDimensions(event) {
        var pageHeight = event.nativeEvent.layout.height
        var pageWidth = event.nativeEvent.layout.width
        this.setState({
            pageHeight, pageWidth
        })
    }

    _onSelect = (item,score) => {
        console.log("score :" ,score)
        var selected = this.state.selected
        var score1 = this.state.score
        if (selected.indexOf(item) == -1) {
            selected.push(item);
            score1.push(score);
            this.setState({
                selected: selected,
                score:score1,
            })
        } else {
            selected = selected.filter(i => i != item)
            score1 = selected.filter(i => i != score)
            this.setState({
                selected: selected,
                score: score1,
            })
        }
        this.props.callback(selected,score1)
    }

    _isSelected = (item) => {
        const selected = this.state.selected
        if (selected.indexOf(item) == -1) {
            return false
        }
        return true
    }

    render() {
        const {checkboxes, iconColor, iconSize, labelStyle, checkedIcon, uncheckedIcon, rowStyle, rowDirection} = this.props;
        let moreexplanationStyle = {marginRight: 3,color:'red',fontSize:10, fontFamily:'IRANSansMobile',};

        return (
            <View
                onLayout={(evt) => {
                    this.getNewDimensions(evt)
                }}
                style={{
                    flex: 1,
                    flexDirection: rowDirection,
                    padding: 5,
                    justifyContent: 'flex-end',
                    alignItems: 'flex-end',
                    fontFamily:'IRANSansMobile'
                }}
            >
                {checkboxes.map((checkbox, index) => {
                    return (
                        <TouchableOpacity
                            key={index}
                            style={rowStyle}
                            onPress={() => {
                                this._onSelect(checkbox.value,checkbox.score)
                            }}
                        >
                            <Text style={[{alignSelf: 'center'},moreexplanationStyle ]}>{checkbox.moreExplanation}</Text>
                            <Text style={labelStyle}>{checkbox.label}</Text>
                            {this._isSelected(checkbox.value) ?
                                <Icon name={checkedIcon} color={iconColor} size={iconSize}/>
                                : <Icon name={uncheckedIcon} color={iconColor} size={iconSize} style={{fontWeight:500}}/>
                            }
                        </TouchableOpacity>
                    )
                })}
            </View>
        );
    }
}
